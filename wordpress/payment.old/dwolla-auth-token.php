<?php
  /*
  * Makes a curl request to dwolla and returns access token (type bearer)
  * which is valid 3601 seconds (1 hour)
  */
  $dwollaconf = include('dwolla.conf.php');

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $dwollaconf['token_endpoint']);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
    'client_id' => $dwollaconf['api_key'],
    'client_secret' => $dwollaconf['api_secret'],
    'grant_type' => 'client_credentials'
  )));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $server_output = curl_exec ($ch);
  curl_close ($ch);
  $server_output = json_decode($server_output);
  return $server_output->access_token;
