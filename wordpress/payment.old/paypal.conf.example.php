<?php

$USE_LIVE = false;

$conf = [
  'sandbox' => [
    'mode' => 'TEST', // mode 'TEST' or 'LIVE'
    'partner' => 'PayPal', // The Payflow partner.
    'vendor' => '', // The Merchant Login ID that you use to log into PayPal Manager.
    'user' => '', // The name of the user whom you added to your account using PayPal Manager, above.
    'password' => '', // The password of the user whom you added to your account using PayPal Manager, above.
    'transaction_type' => 'S', // The type of the transaction, e.g. S for sale.
    'currency' => 'USD',
    'endpoint' => 'https://pilot-payflowpro.paypal.com/',
    'return_url' => '.../success-cc.php', // full URL to success-cc.php
    'cancel_url' => '.../cancel-cc.php', // full URL to cancel-cc.php ==> NOT REALLY USED!
    'error_url' => '.../error-cc.php', // full URL to error-cc.php
    'success_url' => '.../success-cc.php', // full URL to success-cc.php
  ],
  'live' => [
    'mode' => 'LIVE', // mode 'TEST' or 'LIVE'
    'partner' => 'PayPal', // The Payflow partner.
    'vendor' => '', // The Merchant Login ID that you use to log into PayPal Manager.
    'user' => '', // The name of the user whom you added to your account using PayPal Manager, above.
    'password' => '', // The password of the user whom you added to your account using PayPal Manager, above.
    'transaction_type' => 'S', // The type of the transaction, e.g. S for sale.
    'currency' => 'USD',
    'endpoint' => 'https://payflowpro.paypal.com',
    'return_url' => '.../success-cc.php', // full URL to success-cc.php
    'cancel_url' => '.../cancel-cc.php', // full URL to cancel-cc.php ==> NOT REALLY USED!
    'error_url' => '.../error-cc.php', // full URL to error-cc.php
    'success_url' => '.../success-cc.php', // full URL to success-cc.php
  ]
];

return $conf[$USE_LIVE === true ? 'live' : 'sandbox'];
