<?php $config = require_once( dirname(__FILE__) . '/config.php' ); ?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Balance bank account</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="<?=$config['BASE_URL'];?>/styles/main.css">
    </head>
    <body>

      <!-- Header -->
      <header id="header">
        <div style="background-image: url(https://www.cccssf.org/bankruptcy/images/header_background.png);background-repeat: no-repeat; background-size: cover; background-position: 50% 0; max-height: 50px; letter-spacing: -4px; text-align: right; padding: 10px 5px;" class="header-t">
          <div class="container">
            <div class="row">
              <!-- search form-->
              <ul class="contact-block" style="text-transform: uppercase; letter-spacing: 0; line-height: 0; font-size: 0; display: inline-block; vertical-align: middle; letter-spacing: 1px; margin-right: 11%;">
                <li><a href="tel:8007777526"><span class="icon-phone"></span>800.777.PLAN (7526)</a></li>
              </ul>
            </div>
          </div>
        </div>
        <nav class="navbar navbar-default">
          <div class="container">
            <div class="row">
              <div class="navbar-header" style="margin:30px 0; margin-left: 11%;">
                <a href="http://www.balancepro.org/"><img src="https://www.cccssf.org/bankruptcy/images/logo.svg" width="238" height="51" alt="balance"></a>
              </div>
            </div>
          </div>
        </nav>
      </header>

      <!-- Main -->
      <div class="main">
        <h1>There was an error with your payment.</h1>

        <?php
          global $NO_TRY_AGAIN;
          if(!$NO_TRY_AGAIN) {
        ?>
        <p><a onclick='window.history.back()'>Try again</a> or contact us at <a href="tel:8007777526">800.777.PLAN (7526)</a></p>
        <?php } ?>

        <p>Administrators were notified about the issue.</p>

      </div>


      <!-- Footer -->
      <footer id="footer" style="background-image: url(https://www.cccssf.org/bankruptcy/images/footer_background.png);">
        <div class="container">
          <div class="row">
            <div class="logo-holder"><a href="http://www.balancepro.org/"><img src="https://www.cccssf.org/bankruptcy/images/logo-primary.svg" width="198" height="44" alt="Balance"></a></div>

          </div>
        </div>
      </footer>

    </body>
</html>
