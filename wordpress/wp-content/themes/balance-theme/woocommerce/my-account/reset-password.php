<?php
$my_account_page_permalink = get_the_permalink ( get_option( 'woocommerce_myaccount_page_id' ) );
$user_id = get_current_user_id();
$args = array( 'key' => '', 'login' => '' );
if ( isset( $_GET['key'] ) && isset( $_GET['login'] ) && $user_id == 0 ) {
	$user = check_password_reset_key( $_GET['key'], $_GET['login'] );
	// reset key / login is correct, display reset password form with hidden key / login values
	if ( is_object( $user ) ) {
		$args['key'] = esc_attr( $_GET['key'] );
		$args['login'] = esc_attr( $_GET['login'] );
	}
}
?>
<ul role="tablist" class="nav nav-tabs">
	<li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab"><span class="icon-user"></span><?php _e( 'Reset Password', 'balance' ); ?></a></li>
</ul>
<div class="tab-content">
	<?php  if ( isset( $_GET['reset'] ) ) { ?>
	<div class="woocommerce-message woocommerce-message-success"><?php echo __( 'Your password has been successfully reset. You will be redirected to login screen in... ', 'woocommerce' ) . '<b><span class="counter">4</span>s</b>'; ?></div>
	<script>
		jQuery(document).ready(function() {
			counter = 3;
			setInterval(function() {
				jQuery('.counter').html(counter);
				counter--;
				if (counter == 0) {
					window.location = '<?php echo $my_account_page_permalink; ?>?action=login';
				}
			}, 1000);
		});
	</script>
	<br>
	<?php }  else { ?>
		<div id="tab1" role="tabpanel" class="tab-panel active">
			<form method="post" class="login account-form" action="">
				<fieldset>
					<p><?php wc_print_notices(); ?></p>
					<div class="form-block">
						<div class="input-group">
							<span class="label">
								<label for="password_1"><?php _e( 'New password', 'balance' ); ?></label>
							</span>
							<input id="password_1" type="password" name="password_1" placeholder="<?php _e( '8 characters, no spaces', 'balance' ); ?>" required>
						</div>
						<div class="input-group">
							<span class="label">
								<label for="password_2"><?php _e( 'Re-enter new password', 'balance' ); ?></label>
							</span>
							<input id="password_2" type="password" name="password_2" placeholder="<?php _e( '8 characters, no spaces', 'balance' ); ?>" required>
						</div>
						<?php do_action( 'woocommerce_lostpassword_form' ); ?>
						<div class="btn-holder">
							<input type="hidden" name="reset_key" value="<?php echo isset( $args['key'] ) ? $args['key'] : ''; ?>" />
	      						<input type="hidden" name="reset_login" value="<?php echo isset( $args['login'] ) ? $args['login'] : ''; ?>" />
							<?php wp_nonce_field( 'reset_password' ); ?>
							<input type="hidden" name="wc_reset_password" value="true" />
							<input type="submit" class="btn btn-warning" name="login" value="<?php esc_attr_e( 'Reset Password', 'woocommerce' ); ?>" />
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	<?php } ?>
</div>
