<?php
global $my_account_action;
$user = get_userdata( $user_id );
$my_account_page_permalink = get_the_permalink ( get_option( 'woocommerce_myaccount_page_id' ) );
if ( $my_account_action == 'view-billing-address') {
$name = 'billing';
$address = apply_filters( 'woocommerce_my_account_my_address_formatted_address', array(
		'first_name'  => get_user_meta( $user->ID, $name . '_first_name', true ),
		'last_name'   => get_user_meta( $user->ID, $name . '_last_name', true ),
		'company'     => get_user_meta( $user->ID, $name . '_company', true ),
		'address_1'   => get_user_meta( $user->ID, $name . '_address_1', true ),
		'address_2'   => get_user_meta( $user->ID, $name . '_address_2', true ),
		'city'        => get_user_meta( $user->ID, $name . '_city', true ),
		'postcode'    => get_user_meta( $user->ID, $name . '_postcode', true ),
		'country'     => get_user_meta( $user->ID, $name . '_country', true ),
		'state'       => get_user_meta( $user->ID, $name . '_state', true ),
		'phone'       => get_user_meta( $user->ID, $name . '_phone', true ),
		'email'       => get_user_meta( $user->ID, $name . '_email', true ),
	), $user->ID, $name );
}
?>
<h1>My account</h1>
<ul role="tablist" class="nav nav-tabs">
	<li role="presentation" class="<?php echo $my_account_action == 'view-account-details' ? 'active' : ''; ?>"><a href="?action=view-account-details" aria-controls="tab1" role="tab"><span class="icon-user"></span><?php _e( 'My Account', 'balance' ); ?></a></li>
	<li role="presentation" class="<?php echo $my_account_action == 'view-billing-address' ? 'active' : ''; ?>"><a href="?action=view-billing-address" aria-controls="tab2" role="tab"><span class="icon-user"></span><?php _e( 'Billing Address', 'balance' ); ?></a></li>
	<!-- <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab"><span class="icon-user"></span><?php _e( 'Payment Methods', 'balance' ); ?></a></li> -->
	<li role="presentation" class="<?php echo $my_account_action == 'view-my-orders' ? 'active' : ''; ?>"><a href="?action=view-my-orders" aria-controls="tab4" role="tab"><span class="icon-user"></span><?php _e( 'My Orders', 'balance' ); ?></a></li>
</ul>
<div class="tab-content">
	<?php wc_print_notices(); ?>
	<?php if ( $my_account_action == 'view-account-details') { ?>
	<!-- MY ACCOUNT -->
	<div id="tab1" role="tabpanel" class="tab-panel active">
		<form class="edit-account account-form" action="<?php echo $my_account_page_permalink . '?action=view-account-details'; ?>" method="post">
			<fieldset>
				<?php do_action( 'woocommerce_edit_account_form_start' ); ?>
				<p><?php _e( 'Manage your account details', 'balance' ); ?>:</p>
				<div class="form-block">
					<div class="input-group">
						<span class="label">
							<label for="account_first_name"><?php _e( 'First Name', 'woocommerce' ); ?></label>
						</span>
						<input id="account_first_name" type="text" autocomplete="off" name="account_first_name" placeholder="<?php _e( 'First Name', 'balance' ); ?>" value="<?php echo esc_attr( $user->first_name ); ?>" required>
					</div>
					<div class="input-group">
						<span class="label">
							<label for="account_last_name"><?php _e( 'Last Name', 'woocommerce' ); ?></label>
						</span>
						<input id="account_last_name" type="text" autocomplete="off" name="account_last_name" placeholder="<?php _e( 'Last Name', 'balance' ); ?>" value="<?php echo esc_attr( $user->last_name ); ?>" required>
					</div>
					<div class="input-group">
						<span class="label">
							<label for="account_email"><?php _e( 'Email Address', 'woocommerce' ); ?></label>
						</span>
						<input id="account_email" type="email" autocomplete="off" name="account_email" placeholder="<?php _e( 'Your Email', 'balance' ); ?>" value="<?php echo esc_attr( $user->user_email ); ?>" required>
					</div>
					<div class="input-group">
						<span class="label">
							<label for="account_username"><?php _e( 'Username', 'woocommerce' ); ?></label>
						</span>
						<input id="account_username" type="text" disabled autocomplete="off" placeholder="<?php _e( 'Your username', 'balance' ); ?>" value="<?php echo esc_attr( $user->user_login ); ?>">
					</div>
					<div class="input-group">
						<span class="label">
							<label for="password_current"><?php _e( 'Current Password', 'woocommerce' ); ?></label>
						</span>
						<input id="password_current" type="password" autocomplete="off" name="password_current" placeholder="<?php _e( 'Leave blank to leave unchanged', 'balance' ); ?>">
					</div>
					<div class="input-group">
						<span class="label">
							<label for="password_1"><?php _e( 'New Password', 'woocommerce' ); ?></label>
						</span>
						<input id="password_1" type="password" autocomplete="off" name="password_1" placeholder="<?php _e( 'Leave blank to leave unchanged', 'balance' ); ?>">
					</div>
					<div class="input-group">
						<span class="label">
							<label for="password_2"><?php _e( 'Confirm New Password', 'woocommerce' ); ?></label>
						</span>
						<input id="password_2" type="password" autocomplete="off" name="password_2" placeholder="<?php _e( 'Leave blank to leave unchanged', 'balance' ); ?>">
					</div>
				</div>
			</fieldset>
			<br>
			<?php do_action( 'woocommerce_edit_account_form' ); ?>
			<p class="btn-holder">
				<?php wp_nonce_field( 'save_account_details' ); ?>
				<input type="submit" class="btn btn-warning" name="save_account_details" value="<?php esc_attr_e( 'Save Changes', 'woocommerce' ); ?>" />
				<input type="hidden" name="action" value="save_account_details" />
				<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
			</p>
		</form>
	</div>
	<?php } ?>
	<!-- END MY ACCOUNT -->
	<!-- BILLING ADDRESS -->
	<?php if ( $my_account_action == 'view-billing-address') { ?>
	<div id="tab2" role="tabpanel" class="tab-panel active">
		<form class="edit-account account-form" action="<?php echo $my_account_page_permalink . '?action=view-billing-address'; ?>" method="post">
			<fieldset>
				<?php do_action( "woocommerce_before_edit_address_form_billing" ); ?>
				<p><?php _e( 'Manage your billing address details', 'balance' ); ?>:</p>
				<div class="form-block">
					<div class="input-group">
						<span class="label">
							<label for="billing_first_name"><?php _e( 'First Name', 'woocommerce' ); ?></label>
						</span>
						<input id="billing_first_name" type="text" autocomplete="off" name="billing_first_name" placeholder="<?php _e( 'First Name', 'balance' ); ?>" value="<?php echo !empty( $_POST['billing_first_name'] ) ? wc_clean( $_POST['billing_first_name'] ) : $address['first_name'] ?>">
					</div>
					<div class="input-group">
						<span class="label">
							<label for="billing_last_name"><?php _e( 'Last Name', 'woocommerce' ); ?></label>
						</span>
						<input id="billing_last_name" type="text" autocomplete="off" name="billing_last_name" placeholder="<?php _e( 'Last Name', 'balance' ); ?>" value="<?php echo !empty( $_POST['billing_last_name'] ) ? wc_clean( $_POST['billing_last_name'] ) : $address['last_name'] ?>">
					</div>
					<div class="input-group">
						<span class="label">
							<label for="billing_company"><?php _e( 'Company', 'woocommerce' ); ?></label>
						</span>
						<input id="billing_company" type="text" autocomplete="off" name="billing_company" placeholder="<?php _e( 'Company', 'balance' ); ?>" value="<?php echo !empty( $_POST['billing_company'] ) ? wc_clean( $_POST['billing_company'] ) : $address['company'] ?>">
					</div>
					<div class="input-group">
						<span class="label">
							<label for="billing_address_1"><?php _e( 'Address 1', 'woocommerce' ); ?></label>
						</span>
						<input id="billing_address_1" type="text" autocomplete="off" name="billing_address_1" placeholder="<?php _e( 'Address 1', 'balance' ); ?>" value="<?php echo !empty( $_POST['billing_address_1'] ) ? wc_clean( $_POST['billing_address_1'] ) : $address['address_1'] ?>">
					</div>
					<div class="input-group">
						<span class="label">
							<label for="billing_address_2"><?php _e( 'Address 2', 'woocommerce' ); ?></label>
						</span>
						<input id="billing_address_2" type="text" autocomplete="off" name="billing_address_2" placeholder="<?php _e( 'Address 2', 'balance' ); ?>" value="<?php echo !empty( $_POST['billing_address_2'] ) ? wc_clean( $_POST['billing_address_2'] ) : $address['address_2'] ?>">
					</div>
					<div class="input-group">
						<span class="label">
							<label for="billing_city"><?php _e( 'City', 'woocommerce' ); ?></label>
						</span>
						<input id="billing_city" type="text" autocomplete="off" name="billing_city" placeholder="<?php _e( 'City', 'balance' ); ?>" value="<?php echo !empty( $_POST['billing_city'] ) ? wc_clean( $_POST['billing_city'] ) : $address['city'] ?>">
					</div>
					<div class="input-group">
						<span class="label">
							<label for="billing_postcode"><?php _e( 'Postcode/ZIP', 'woocommerce' ); ?></label>
						</span>
						<input id="billing_postcode" type="text" autocomplete="off" name="billing_postcode" placeholder="<?php _e( 'Postcode', 'balance' ); ?>" value="<?php echo !empty( $_POST['billing_postcode'] ) ? wc_clean( $_POST['billing_postcode'] ) : $address['postcode'] ?>">
					</div>
					<?php
						$countries = WC()->countries->get_allowed_countries();
					?>
					<div class="input-group">
						<span class="label">
							<label for="billing_country"><?php _e( 'Country', 'woocommerce' ); ?></label>
						</span>
						<select name="billing_country" class="select2 form-control inner-select">
							<option value=""><?php _e( '-- Select Country --', 'woocommerce' ); ?></option>
							<?php foreach ($countries as $key => $value) { ?>
								<option value="<?php echo $key; ?>" <?php selected( !empty( $_POST['billing_country'] ) ? wc_clean( $_POST['billing_country'] ) : $address['country'], $key, 1 ); ?>><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</div>
					<?php
						$states = WC()->countries->get_states( 'US' );
					?>
					<div class="input-group">
						<span class="label">
							<label for="billing_state"><?php _e( 'State/County', 'woocommerce' ); ?></label>
						</span>
						<select name="billing_state" class="select2 form-control inner-select">
							<option value=""><?php _e( '-- Select State/County --', 'woocommerce' ); ?></option>
							<?php foreach ($states as $key => $value) { ?>
								<option value="<?php echo $key; ?>" <?php selected( !empty( $_POST['billing_state'] ) ? wc_clean( $_POST['billing_state'] ) : $address['state'], $key, 1 ); ?>><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="input-group">
						<span class="label">
							<label for="billing_phone"><?php _e( 'Telephone', 'woocommerce' ); ?></label>
						</span>
						<input id="billing_phone" type="text" autocomplete="off" name="billing_phone" placeholder="<?php _e( 'Telephone', 'balance' ); ?>" value="<?php echo !empty( $_POST['billing_phone'] ) ? wc_clean( $_POST['billing_phone'] ) : $address['phone'] ?>">
					</div>
					<div class="input-group">
						<span class="label">
							<label for="billing_email"><?php _e( 'Email Address', 'woocommerce' ); ?></label>
						</span>
						<input id="billing_email" type="email" autocomplete="off" name="billing_email" placeholder="<?php _e( 'Your Email', 'balance' ); ?>" value="<?php echo !empty( $_POST['billing_email'] ) ? wc_clean( $_POST['billing_email'] ) : $address['email'] ?>">
					</div>
				</div>
			</fieldset>
			<br>
			<p class="btn-holder">
				<?php wp_nonce_field( 'woocommerce-edit_address' ); ?>
				<input type="submit" class="btn btn-warning" name="save_address" value="<?php esc_attr_e( 'Save Changes', 'woocommerce' ); ?>" />
				<input type="hidden" name="action" value="edit_address" />
				<?php do_action( "woocommerce_after_edit_address_form_billing" ); ?>
			</p>
		</form>
	</div>
	<?php } ?>
	<!-- END BILLING ADDRESS -->
	<!-- PAYMENT METHODS -->
	<!-- <div id="tab3" role="tabpanel" class="tab-panel">
		<form class="edit-account account-form" action="<?php echo $my_account_page_permalink . '?action=view-account-details'; ?>" method="post">
			<fieldset>
			<br><br>
			</fieldset>
		</form>
	</div> -->
	<!-- END PAYMENT METHODS -->
	<!-- MY ORDERS -->
	<?php if ( $my_account_action == 'view-my-orders') { ?>
	<div id="tab4" role="tabpanel" class="tab-panel active">
		<form class="edit-account account-form" action="" method="post">
			<fieldset>
				<?php if ( !empty( $_GET['order_id'] ) ) {
					$order = wc_get_order( $_GET['order_id'] );
				?>
				<?php wc_print_notices(); ?>
				<p class="order-info"><?php printf( __( 'Order #<mark class="order-number">%s</mark> was placed on <mark class="order-date">%s</mark> and is currently <mark class="order-status">%s</mark>.', 'woocommerce' ), $order->get_order_number(), date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ), wc_get_order_status_name( $order->get_status() ) ); ?></p>
				<?php if ( $notes = $order->get_customer_order_notes() ) : ?>
					<h4><?php _e( 'Order Updates', 'woocommerce' ); ?></h4>
					<ol class="commentlist notes">
						<?php foreach ( $notes as $note ) : ?>
						<li class="comment note">
							<div class="comment_container">
								<div class="comment-text">
									<p class="meta"><?php echo date_i18n( __( 'l jS \o\f F Y, h:ia', 'woocommerce' ), strtotime( $note->comment_date ) ); ?></p>
									<div class="description">
										<?php echo wpautop( wptexturize( $note->comment_content ) ); ?>
									</div>
					  				<div class="clear"></div>
					  			</div>
								<div class="clear"></div>
							</div>
						</li>
						<?php endforeach; ?>
					</ol>
					<?php
				endif;
				do_action( 'woocommerce_view_order', $_GET['order_id'] );
				?>
				<a href="<?php echo $my_account_page_permalink . '?action=view-my-orders'; ?>" class="btn btn-warning back-to-all-orders">< Back</a><br><br>
			</form>
		</fieldset>
		<?php
		} else { ?>
			<form class="edit-account account-form" action="<?php echo $my_account_page_permalink . '?action=view-my-orders'; ?>" method="post">
				<fieldset>
				<?php
				$my_orders_columns = apply_filters( 'woocommerce_my_account_my_orders_columns', array(
					'order-number'  => __( 'Order', 'woocommerce' ),
					'order-date'    => __( 'Date', 'woocommerce' ),
					'order-status'  => __( 'Status', 'woocommerce' ),
					'order-total'   => __( 'Total', 'woocommerce' ),
					'order-actions' => '&nbsp;',
				) );

				$customer_orders = get_posts( apply_filters( 'woocommerce_my_account_my_orders_query', array(
					'numberposts' => 9999,
					'meta_key'    => '_customer_user',
					'meta_value'  => get_current_user_id(),
					'post_type'   => wc_get_order_types( 'view-orders' ),
					'post_status' => array_keys( wc_get_order_statuses() )
				) ) );

				if ( $customer_orders ) { ?>
					<p><?php echo apply_filters( 'woocommerce_my_account_my_orders_title', __( 'Recent Orders', 'woocommerce' ) ); ?>:</p>
					<table class="colored">
						<thead>
							<tr>
								<?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
									<th class="<?php echo esc_attr( $column_id ); ?>"><span class="nobr"><?php echo esc_html( $column_name ); ?></span></th>
								<?php endforeach; ?>
							</tr>
						</thead>
						<tbody>
							<?php foreach ( $customer_orders as $customer_order ) :
								$order      = wc_get_order( $customer_order );
								$item_count = $order->get_item_count();
								?>
								<tr class="order">
									<?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
										<td class="<?php echo esc_attr( $column_id ); ?>" <?php if ( 'order-actions' !== $column_id ) { ?>data-header-title="<?php echo esc_attr( $column_name ); ?>"<?php } ?>>
											<?php if ( has_action( 'woocommerce_my_account_my_orders_column_' . $column_id ) ) : ?>
												<?php do_action( 'woocommerce_my_account_my_orders_column_' . $column_id, $order ); ?>

											<?php elseif ( 'order-number' === $column_id ) : ?>
												<a href="<?php echo esc_url( $order->get_view_order_url() ); ?>">
													<?php echo _x( '#', 'hash before order number', 'woocommerce' ) . $order->get_order_number(); ?>
												</a>

											<?php elseif ( 'order-date' === $column_id ) : ?>
												<time datetime="<?php echo date( 'Y-m-d', strtotime( $order->order_date ) ); ?>" title="<?php echo esc_attr( strtotime( $order->order_date ) ); ?>"><?php echo date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ); ?></time>

											<?php elseif ( 'order-status' === $column_id ) : ?>
												<?php echo wc_get_order_status_name( $order->get_status() ); ?>

											<?php elseif ( 'order-total' === $column_id ) : ?>
												<?php echo sprintf( _n( '%s for %s item', '%s for %s items', $item_count, 'woocommerce' ), $order->get_formatted_order_total(), $item_count ); ?>

											<?php elseif ( 'order-actions' === $column_id ) : ?>
												<?php
													$actions = array(
														'pay'    => array(
															'url'  => $order->get_checkout_payment_url(),
															'name' => __( 'Pay', 'woocommerce' )
														),
														'view'   => array(
															'url'  => $order->get_view_order_url(),
															'name' => __( 'View', 'woocommerce' )
														),
														'cancel' => array(
															'url'  => $order->get_cancel_order_url( wc_get_page_permalink( 'myaccount' ) ),
															'name' => __( 'Cancel', 'woocommerce' )
														)
													);

													if ( ! $order->needs_payment() ) {
														unset( $actions['pay'] );
													}

													if ( ! in_array( $order->get_status(), apply_filters( 'woocommerce_valid_order_statuses_for_cancel', array( 'pending', 'failed' ), $order ) ) ) {
														unset( $actions['cancel'] );
													}

													if ( $actions = apply_filters( 'woocommerce_my_account_my_orders_actions', $actions, $order ) ) {
														foreach ( $actions as $key => $action ) {
															echo '<a href="' . esc_url( $action['url'] ) . '" class="' . sanitize_html_class( $key ) . '">' . esc_html( $action['name'] ) . '</a>&nbsp;&nbsp;';
														}
													}
												?>
											<?php endif; ?>
										</td>
									<?php endforeach; ?>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				<?php } else { ?>
					<div class="woocommerce-message woocommerce-message-error"><?php echo __( 'No orders found.', 'woocommerce' ); ?></div><br>
				<?php } ?>
				</fieldset>
			</form>
		 <?php } ?>
	</div>
	<?php } ?>
	<!-- END MY ORDERS -->
</div>
