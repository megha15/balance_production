// page init
jQuery(function() {
    initCustomForms();
    initAccordion();
    initMobileNav();
    initThumbnailSameHeight();
    initFormValidation();
    initSameHeight();
    initSameHeightFinance();
    initButtonRename();
    initSlickGallery();
    initHideModal();
    jQuery('input, textarea').placeholder();
    initFixedScrollHeader();
    initFixedScrollBlock();
    initSimpleQuizChecker();
    // initCheckAll();
    // initAutocomplete();
    // initTagAutocomplete();
    // initDotDotDot();
    initAnchorSelect();
    initModalTab();
    //initFullQuizChecker();
    initFluidVideos();
    initCaptcha();
    initDatepicker();
    initNumeric();
});

// mobile menu init
function initMobileNav() {
    jQuery('body').mobileNav({
        menuActiveClass: 'search-active',
        menuOpener: '.btn-search'
    });
}

// resize videos all around video-holder div
function initFluidVideos() {
    // Find all YouTube & Vimeo videos
    var $ = jQuery;
    var $allVideos = $(".video-holder iframe[src*='//player.vimeo.com'], .video-holder iframe[src*='//www.youtube.com']");
    // Figure out and save aspect ratio for each video
    $allVideos.each(function() {
        $(this)
            .data('aspectRatio', this.height / this.width)
            // and remove the hard coded width/height
            .removeAttr('height')
            .removeAttr('width');
    });

    // When the window is resized
    $(window).resize(function() {
        // Resize all videos according to their own aspect ratio
        $allVideos.each(function() {
            var $el = $(this);
            var newWidth = $el.closest('.video-holder').width();
            $el
                .width(newWidth)
                .height(newWidth * $el.data('aspectRatio'));
        });
        // Kick off one resize to fix all videos on page load
    }).resize();
}

function initSameHeight() {
  jQuery('.report-holder, .same-height-holder').sameHeight({
    elements: '.same-height',
    flexible: true,
    multiLine: true,
    biggestHeight: true
  });
}
function initDotDotDot(){
  jQuery('.dot-holder').dotdotdot({
    watch: true
  });
}

var initCaptchaInterval;

function initCaptcha() {
	initCaptchaInterval = setInterval(captchaCallback, 1000);
}

var captchaCallback = function(){
	if ( jQuery('.g-recaptcha:not(.initialized)').length == 0 ) {
		console.log('interval cleared');
		clearInterval(initCaptchaInterval);
	} else if (typeof window.grecaptcha !== 'undefined') {
		console.log('interval called');
		jQuery('.g-recaptcha:not(.initialized)').each(function(index, el) {
			jQuery(this).addClass('initialized');
			jQuery(this).parent().find('.loading-recaptcha').remove();
			var validateElement = jQuery(this).attr('data-validate');
			grecaptcha.render(el, {
				'sitekey' : '6LdqjBkTAAAAALZ7NB29sJUPmgOFzWqoxzY9hUl_',
				'callback': function () {
					jQuery(validateElement).removeAttr('disabled');
				},
				'expired-callback': function() {
					jQuery(validateElement).attr('disabled', true);
				}
			});
		});
	}
};

function initDatepicker() {
  jQuery('[data-datepicker]').uiDatepicker({
    beforeShow: function(input, inst) {
      setTimeout(function() {
        var jcfOptions = {
          fakeDropInBody: false
        };
        inst.dpDiv.find('select').data('jcf', jcfOptions);
        jcf.replaceAll(inst.dpDiv);
      }, 10);
    },
    onChangeMonthYear: function(year, month, inst) {
      setTimeout(function() {
        var jcfOptions = {
          fakeDropInBody: false
        };
        inst.dpDiv.find('select').data('jcf', jcfOptions);
        jcf.replaceAll(inst.dpDiv);
      }, 10);
    }
  });
}

function initNumeric() {
  $("input[type='number']").keypress(function(e) { filter_non_numeric_keys(e) });
}

function filter_non_numeric_keys(e) {
  var c = e.which || e.keyCode;
  // exclude all alphanum & symbols except [0-9]$.
  if((c >= 33 && c <= 35) || (c >= 37 && c <= 45) || c == 47 || (c >= 58 && c <= 126))
    e.preventDefault();
}

// initialize custom form elements
function initCustomForms() {
  jcf.setOptions('Select', {
    wrapNative: false
  });
  jcf.replaceAll();
}

function initFormValidation() {
  jQuery('.submit-form').formValidation({
    addClassToParent: '.input-group',
    errorClass: 'input-error'
  });
}


// Headline with horizontal line of logos

// initialize slick
function initSlickGallery() {
  jQuery('.slick-slideset').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 2,
    slidesToScroll: 2,
    infinite: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      }
    ]
  });
}
// You Might Also Like

// collapse button rename
function initButtonRename () {
  jQuery('#buttonRename').on('click', function(e) {
    // event.preventDefault();
    var text = jQuery(this).text();
    if (text === 'See More'){
      jQuery(this).text('See Less');
    } else {
      jQuery(this).text('See More');
    }
  });
}
// Headline, blurb

jQuery(function($) {
  $('.height-calc').responsiveEqualHeightGrid();  
});

//M46 module

// Header

// clickable parent nav element

function initClickableNav(){
  jQuery(function($) {
    // $('a.dropdown-toggle').click(function(){
    //   location.href = this.href;
    // });
    // if($(window).width()<768){
    //   console.log('mobile');
    //   // $('a.dropdown-toggle').removeClass('dropdown-toggle')
    // }
  });
}
// accordion menu init
function initAccordion() {
  jQuery('.navbar-nav').slideAccordion({
    opener: '.dropdown-toggle',
    slider: '.slide-drop',
    animSpeed: 300
  });

  jQuery('.aside-filter').slideAccordion({
    opener: 'a.filter-opener',
    slider: '.filter-drop',
    animSpeed: 300
  });
}

// mobile menu init
function initMobileNav() {
  jQuery('body').mobileNav({
    menuActiveClass: 'search-active',
    menuOpener: '.btn-search'
  });

  jQuery('body').mobileNav({
    menuActiveClass: 'filter-active',
    menuOpener: '.filter-drop-opener, .filter-drop-close'
  });
}

// initialize fixed blocks on scroll
function initFixedScrollHeader() {
  jQuery('body').fixedScrollBlock({
    slideBlock: '#header.nav-fixed',
    positionType: 'fixed',
    animSpeed: 0,
    animDelay: 0
  });
}

// hide parrent modal
function initHideModal() {
  jQuery(document).on('show.bs.modal', function (e) {
    console.log("hiding");
    var activeModal = jQuery('[role="dialog"].in');
    if (activeModal.length) {
      activeModal.modal('hide');
    }
  });
}

// modal tab init
function initModalTab() {
  jQuery(document).on('click', 'a[data-toggle="modal"][data-rel-tab]', function(e) {
    e.preventDefault();
    console.log("muh triggered");
    var tabLink = jQuery('a[data-toggle="tab"][href="' + jQuery(this).attr('data-rel-tab') + '"]');
    tabLink.tab('show');
  });
}
// Footer

function initFixedScrollBlock() {
  jQuery('#intro').fixedScrollBlock({
    slideBlock: '#sidebar',
    positionType: 'fixed'
  });
}

function initSimpleQuizChecker() {
  jQuery('form.knowledge-form').submit(function(e){
    e.preventDefault();
    e.stopPropagation();
    var $this = jQuery(this);
    var $checked = $this.find('input:radio:checked')
    // console.log($checked, $checked.attr('name'), $checked.attr('correct') );
    if($checked.attr('correct') == 'correct'){
      jQuery('.note-block.success', $this.parent()).show();
      jQuery('.note-block.fail', $this.parent()).hide();
    }else{
      jQuery('.note-block.success', $this.parent()).hide();
      jQuery('.note-block.fail', $this.parent()).show();
    }
  }).parent().find('.note-block').hide();

}

// Life Stages

// align blocks height
function initThumbnailSameHeight() {
  jQuery('.thumbnail-holder').sameHeight({
    elements: '.same-height-h2',
    flexible: true,
    multiLine: true,
    biggestHeight: true
  });
  jQuery('.thumbnail-holder').sameHeight({
    elements: 'ul.list',
    flexible: true,
    multiLine: true,
    biggestHeight: true
  }); 
}

// Three msgs with names

//M56a module

function initSameHeightFinance() {
  jQuery('.same-height-holder').each(function() {
  	jQuery(this).find('.finance-wrap').responsiveEqualHeightGrid();
  	jQuery(this).find('.height-finance-wrap').responsiveEqualHeightGrid();
  });

}

// Hero with CTA, no carousel

//M17a module

// anchor select init
function initAnchorSelect() {
	jQuery('#chapter-select').on('change', function(e) {
    var new_url = jQuery(this).val();
    window.location.href = new_url;
	});
}

/*
function initFullQuizChecker() {
	jQuery("form#quiz-form").submit(function(e){
		e.preventDefault();
		e.stopPropagation();
		jQuery('.quiz-questions').fadeOut();
		jQuery('.quiz-header').fadeOut();
		//init vars
		var quizId = (jQuery('#quizId') && jQuery('#quizId').val()? jQuery('#quizId').val(): -1);
		var noofquestions = jQuery('.questions > li').length;
		var noofcorrectanswers = (function () {
			var noofcorrectanswers =  0;
			jQuery('.questions > li').each(function() {
				jQuery('input', this).each(function() {
					if ( jQuery(this).is(':checked') && jQuery(this).attr('correct') == 'correct' ) {
						noofcorrectanswers++;
					}
				});
			});

			return noofcorrectanswers;
		})();

		jQuery("html, body").animate({ scrollTop: "0px" }, 200, function() {
			jQuery('.quiz-number-of-correct').html(noofcorrectanswers);
			jQuery('.quiz-number-of-questions').html(noofquestions);
			jQuery('.quiz-percent').html(((noofquestions > 0 ? (noofcorrectanswers / noofquestions) : 0) *100) + '%');
			jQuery('.quiz-results').fadeIn();
		});

		jQuery.ajax('/log-quiz/' + quizId + '/'+ noofcorrectanswers);
	});

	jQuery('.quiz-retry').on('click', function() {
		jQuery('.quiz-questions').fadeIn();
		jQuery('.quiz-header').fadeIn();
		jQuery('.quiz-results').fadeOut();
	});
}//M24 module

*/
//M60 module

//M70-M84 module
// upload field copy the value to dummy label
jQuery(document).ready(function() {
  jQuery('.upload-wrapper input[type=file]').each(function() {
    if ( jQuery(this).val() != '' ) {
      jQuery(this).parents().find('.uploaded-file').html(jQuery(this).val());
    } else {
      jQuery(this).parents().find('.uploaded-file').html(jQuery(this).attr('data-value'));
    }
  });
  jQuery('.upload-wrapper input[type=file]').on('change', function() {
    jQuery(this).parents().find('.uploaded-file').html(jQuery(this).val().replace('C:\\fakepath\\', ''));
    console.log(jQuery(this).parents().find('.uploaded-file'));
  });
});
