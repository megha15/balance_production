<?php

function render_m02_section_calculator( $module_data ) {
	$output = '';

	if ( !empty( $module_data['init'] ) && !empty( $module_data['support'] ) ) {
		// remove the .js extension if exists
		if ( strpos( $module_data['init'], '.js' ) !== false ) {
			$module_data['init'] = str_replace( '.js', '', $module_data['init'] );
		}
		// remove the .js extension if exists
		if ( strpos( $module_data['support'], '.js' ) !== false ) {
			$module_data['support'] = str_replace( '.js', '', $module_data['support'] );
		}
		$output .= '<!-- M02: SECTION -->';
		$output .= '<div class="block">';
		$output .=   '<div class="container">';
		$output .=     '<div class="row">';
		$output .=       '<div class="article">';

		$output .=       '<div class=KJEWrapper>';
		$output .=       '<div class=KJEWidthConstraint>';
		$output .=       '<noscript>';
		$output .=       '<div align=center>';
		$output .=       '  <div align=center id=KJENoJavaScript class=KJENoJavaScript>' . __( 'Javascript is required for this calculator.  If you are using Internet Explorer, you may need to select to \'Allow Blocked Content\' to view this calculator.', 'balance' );
		$output .=       '    <p>';
		$output .=       '      <b>' . __( 'For more information about these these financial calculators please visit', 'balance' ) . ': <a href=http://www.dinkytown.net target=_blank>' . __( 'Financial Calculators', 'balance' ) . '</a>' . __( 'from KJE Computer Solutions, LLC', 'balance' ) . '</b>';
		$output .=       '    </p>';
		$output .=       '  </div>';
		$output .=       '</div>';
		$output .=       '</noscript>';
		$output .=       '<div id="KJEAllContent"></div>';
		$output .=       '<!--';
		$output .=       __( 'Financial Calculators, &copy;1998-2019 KJE Computer Solutions, Inc.', 'balance' );
		$output .=       __( 'For more information please see:', 'balance' );
		$output .=       '<A HREF="http://www.dinkytown.net">http://www.dinkytown.net</A>';
		$output .=       ' -->';
		$output .=		'<link type="text/css" rel="StyleSheet" href="' . get_stylesheet_directory_uri() . '/calculator/2021/KJEnew.css" />';
		$output .=		'<link type="text/css" rel="StyleSheet" href="' . get_stylesheet_directory_uri() . '/calculator/2021/KJESiteSpecific.css" />';
		$output .=       '<script language="JavaScript" type="text/javascript" src="' . get_stylesheet_directory_uri() . '/calculator/2021/KJE.js"></script>';
		$output .=       '<script language="JavaScript" type="text/javascript" src="' . get_stylesheet_directory_uri() . '/js/M02a.js?v=2019"></script>';
		$output .=       '<script language="JavaScript" type="text/javascript" src="' . get_stylesheet_directory_uri() . '/calculator/2021/'.$module_data['init'].'.js"></script>';
		$output .=       '<script language="JavaScript" type="text/javascript" src="' . get_stylesheet_directory_uri() . '/calculator/2021/'.$module_data['support'].'.js"></script>';
		$output .=       '</div>';
		$output .=       '</div>';

		$output .=       '</div>';
		$output .=     '</div>';
		$output .=   '</div>';
		$output .=  '</div>';
		$output .= '<!-- end M02: SECTION -->';
	}

	return stripslashes( $output );
}
