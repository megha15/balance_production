<?php
/**
 * All the fields functions return full HTML string for a certain field. These field functions are called in files of edit modules and they form edit fields to input different kinds of data that are saved to database afterwords.
 *
 * @see /modules folder
 */

/**
 * Helper which builds "links to" field where user can select type of link (internal, external)
 *
 * @param unknown $value_holder          array property containing all the data for links to
 * @param unknown $name_field            string name of the field (array index) for links to
 * @param unknown $class                 string additional special css class we want to add to links to field
 * @param unknown $internal_link_label   string of internal link radiobutton label
 * @param unknown $custom_array_of_pages array of custom elements such as pages, files which is shown on dropdown
 *
 * @return string full HTML string for links to field
 *
 * @todo when changing from Page to Custom radio, it doesn't remember the previous choice. It needs to remember and check which radio is selected
 */
function links_to_field( $value_holder, $name_field, $class = '', $internal_link_label = 'Page', $autocomplete_args = array( 'post_type' => 'any', 'posts_per_page' => -1, 'post_status' => 'publish' ) ) {
	$value_holder = empty( $value_holder ) ? array( 'url_page_id' => null, 'url' => '', 'url_type'=> '' ) : $value_holder;

	$output = '&nbsp;&nbsp;';

	$output .= '<label class="for">';
	$output .= '<input type="radio" class="url_selector ' . $class . '" id="' . $name_field . '[url_type]-0" name="' . $name_field . '[url_type]" value="1" ' . checked( ( !empty( $value_holder['url_type'] ) ? $value_holder['url_type'] : '' ), "1", false ) . '>';
	$output .= $internal_link_label . '&nbsp;&nbsp;&nbsp;&nbsp;</label>';

	$output .= '<label class="for">';
	$output .= '<input type="radio" class="url_selector ' . $class . '" id="' . $name_field . '[url_type]-1" name="' . $name_field . '[url_type]" value="" ' . checked( ( !empty( $value_holder['url_type'] ) ? $value_holder['url_type'] : '' ), "", false ) . '>';
	$output .= __( 'Custom (insert full URL)', 'balance_theme' ) . '&nbsp;&nbsp;&nbsp;&nbsp;</label>';

	$output .= '<br>';
	$output .= '<input type="text" value="' . ( !empty( $value_holder['url'] )  ? stripslashes( $value_holder['url'] ) : '' ) . '" class=" ' . $class . ' ' . ( !empty( $value_holder['url_type'] ) ? 'hidden' : '' ) . ' custom_url" />';
	$output .= multi_autocomplete_field( $value_holder['url_page_id'], $name_field . '[url_page_id]', $autocomplete_args, 1, '', ' page_url ' . ( empty( $value_holder['url_type'] ) ? 'hidden-important' : '' ), false );

	$output .= '<input type="hidden" class="' . $class . ' attribute url_value_holder" name="' . $name_field . '[url]" value="' . stripslashes( $value_holder['url'] ) . '"/>';
	$output .= '<input type="hidden" class="' . $class . ' attribute url_id_holder related-to-autocomplete" name="' . $name_field . '[url_page_id]" value="' . stripslashes( $value_holder['url_page_id'] ) . '"/>';
	$output .= '<br><br>';
	$output .= '<label>' . __( '"onclick" attribute:', 'balance_theme' ) . '</label>';
	$output .= '<textarea class="' . $class . ' not-wp-editor url_onclick_js" name="' . $name_field . '[url_onclick_js]" rows="2">' . ( !empty( $value_holder['url_onclick_js'] ) ? stripslashes( $value_holder['url_onclick_js'] ) : '' ) . '</textarea>';
	return $output;
}

/**
 * Helper which builds "select/dropdown" field where user can select page/post_type or any other elements from array
 *
 * @param unknown $value_holder          array property containing all the data for links to
 * @param unknown $name_field            string name of the field (array index) for links to
 * @param unknown $internal_link_label   string gets appended to the post_type title in the dropdown for easier differentiation between post types
 * @param unknown $custom_array_of_pages array of custom elements such as pages, files which is shown on dropdown
 *
 * @return string full HTML string for dropdown field
 */
function dropdown_field( $value_holder, $name_field, $internal_link_label = '', $custom_array_of_pages = array(), $class= "", $display_empty = true, $overwrite_array_of_pages = false ) {
	global $array_of_pages;
	if ( !empty( $custom_array_of_pages ) || $overwrite_array_of_pages ) {
		$array_of_pages = $custom_array_of_pages;
	}
	$output = '';
	$output .= '<select id="' . $name_field . '" class="single-drp ' . $class . '" name="' . $name_field . '">';
	if ( $display_empty ) {
		$output .= '<option value="">-- none --</option>';
	}
	foreach ( $array_of_pages as $page ) {
		$output .= '<option value="' . $page['id'] . '"' . selected( $page['id'], $value_holder, false ) . '>' . $page['title'] . ( !empty( $internal_link_label ) ? ' (' . $internal_link_label . ')' : '' )  . '</option>';
	}
	$output .= '</select>';
	return $output;
}

/**
 * Helper which builds upload field where user can select/upload media
 *
 * @param unknown $value_holder array property containing all the data for upload field
 * @param unknown $name_field   string name of the field (array index) for upload field
 * @param unknown $class        string additional special css class we want to add to upload field
 *
 * @return string full HTML string for upload field
 */
function upload_field(  $value_holder, $name_field, $class = '', $upload_type = 'Image', $library_type = 'image' ) {
	$output = '';
	$data_library_type = empty( $library_type ) ? '' : 'data-library-type="'.$library_type.'"';
	$output .= '<div class="' . $class . ' upload-wrapper ' . ( !empty( $value_holder[$library_type]['filename'] ) ? 'has-image' : '' ) . '" '.$data_library_type.'>';
	$output .= '<input type="hidden" name="' . $name_field . '['.$library_type.'][attachment_id]" value="' . ( !empty( $value_holder[$library_type]['attachment_id'] ) ?  $value_holder[$library_type]['attachment_id'] : '' ) . '">';
	$output .= '<input type="hidden" name="' . $name_field . '['.$library_type.'][filename]" value="' . ( !empty( $value_holder[$library_type]['filename'] ) ?  $value_holder[$library_type]['filename'] : '' ) . '">';
	$output .= '<input type="hidden" name="' . $name_field . '['.$library_type.'][fullpath]" value="' . ( !empty( $value_holder[$library_type]['fullpath'] ) ?  $value_holder[$library_type]['fullpath'] : '' ) . '">';
	$output .= '<input type="hidden" name="' . $name_field . '['.$library_type.'][filesize]" value="' . ( !empty( $value_holder[$library_type]['filesize'] ) ?  $value_holder[$library_type]['filesize'] : '' ) . '">';
	$output .= '<input type="hidden" name="' . $name_field . '['.$library_type.'][editurl]" value="' . ( !empty( $value_holder[$library_type]['editurl'] ) ?  $value_holder[$library_type]['editurl'] : '' ) . '">';
	$output .= '<input type="hidden" name="' . $name_field . '['.$library_type.'][type]" value="' . ( !empty( $value_holder[$library_type]['type'] ) ?  $value_holder[$library_type]['type'] : '' ) . '">';
	$output .= '<input type="hidden" name="' . $name_field . '['.$library_type.'][subtype]" value="' . ( !empty( $value_holder[$library_type]['type'] ) ?  $value_holder[$library_type]['subtype'] : '' ) . '">';
	if ( !empty( $value_holder[$library_type]['filename'] ) ) {
		$img_src = '';
		$img_class = '';
		$mime_type = get_post_mime_type( $value_holder[$library_type]['attachment_id'] );
		if ( strpos( $mime_type, 'image' ) !== false ) {
			$img_src = wp_get_attachment_url( $value_holder[$library_type]['attachment_id'] );
		} else {
			$img_src = wp_mime_type_icon( $mime_type );
			$img_class = 'mime-icon';
		}
		$output .= '<img class="' . $img_class . '" data-dummy-src="' . get_stylesheet_directory_uri() . '/css/placeholder.png" src="' . $img_src . '">';
		$output .= '<input type="button" class="button button-primary btn-upload" data-toggle-title="' . __( 'Upload / Select image', 'balance_theme' ) . '" value="' . __( 'Change '.$upload_type, 'balance_theme' ) . '" />';
		$output .= '<input type="button" class="button btn-upload-remove" value="X">';
		$output .= '<pre class="upload-img-data">';
		$output .= 'File: ' . $value_holder[$library_type]['filename'];
		$output .= '<br>Path: ' . $value_holder[$library_type]['fullpath'];
		if ( !empty( $value_holder[$library_type]['filesize'] ) ) {
			$output .= '<br>Size: ' . $value_holder[$library_type]['filesize'];
		}
		if ( !empty( $value_holder[$library_type]['type'] ) && !empty( $value_holder[$library_type]['subtype'] ) ) {
			$output .= '<br>Type: ' . $value_holder[$library_type]['type'] . ' (' . $value_holder[$library_type]['subtype'] . ')';
		}
		if ( !empty( $value_holder[$library_type]['editurl'] ) ) {
			$output .= '<br><a href="' .  $value_holder[$library_type]['editurl'] . '" target="blank">Edit</a>';
		}
		$output .=  '</pre>';
	} else {
		$output .= '<img data-dummy-src="' . get_stylesheet_directory_uri() . '/css/placeholder.png">';
		$output .= '<input type="button" class="button button-primary btn-upload" data-toggle-title="' . __( 'Change '.$upload_type, 'balance_theme' ) . '" value="' . __( 'Upload / Select '.$upload_type, 'balance_theme' ) . '" />';
		$output .= '<input type="button" class="button btn-upload-remove" value="X">';
		$output .= '<pre class="upload-img-data hidden"></pre>';
	}
	$output .= '</div>';
	return $output;
}

/**
 * Helper which builds "textarea" field where user can input text
 *
 * @param unknown $value_holder      array property containing all the data for textarea
 * @param unknown $name_field        string name of the field (array index) for textarea
 * @param unknown $is_wysiwyg_editor boolean which determine if wysiwyg editor of this textarea
 * @param unknown $rows              integer for number of rows of textarea
 * @param unknown $default_value     string default value of textarea
 * @param unknown $class             string additional special css class we want to add to textarea
 * @param unknown $settings          array for include additional settings of textarea
 * @param unknown $id                string for custom id of textarea
 *
 * @return string full HTML string for textarea
 */
function textarea_field( $value_holder, $name_field, $is_wysiwyg_editor = true, $rows = 0, $default_value = '', $class = '', $settings = array(), $id = '' ) {
	$output = '';

	if ( $is_wysiwyg_editor ) {
		$output .= '<textarea id="'. ( !empty( $id ) ? $id : hyphenize( $name_field ) ) .'" class="' . $class . ' wp-tmce attribute" ' . ( $rows > 0 ? 'rows="' . $rows . '"' : '' ) . ' name="' . $name_field . '" data-settings="'. htmlentities( json_encode( $settings ), ENT_QUOTES, 'UTF-8' ) .'">' . ( !empty( $value_holder ) ? stripslashes( $value_holder ) : $default_value ) . '</textarea>';
	}
	else {
		$output .= '<textarea id="'. ( !empty( $id ) ? $id : hyphenize( $name_field ) ) .'" class="' . $class . ' attribute" ' . ( $rows > 0 ? 'rows="' . $rows . '"' : '' ) . ' name="' . $name_field . '">' . ( !empty( $value_holder ) ? stripslashes( $value_holder ) : $default_value ) . '</textarea>';
	}
	return $output;
}

/**
 * Helper which builds "text field" field where user can input text
 *
 * @param unknown $value_holder  array property containing all the data for text field
 * @param unknown $name_field    string name of the field (array index) for text field
 * @param unknown $default_value string default value of text field
 * @param unknown $class         string additional special css class we want to add to text field
 *
 * @return string full HTML string for text field
 */
function text_field( $value_holder, $name_field, $default_value = '', $class = '', $placeholder = '', $type = 'text' ) {
	$output = '';
	$output .= '<input type="' . $type . '" class="' . $class . '" name="' . $name_field . '" value="' . ( !empty( $value_holder ) ? stripslashes( $value_holder ) : $default_value ) . '" ' . ( !empty( $placeholder ) ? 'placeholder="' . $placeholder . '"' : '' ) . ' />';
	return $output;
}

function email_field( $value_holder, $name_field, $default_value = '', $class = '', $placeholder = '' ) {
	$output = '';
	$output .= text_field( $value_holder, $name_field, $default_value, $class, $placeholder, 'email' );
	return $output;
}

function number_field( $value_holder, $name_field, $default_value = '', $class = '', $placeholder = '', $min = '', $max = '', $step = '' ) {
  $output = '';
  $output .= '<input type="number" class="' . $class . '" name="' . $name_field . '" min="' . $min . '" max="' . $max . '" step="' . $step . '" value="' . ( !empty( $value_holder ) ? stripslashes( $value_holder ) : $default_value ) . '" ' . ( !empty( $placeholder ) ? 'placeholder="' . $placeholder . '"' : '' ) . ' />';
  return $output;
}


/**
 * Helper which builds "instruction text"
 *
 * @param unknown $value_holder array will be skipped
 * @param unknown $name_field   will be skipped
 * @param unknown $class        accepts a string which will be wrapped around the label
 * @param unknown $content      will be content injected into field
 *
 */

function instruction_field( $value_holder , $name_field , $class= '', $content = '' ) {
	$output = '';
	//$output .= '        <label><b>'.__('Logos', balance).':</b></label><br/>';
	$output .= '        <label class="' . $class . '"><b><small>'.__( $content, 'balance' ).'</small></b></label>';
	$output .= '        <br/>';
	return $output;
}

/**
 * Helper which builds "checkbox field" field where user can input text
 *
 * @param unknown $value_holder  array property containing all the data for checkbox field
 * @param unknown $name_field    string name of the field (array index) for checkbox field
 * @param unknown $default_value boolean default checked state of checkbox field
 * @param unknown $class         string additional special css class we want to add to the checkbox field
 *
 * @return string full HTML string for checkbox field
 */
function checkbox_field( $value_holder, $name_field, $default_value = false, $class = '' ) {
	$output = '&nbsp;<input type="checkbox" name="'.$name_field.'" value="1" ' . checked( $value_holder, 1, false ) . ' />';

	return $output;
}


/**
 * Helper which builds automplete text field
 *
 * @param unknown $value_holder      array property containing all the data for autocomplete field
 * @param unknown $name_field        string name of the field (array index) for autocomplete field
 * @param unknown $autocomplete_args array of arguments for get_posts function
 * @param unknown $default_value     string of ids separated by commas for prepopulated values
 *
 * @return string full HTML string for autocomplete field
 */
function multi_autocomplete_field( $value_holder, $name_field, $autocomplete_args = array( 'post_type' => 'any', 'posts_per_page' => -1, 'post_status' => 'publish' ), $maxelements = 0, $default_value = '', $class = '', $create_hidden_field = true ) {
	global $post_id;
	$output = '';

	$selected_items = '';
	$exclude_array = array( 'exclude' => $post_id );
	$autocomplete_args = array_merge( $autocomplete_args, $exclude_array  );
	if ( strlen( $value_holder ) > 0 ) {

		$arr_values = explode( ',', $value_holder );

		foreach ( $arr_values as $arr_value ) {
			$selected_items .= '<option value="' . $arr_value . '" selected="selected">' . get_the_title( $arr_value ) . ' <span class="marker-label">(' . ucfirst( get_post_type_object( get_post_type( $arr_value ) )->labels->singular_name ) . ')</span></option>';
		}
	}
	$output .= '<select class="multiautosuggestfield ' . $class . '" multiple="multiple" class="tokenize-sample" data-maxelements="'.$maxelements.'" data-related="' . $name_field . '" data-args=' . "'" . serialize( $autocomplete_args ) . "'" . '>'.$selected_items.'</select>';
	if ( $create_hidden_field ) {
		$output .= '<input type="hidden" class="related-to-autocomplete" name="' . $name_field . '" value="' . ( !empty( $value_holder ) ? stripslashes( $value_holder ) : $default_value ) . '"  />';
	}

	return $output;
}

/**
 * Helper which builds "date picker" field where user can select date
 *
 * @param unknown $value_holder  array property containing all the data for date field
 * @param unknown $name_field    string name of the field (array index) for date field
 * @param unknown $default_value string default value of date field
 * @param unknown $class         string additional special css class we want to add to date field
 *
 * @return string full HTML string for date field
 */
function datepicker_field( $value_holder, $name_field, $default_value = '', $class = '' ) {
	$default_value = empty( $default_value ) ? current_time( 'm/d/Y' ) : $default_value;
	return text_field( $value_holder, $name_field, $default_value, 'custom-datepicker ' . $class );
}

/**
 * Helper which builds "radiobutton list" field where user can select option
 *
 * @param unknown $value_holder  array property containing all the data for radiobutton option
 * @param unknown $name_field    string name of the field (array index) for radiobutton option
 * @param unknown $options_array array for all the choices
 * @param unknown $default_value string default value of radiobutton options
 * @param unknown $inline        boolean for determing if all the options are in new line
 *
 * @return string full HTML string for radiobutton option
 */
function radiobuttonlist_field( $value_holder, $name_field, $options_array, $default_value, $inline = false ) {
	$output = '';
	foreach ( $options_array as $key => $option ) {
		$output .= ( $inline == false ? "<br>" : "" );
		$output .= '<label class="for">';
		$output .= '<input type="radio" id="'.$name_field.'-'.$key.'" name="'.$name_field.'" value="'.$key.'" ' . checked( ( !empty( $value_holder )  ? $value_holder :  $default_value ), $key, false ) . ' />';
		$output .= $option . '&nbsp;&nbsp;&nbsp;&nbsp;</label>';
	}
	return $output;
}

/**
 * Helper which builds "checkbox list" field where user can select options
 * Do not use checkboxlist_field if you have different value and option text especially if you need HTML for option text (it is not just plain text)
 * use checkboxlist_arrayed_field() instead
 *
 * @param unknown $value_holder   array property containing all the data for checkbox option
 * @param unknown $name_field     string name of the field (array index) for checkbox option
 * @param unknown $options_array  array for all the choices
 * @param unknown $default_values array of default checked values
 * @param unknown $inline         boolean for determing if all the options are in new line
 *
 * @return string full HTML string for checkbox options
 */
function checkboxlist_field( $value_holder, $name_field, $options_array, $default_values = array() , $inline = true ) {
	$output = '';
	$checkboxlist = '';
	foreach ( $options_array as $key => $option ) {
		$checkboxlist .= '<label class="for">';
		$checkboxlist .= '<input type="checkbox" id="'.$name_field.'['.$key.']" name="'.$name_field. '['.$key.']" value="'.$option.'" ' . checked( ( !empty( $value_holder )  ? ( !empty( $value_holder[ $key ] ) ? $value_holder[ $key ] : null ) : ( !empty( $default_values ) ? $default_values[ $key ] : null ) ), $option, false ) . ' />';
		$checkboxlist .= $option . '&nbsp;&nbsp;&nbsp;&nbsp;</label>';
	}
	$output .= "<div class='checkbox-list" . ( $inline ? ' inline' : '' ) ."'>" . $checkboxlist . "</div>";
	return $output;
}

/**
 * Helper which builds "checkbox list" field where user can select options
 * use this function when:  HTML for option text /  Different value and option text needed
 *
 * @param unknown $value_holder   array property containing all the data for checkbox option
 * @param unknown $name_field     string name of the field (array index) for checkbox option
 * @param unknown $options_array  array for all the choices; it has index [0] for value and [1] for text
 * @param unknown $default_values array of default checked values
 * @param unknown $inline         boolean for determing if all the options are in new line
 *
 * @return string full HTML string for checkbox options
 */
function checkboxlist_arrayed_field( $value_holder, $name_field, $options_array, $default_values = array() , $inline = true ) {
	$output = '';
	$checkboxlist = '';
	foreach ( $options_array as $key => $option ) {
		$checkboxlist .= '<label class="for">';
		$checkboxlist .= '<input type="checkbox" id="'.$name_field.'['.$key.']" name="'.$name_field. '['.$key.']" value="'.$option[0].'" ' . checked( ( !empty( $value_holder )  ? ( !empty( $value_holder[ $key ] ) ? $value_holder[ $key ] : null ) : ( !empty( $default_values ) ? $default_values[ $key ] : null ) ), $option[0], false ) . ' />';
		$checkboxlist .= $option[1] . '&nbsp;&nbsp;&nbsp;&nbsp;</label>';
	}
	$output .= "<div class='checkbox-list" . ( $inline ? ' inline' : '' ) ."'>" . $checkboxlist . "</div>";
	return $output;
}


/**
 * Helper which builds "checkbox or radiobutton list" field where user can select options from post types
 *
 * @param unknown $value_holder   array property containing all the data for selected post types
 * @param unknown $name_field     string name of the field (array index)
 * @param unknown $post_types     array of allowed post types
 * @param unknown $default_values array of default values
 * @param unknown $inline         boolean for determing if all the options are in new line
 * @param unknown $exclude        array posty types that are excluded from query
 * @param unknown $mode           string 'checkbox' or 'radiolist' that defines what kind of choices
 *
 * @return string full HTML string for post types options
 */
function post_types_field( $value_holder, $name_field, $post_types = null, $default_values = null, $inline = false, $exclude = array(), $mode = 'checkbox' ) {

	if ( empty( $post_types ) ) {
		$args = array(
			'_builtin' => false
		);
		$post_types = get_post_types( $args, 'objects', 'and' );
	}

	$options = array();
	foreach ( $post_types as $key => $post_type ) {
		if ( TRUE === in_array( $post_type->name, $exclude ) )
			continue;

		$options[$post_type->name] = $post_type->label;
	}

	$checkboxlist_value_holder = !empty( $value_holder['filter_items'] ) ? $value_holder['filter_items'] : '';
	$preselect_value_holder = !empty( $value_holder['preselected_item'] ) ? $value_holder['preselected_item'] : '';

	if ( $mode == 'radiolist' ) {
		//TODO: make a default selected value
		$output = radiobuttonlist_field( $value_holder, $name_field, $options, $default_values, $inline );
	}
	else {
		$output = checkboxlist_field( $checkboxlist_value_holder, $name_field . '[filter_items]', $options, $default_values, $inline );
		$output .= '<br/>Preselected item:<br/>';
		$output .= '<select name="'. $name_field . '[preselected_item]" class="preselected_tax" >';
		$output .= '<option value="">None</option>';
		foreach ( $options as $key => $option ) {
			$output .= '<option value="'. $option .'" '. ( $preselect_value_holder == $option ? 'selected="selected"': '' ) .'>'. $option .'</option>';
		}
		$output .= '</select><br/>';
	}

	return $output;
}

/**
 * Helper which builds "checkbox or radiobutton list" field where user can select options from taxonomies
 *
 * @param unknown $value_holder   array property containing all the data for selected taxonomies
 * @param unknown $name_field     string name of the field (array index)
 * @param unknown $taxonomies     array of allowed taxonomies
 * @param unknown $default_values array of default values
 * @param unknown $inline         boolean for determing if all the options are in new line
 * @param unknown $mode           string 'checkbox' or 'radiolist' that defines what kind of choices
 *
 * @return string full HTML string for taxonomies options
 *
 * @todo make a default selected value for radiobutton
 */
function taxonomies_terms_field( $value_holder, $name_field, $taxonomies = null, $default_values = null, $inline = false, $mode = 'checkbox' ) {
	if ( empty( $taxonomies ) ) {
		$taxonomies = array( 'category' );
	}
	$args = array(
		'orderby'           => 'name',
		'order'             => 'ASC',
		'hide_empty'        => false,
	);
	$terms = get_terms( $taxonomies, $args );

	$options = array();
	foreach ( $terms as $key => $item ) {
		$options[$item->term_id] = $item->name;
	}

	if ( $mode == 'checkbox' ) {
		$checkboxlist_value_holder = !empty( $value_holder['filter_items'] ) ? $value_holder['filter_items'] : '';
		$preselect_value_holder = !empty( $value_holder['preselected_item'] ) ? $value_holder['preselected_item'] : '';

		$output = "No taxonomy items found.";
		if ( !empty( $options ) ) {
			$output = checkboxlist_field( $checkboxlist_value_holder , $name_field . '[filter_items]', $options, $default_values, $inline );
			$output .= '<br/>Preselected item:<br/>';
			$output .= '<select name="'. $name_field . '[preselected_item]" class="preselected_tax" >';
			$output .= '<option value="">None</option>';
			foreach ( $options as $key => $option ) {
				$output .= '<option value="'. $option .'" '. ( $preselect_value_holder == $option ? 'selected="selected"': '' ) .'>'. $option .'</option>';
			}
			$output .= '</select><br/>';
		}
	} else {
		//TODO: make a default selected value
		$output = radiobuttonlist_field( $value_holder, $name_field, $options, $default_values, $inline );
	}

	return $output;
}
/**
 * Helper which builds a simple edit module
 *
 * @param unknown $key          integer key for the edit module
 * @param unknown $module       string name of the module  (m01)
 * @param unknown $fields       array of fields to list in the edit module
 * @param unknown $data         array of module data
 * @param unknown $module_title string Module title to display in wp-admin
 *
 * @return string full HTML string for edit module
 *
 */
function simple_edit_module( $key, $module, $fields, $data, $module_title = false, $visible_on = "all" ) {
	global $theme_namespace;
	$module_title = $module_title === false ? 'NO TITLE SET - '.$module : $module_title;

	foreach ( $fields as $field ) {
		if ( !empty($field['name']) && empty( $data[$module.'_module'][$key][$field['name']] ) ) {
			$data = init_array_on_var_and_array_key($data, $module.'_module');
			if (!is_array($data[$module.'_module'][ $key ])) {
				$data[$module.'_module'][ $key ] = array();
			}
			$data[$module.'_module'][ $key ][$field['name']] = $field['default_value'];
		}
	}

	$output = '';
	$output .= '<a name="'.$module.'-module-wrapper-'. $key .'"></a>';
	$output .= '<div class="module-wrapper '.$module.'-module-module-wrapper-'. $key .'" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
	$output .= '  <div class="postbox postbox-custom '.$module.'-module-list-wrapper-'. $key .'">';
	$output .= '    <h3>'. $module_title . ( intval( $key ) > 0 ? ' #'.( intval( $key )+1 ) : '' ) .'<a class="description fright section-expander is-collapsed" data-toggle-title="'. __( 'Collapse', $theme_namespace ) .'" href="javascript:;">'. __( 'Expand', $theme_namespace ) .'</a></h3>';
	$output .= '    <div class="inside hidden">';

	$output .= render_form( $fields, $data[$module.'_module'][$key], $module.'_module['.$key.']' );

	$output .= '    </div>';
	$output .= '  </div>';
	$output .= '</div>';
	return $output;
}
function render_form( $fields, $data, $base_name_prefix ) {
	$output = '';
	foreach ( $fields as $field ) {
		if ( $field['type'] == 'spacer' ) {
			$output.= '<p><br /></p>';
		}else if ( $field['type'] == 'repeater' ) {
			$output.= render_repeater( $field, $data[$field['name']], $base_name_prefix );
		} else {
			$output.= render_field( $field, $data, $base_name_prefix );
		}
	}
	return $output;
}
function render_field( $field, $data, $base_name_prefix ) {
	global $theme_namespace;
	$output = '';
	if ( !empty( $field['type'] ) && !empty( $field['name'] ) && function_exists( $field['type'].'_field' ) ) {
		$field_function = $field['type'].'_field';
		$output .= '      <p>';
		if ( !empty( $field['label'] ) ) {
			$output .= '       <label><b>'. __( $field['label'], $theme_namespace ) .':</b></label>';
		}

		if ( empty( $data[$field['name']] ) ) {
			$data[$field['name']] = '';
		}

		$params = array(
			$data[ $field['name'] ],
			$base_name_prefix.'['.$field['name'].']'
		);

		$additional_params = !empty( $field['additional_params'] ) ? $field['additional_params'] : array();
		$params = array_merge( $params, $additional_params );
		$output .= call_user_func_array( $field_function, $params );

		$output .= '      </p>';
	}
	return $output;
}

function render_repeater( $repeater, $data = array(), $base_name_prefix ) {
	global $theme_namespace;
	$output = '';
	if ( !empty( $repeater['title'] ) ) {
		$output .= '        <label><b>'. __( $repeater['title'], $theme_namespace ) .':</b></label><br/>';
		$output .= '        <label><b><small>'. __( 'Change order by dragging items up & down.', $theme_namespace ) .'</small></b></label>';
		$output .= '        <br/>';
	}
	$maxAttr = !empty( $repeater['max'] ) ? 'data-max='.$repeater['max'] : '';
	$output .= '<div class="items-list" '.$maxAttr.'>';

	if ( empty( $data ) || !is_array( $data ) ) {
		$data = array( array() );
		foreach ( $repeater['fields'] as $field ) {
			$data[0][$field['name']] = $field['default_value'];
		}
	}

	foreach ( $data as $item_key => $item ) {
		$output .= '<div class="item">';
		$output .= '  <h4 class="sortable-item">';
		$output .= __( $repeater['label'], $theme_namespace );
		$output .= '    #<span class="counter">'. ( $item_key + 1 ) .'</span>';
		$output .= '    <a class="remove-item description fright">'. __( 'Remove Item', $theme_namespace ) .'</a>&nbsp;';
		$output .= '    <a class="collapse-item description fright" data-toggle-title="'. __( 'Expand Item', $theme_namespace ) .'">'. __( 'Collapse Item', $theme_namespace ) .'</a>';
		$output .= '  </h4>';

		$output .= render_form( $repeater['fields'], $item, $base_name_prefix.'['.$repeater['name'].']['.$item_key.']' );

		$output .= '</div>';
	}
	if(empty( $repeater['max']) || $repeater['max'] != 1){
		$output .= '  <input type="button" class="button add-new-item" value="'. __( 'Add New', $theme_namespace ) .' ' . $repeater['label'] . '" />';
	}
	$output .= '</div>';

	return $output;
}
?>
