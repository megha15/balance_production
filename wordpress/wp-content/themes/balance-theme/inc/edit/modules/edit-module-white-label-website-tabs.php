
<?php
/*
 *  WP Edit module: White Label Websites Tabs
 *  Description: Module with tabs of the credit union site administration. On change only the modules of the section gets displayed
 */
function white_label_websites_tabs_module_form() {

		if (!session_id()) {
			session_start();
		}
		if(isset($_SESSION['ptab']))
		$ptab = $_SESSION['ptab'];
		else $ptab='';
		if($ptab==8) {
			$cls = 'nav-tab nav-tab-active'; 
			$cls_general = 'nav-tab'; 
		}	
		else{
			$cls='nav-tab';
			$cls_general = 'nav-tab nav-tab-active'; 
		}
  $output = '';
  $output .= '<h2 class="nav-tab-wrapper">';
    $output .= '<a href="#" class="nav-tab nav-tab-active" id="general-tab" data-tab="tab-0">General</a>';
    $output .= '<a href="#" class="nav-tab" data-tab="tab-1">Homepage</a>';
    $output .= '<a href="#" class="nav-tab" data-tab="tab-2">Contact Us Page</a>';
    $output .= '<a href="#" class="nav-tab" data-tab="tab-3">Resources Page</a>';
    $output .= '<a href="#" class="nav-tab" data-tab="tab-4">Programs Page</a>';
    $output .= '<a href="#" class="nav-tab" data-tab="tab-5">Webinars Page</a>';
    $output .= '<a href="#" class="nav-tab" data-tab="tab-6">Administrators</a>';
    $output .= '<a href="#" class="nav-tab" data-tab="tab-7">Settings</a>';
    $output .= '<a href="#" class="'.$cls.'" data-tab="tab-8">Past Certificates</a>';
    $output .= '<a href="#" class="nav-tab" data-tab="tab-9">Messages</a>';
    $output .= '<a href="#" class="nav-tab" data-tab="tab-10">Chat</a>';
  $output .= '</h2>';
  
  if($ptab==8) {
	$output .= '<script>function hide_tab(){jQuery("#general-tab").removeClass("nav-tab-active")}';
	$output .='jQuery(document).ready(function() {hide_tab();});';
	$output .= '</script>';
	}
 
  return $output;
}
?>
