<?php
/*
 *  WP Edit module: M5
 *  Description: Module with downloadable content for resources.
 */

function m5_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings) {
  global $data;

  $output = simple_edit_module($key, 'm5', array(
    array(
      'type' => 'text',
      'name' => 'title',
      'label' => 'Title',
      'default_value' => ''
    ),
    array(
      'type' => 'textarea',
      'name' => 'copy',
      'label' => 'Copy',
      'default_value' => '',
      'additional_params' => array(
        true,
        5,
        '',
        '',
        $custom_settings
      )
    ),
    array(
      'type' => 'upload',
      'name' => 'resource',
      'label' => 'resource',
      'default_value' => '',
      'additional_params' => array(
        '',
        'mp3',
        'audio/mpeg'
      )
    ),
    array(
      'type' => 'text',
      'name' => 'cta',
      'label' => 'Cta text',
      'default_value' => 'Download'
    )
  ), $data, $module_title, $visible_on);


  return $output;

}