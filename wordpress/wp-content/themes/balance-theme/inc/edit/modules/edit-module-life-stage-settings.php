<?php
/*
 *  WP Edit module: Life Stage Settings
 */
function life_stage_settings_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array() ) {
  global $data;

  if( empty( $data['life_stage_settings'] ) ){
    $data = init_array_on_var_and_array_key($data, 'life_stage_settings');
    $data['life_stage_settings'][ $key ] = array(
      'accessgroups' => '',
    );
  }

  $autocomplete_args_programs = array(
  'post_type' => 'access_group',
  'posts_per_page' => -1,
  'post_status' => 'publish'
  );

  $output = '';
  $output .= '<a name="life-stage-settings-module-wrapper-'. $key .'"></a>';
  $output .= '<div class="module-wrapper life-stage-settings-module-wrapper-'. $key .' hidden" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
  $output .= '  <div class="postbox postbox-custom wlw-programs-module-list-wrapper-'. $key .'">';
  $output .= '    <h3>'. $module_title . (intval($key) > 0 ? ' #'.(intval($key)+1) : '') .'<a class="description fright section-expander is-expanded" data-toggle-title="'. __( 'Expand', 'balance' ) .'" href="javascript:;">'. __( 'Collapse', 'balance' ) .'</a></h3>';
  $output .= '    <div class="inside">';

  $output .= '        <p>';
  $output .= '          <label><b>'. __( 'Resources Groups linked to Life Stage or Life Experience', 'balance' ) .':</b></label>';
  $output .=              multi_autocomplete_field( $data['life_stage_settings'][ $key ]['accessgroups'], 'life_stage_settings['.$key.'][accessgroups]', $autocomplete_args_programs );
  $output .= '        </p>';

  $output .= '    </div>';
  $output .= '  </div>';
  $output .= '</div>';
  return $output;
}
?>
