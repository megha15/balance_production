<?php
/*
 *  WP Edit module: M6
 *  Description: Module with accordion. M06 – Accordion
 */

function m6_module_form( $key, $visible_on = 'all', $module_title = '') {
  global $data;

  $output = simple_edit_module($key, 'm6', array(
    array(
      'type' => 'text',
      'name' => 'title',
      'default_value' => '',
      'label' => 'Title'
    ),
    array(
      'type' => 'repeater',
      'name' => 'bullets',
      'label' => 'Item',
      'default_value' => array(),
      'fields' => array(
        array(
          'type' => 'text',
          'name' => 'copy',
          'label' => 'Bullet copy',
          'default_value' => ''
        )
      )
    )
  ), $data, $module_title, $visible_on);

  return $output;

}