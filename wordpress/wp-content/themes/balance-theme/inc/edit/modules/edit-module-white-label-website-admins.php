<?php
/*
 *  WP Edit module: White Label Websites Admins
 */
function white_label_websites_admins_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array() ) {
	global $data, $wpdb, $post;

	// read the existing data from the whitelabel_admins table.
	$prefix = $wpdb->prefix;
	$post_table_id = $prefix . 'post_id';
	$white_label_websites_table_name = $prefix . 'white_label_websites';
	$white_label_website_id = $wpdb->get_var( "SELECT white_label_website_id FROM $white_label_websites_table_name WHERE $post_table_id = '$post->ID';" );
	$results = $wpdb->get_results( "SELECT * FROM whitelabel_admins WHERE  white_label_website_id = '$white_label_website_id'", OBJECT );
	foreach ($results as $result_key => $result_value) {
		$data['wlw_admins_module'][$key]['admin'][$result_key] = array(
			'email' => $result_value->email
		);
	}

	$output = simple_edit_module( $key, 'wlw_admins', array(
			array(
				'type' => 'repeater',
				'name' => 'admin',
				'label' => 'Admin',
				'default_value' => array(),
				'fields' => array(
					array(
						'type' => 'email',
						'name' => 'email',
						'label' => 'Email',
						'default_value' => ''
					),
				)
			)
		), $data, $module_title, $visible_on );
		
		

		if (!session_id()) {
			session_start();
		}
		
		$page = $_SESSION['pagedvl'] ;
		
		$passing_percent = $data['passing_percent'];
		$rec_per_page = 30;
		$page =$page-1;
		$start = $page*$rec_per_page;
		
		$domain = $wpdb->get_var( "SELECT domain FROM $white_label_websites_table_name WHERE $post_table_id = '$post->ID';" );
		$pdf_file_path = 'http://'.$domain.'/uploads/';
		
		$sqlPastQuizesTotal = "SELECT COUNT(r.ID) AS cnt FROM wp_quiz_results r left join wp_quiz_certificates c on r.ID=c.quiz_result_id where r.wlwid='$white_label_website_id' AND r.timestamp BETWEEN NOW() - INTERVAL 90 DAY AND NOW()";
		$total_records = $wpdb->get_var($sqlPastQuizesTotal);
		$total_pages = ceil($total_records / $rec_per_page);
		
		
		$sqlPastQuizes = "SELECT r.*,crtf_name FROM wp_quiz_results r left join wp_quiz_certificates c on r.ID=c.quiz_result_id where r.wlwid='$white_label_website_id' AND r.timestamp BETWEEN NOW() - INTERVAL 90 DAY AND NOW() ORDER BY ID DESC LIMIT $start, $rec_per_page";
		$resultsPastQuizes = $wpdb->get_results( $sqlPastQuizes, OBJECT );

		 //if(!empty($resultsPastQuizes)){
		

		//$pagedvl = $wp_session['pagedvl'];
		$output .='<div class="module-wrapper wlw_admins-module-module-wrapper-0" data-visible-on="tab-8">';
		$output .='<div style="text-align:center;font-size:15px;margin-bottom: 10px;font-weight: bold;" >[Quiz results are available for 90 days]</div>';
		$output .='<table class="wp-list-table widefat fixed striped posts" style="margin-bottom: 30px;">
		  <thead>
			<tr>
			  <th class="manage-column column-_title" style="width:35px;">Sr.No</th>      
			  <th class="manage-column column-_title">Name</th>
			  <th class="manage-column column-_title">User</th>
			  <th class="manage-column column-_title">Quiz</th>
			  <th class="manage-column column-_title">Date/Time</th>
			  <th class="manage-column column-_title" style="width: 42px;">Per(%)</th>     
			  <th class="manage-column column-_title" style="width: 40px;">Result</th>     
			  <th class="manage-column column-_title" style="width: 60px;">Correct Answers</th>
			  <th class="manage-column column-_title" style="width: 65px;">Certificate</th>
			</tr>
		  </thead>
		  <tbody>';
		  $i=$start+1;
		  foreach ($resultsPastQuizes as $result_key => $result_value) {
			$file = $pdf_file_path.$result_value->crtf_name;
			$postid = $result_value->wp_post_id;
			$wp_user_id = $result_value->wp_user_id;
			$meta_value = $wpdb->get_var( "SELECT meta_value FROM `wp_postmeta` WHERE `post_id`='$postid' AND `meta_key`='_page_edit_data'" );
			$datam = unserialize($meta_value);

			$user_email = $wpdb->get_var( "SELECT user_email FROM `wp_users` WHERE `ID`='$wp_user_id'" );
			$first_name = $wpdb->get_var( "SELECT firstname FROM `whitelabel_users` WHERE `email`='$user_email' AND `white_label_website_id`='$white_label_website_id' " );

			$last_name = $wpdb->get_var( "SELECT lastname FROM `whitelabel_users` WHERE `email`='$user_email' AND `white_label_website_id`='$white_label_website_id'  " );
			$display_name = $first_name." ".$last_name;
			$score = round($result_value->score,2);
			//if($score >= $passing_percent) {
			if($result_value->crtf_name!='') {
				$view = '<a href="'.$file.'" title="View Certificate" target="_blank">View</a>';
				$result='Pass';
			}
			else {
				$view = '';
				$result = 'Fail';
			}		
			$output .='<tr>
			  <td data-column="sr.no" >'.$i.'</td> ';     
			  $output .='<td data-column="User" style="text-align: left;">'.$display_name.'</td>';
			  $output .='<td data-column="User" style="text-align: left;">'.$user_email.'</td>';
			  $output .='<td data-column="Quiz" style="text-align: left;">'.$datam['post_title'].'</td>';
			  $output .='<td data-column="Date" style="text-align: left;">'.date('d M Y, h:i A',strtotime($result_value->timestamp)).'</td>';
			  $output .='<td data-column="Percentage" style="text-align: center;">'.round($result_value->score,2).'%</td>';
			  $output .='<td data-column="Percentage" style="text-align: center;">'.$result.'</td>';
			  $output .='<td data-column="Correct Answers" style="text-align: center;">'.$result_value->num_correct.'</td>';    
			  $output .='<td data-column="Certificate" style="text-align: center;">'.$view.'</td>
			</tr>';
			$i++;
		 }
		 $pagination = '';
		 
		
		 for($i=1; $i <= $total_pages; $i++){
			 if($i==$_SESSION['pagedvl'])
			 $pagination .= " $i &nbsp;";
			 else $pagination .= "<a href='post.php?post=$post->ID&action=edit&paged=$i&tab=8'>$i</a> &nbsp;";
		 }
		 
		 
		$output .='<tr><td colspan=9 style="text-align:center;font-weight:bold">'.$pagination.'</td> </tr>';
		$output .='</tbody></table>';
		$output .='</div>';


	return $output;

}
