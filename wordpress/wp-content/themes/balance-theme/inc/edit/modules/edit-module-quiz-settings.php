<?php
/*
 *  WP Edit module: Quiz settings
 *  Description: Quiz settings module
 */

function quiz_settings_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array()) {
  global $data;

  $maxtries_dd_values = array();
  foreach (range(1, 10) as $number) {
    $maxtries_dd_values[] = array('id' => $number, 'title' => $number);
  }

  $cooldown_dd_values = array();
  foreach (range(1, 24) as $number) {
    $cooldown_dd_values[] = array('id' => $number, 'title' => $number);
  }
  array_push( $cooldown_dd_values,
    array('id' => 48, 'title' => '48 (2 days)'),
    array('id' => 72, 'title' => '72 (3 days)'),
    array('id' => 96, 'title' => '96 (4 days)'),
    array('id' => 120, 'title' => '120 (5 days)'),
    array('id' => 144, 'title' => '144 (6 days)'),
    array('id' => 168, 'title' => '168 (7 days)')
  );

  // retrieve subscription products to fill dropdown or display "none" if there are not published subscriptions available
  $products = array();

  $args = array(
    'post_type' => 'product',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'tax_query' => array(
      array(
        'taxonomy' => 'product_type',
        'field' => 'slug',
        'terms' => array( 'subscription', 'variable-subscription' )
      )
    )
  );

  $loop = new WP_Query( $args );
  if ( $loop->post_count == 0 ) {
    $subscriptions = array(
      'type' => 'dropdown',
      'name' => 'subscription',
      'label' => 'Subscription',
      'default_value' => '',
      'additional_params' => array(
        '',
        array(),
        '',
        true,
        true
      )
    );
  } else {
    while ( $loop->have_posts() ) {
     $loop->the_post();
     global $product;
     $products[] = array('id' => get_the_ID(), 'title' => get_the_title() );
    }

    $subscriptions = array(
      'type' => 'dropdown',
      'name' => 'subscription',
      'label' => 'Subscription',
      'default_value' => '',
      'additional_params' => array(
        '',
        $products,
        '',
        true
      )
    );
  }
  wp_reset_query();

  $output = simple_edit_module($key, 'quiz', array(
    array(
      'type' => 'checkbox',
      'name' => 'payable',
      'label' => 'Payable quiz',
      'default_value' => ''
    ),
    $subscriptions,
    array(
      'type' => 'text',
      'name' => 'buy_first',
      'label' => 'Buy-first message',
      'default_value' => 'You must buy a subscription before you can take the quiz.'
    ),
    array(
      'type' => 'dropdown',
      'name' => 'max_tries',
      'label' => 'Max. number of tries',
      'default_value' => '',
      'additional_params' => array(
        '',
        $maxtries_dd_values,
        '',
        true
      )
    ),
    array(
      'type' => 'dropdown',
      'name' => 'cooldown',
      'label' => 'Cool down period (hours)',
      'default_value' => '',
      'additional_params' => array(
        '',
        $cooldown_dd_values,
        '',
        true
      )
    ),
    array(
      'type' => 'number',
      'name' => 'success_rate',
      'label' => 'Success rate to pass (%)',
      'default_value' => '',
      'additional_params' => array(
        '',
        '',
        '',
        0,
        100,
        0.5
      )
    ),
    array(
      'type' => 'checkbox',
      'name' => 'show_register_form',
      'label' => 'Show registration form at the end of the quiz (if the user is already logged in he/she will not see the registration form)',
      'default_value' => ''
    ),
    array(
      'type' => 'checkbox',
      'name' => 'show_results',
      'label' => 'Show results if quiz was not passed',
      'default_value' => ''
    ),
    array(
        'type'                => 'textarea',
        'name'                => 'message_fail',
        'label'               => 'Message to display on failed quiz',
        'default_value'       => 'You did not receive a passing score. Please re-take the quiz by clicking on the "Retry Quiz" button below.',
        'additional_params'   => array(
          true,
          0,
          '',
          '',
          array('media_buttons' => false, 'quicktags' => false )
        )
    ),
    array(
        'type'                => 'textarea',
        'name'                => 'message_success',
        'label'               => 'Message to display on passed quiz (certificate text will be appended automatically)',
        'default_value'       => 'Congratulations, you passed the quiz!',
        'additional_params'   => array(
          true,
          0,
          '',
          '',
          array('media_buttons' => false, 'quicktags' => false )
      )
    ),
    array(
      'type' => 'checkbox',
      'name' => 'has_certificate',
      'label' => 'Enable PDF certificate for this quiz',
      'default_value' => ''
    ),
    array(
      'type' => 'text',
      'name' => 'certificate_title',
      'label' => 'Certificate title',
      'default_value' => 'Certificate of completion'
    ),
    array(
      'type' => 'text',
      'name' => 'email_subject',
      'label' => 'Email subject',
      'default_value' => ''
    ),
    array(
      'type' => 'textarea',
      'name' => 'email_body',
      'label' => 'Email body',
      'default_value' => '',
      'additional_params' => array(
        true,
        0,
        '',
        '',
        array('media_buttons' => true, 'quicktags' => true )
      )
    ),
  ), $data, $module_title, $visible_on);

  return $output;
}

?>
