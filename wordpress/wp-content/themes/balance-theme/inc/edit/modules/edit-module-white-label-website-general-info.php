<?php
/*
 *  WP Edit module: White Label Websites General Info
 */
function white_label_websites_general_info_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array() ) {
	global $data;
	if ( empty( $data['wlw_general_info_module'] ) ) {
		$data = init_array_on_var_and_array_key($data, 'wlw_general_info_module');
		$data['wlw_general_info_module'][ $key ] = array(
			'domain' => '',
			'title' => '',
			'sylink_id' => '',
			'main_email' => '',
			'description' => '',
			'logo' => array(
				'attachment_id' => '',
				'filename' => '',
				'fullpath' => ''
			),
            'logo_url' => '',
            'promotion_logo' => array(
                'attachment_id' => '',
                'filename' => '',
                'fullpath' => ''
            ),
            'promotion_logo_url' => '',
			'phone' => '',
			'include_account_numbers' => ''
		);
	}

	if ( empty( $data['wlw_general_info_module'][ $key ]['logo'] ) ) {
		$data['wlw_general_info_module'][ $key ]['logo'] = array(
			'attachment_id' => '',
			'filename' => '',
			'fullpath' => ''
		);
	}

	if ( empty( $data['wlw_general_info_module'][ $key ]['phone'] ) ) {
		$data['wlw_general_info_module'][ $key ]['phone'] = '';
	}

	//code by dhiraj
	if (!session_id()) {
		session_start();
	}
	if(isset($_SESSION['ptab']))
	$ptab = $_SESSION['ptab'];
	else $ptab='';
	

	//end code by dhiraj
	
	

	$output = '';
	$output .= '<a name="wlw-general-info-module-wrapper-'. $key .'"></a>';
	$output .= '<div onload="hidediv()" class="module-wrapper wlw-general-info-module-wrapper-'. $key .' hidden" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
	$output .= '  <div class="postbox postbox-custom wlw-general-info-module-list-wrapper-'. $key .'">';
	$output .= '    <h3>'. $module_title . ( intval( $key ) > 0 ? ' #'.( intval( $key )+1 ) : '' ) .'<a class="description fright section-expander is-expanded" data-toggle-title="'. __( 'Expand', 'balance' ) .'" href="javascript:;">'. __( 'Collapse', 'balance' ) .'</a></h3>';
	$output .= '    <div class="inside">';

	$output .= '        <p>';
	$output .= '          <label><b>'. __( 'Domain', 'balance' ) .':</b></label><br/>';
	$output .=              text_field( $data['wlw_general_info_module'][ $key ]['domain'], 'wlw_general_info_module['.$key.'][domain]' );
	$output .= '        </p>';

	$output .= '        <p>';
	$output .= '          <label><b>'. __( 'Title', 'balance' ) .':</b></label><br/>';
	$output .=              text_field( $data['wlw_general_info_module'][ $key ]['title'], 'wlw_general_info_module['.$key.'][title]' );
	$output .= '        </p>';

	$output .= '        <p>';
	$output .= '          <label><b>'. __( 'SYLINK ID', 'balance' ) .':</b></label><br/>';
	$output .=              text_field( $data['wlw_general_info_module'][ $key ]['sylink_id'], 'wlw_general_info_module['.$key.'][sylink_id]' );
	$output .= '        </p>';

	$output .= '        <p>';
	$output .= '          <label><b>'. __( 'Contact Us email', 'balance' ) .':</b></label><br/>';
	$output .=              text_field( $data['wlw_general_info_module'][ $key ]['main_email'], 'wlw_general_info_module['.$key.'][main_email]', '', '', '', 'email' );
	$output .= '        </p>';

	$output .= '        <p>';
	$output .= '          <label><b>'. __( 'Phone', 'balance' ) .':</b></label><br/>';
	$output .=              text_field( $data['wlw_general_info_module'][ $key ]['phone'], 'wlw_general_info_module['.$key.'][phone]' );
	$output .= '        </p>';

	$output .= '        <p>';
	$output .= '          <label><b>'. __( 'Description', 'balance' ) .':</b></label><br/>';
	$output .=              textarea_field( $data['wlw_general_info_module'][ $key ]['description'], 'wlw_general_info_module['.$key.'][description]', false );
	$output .= '        </p>';

	$output .= '        <p>';
	$output .= '          <label><b>'. __( 'Logo', 'balance' ) .':</b></label><br>';
	$output .=              upload_field( $data['wlw_general_info_module'][ $key ]['logo'], 'wlw_general_info_module['.$key.'][logo]' );
	$output .= '        </p>';
    
    $output .= '        <p>';
    $output .= '          <label><b>'. __( 'Logo Url', 'balance' ) .':</b></label><br>';
    $output .=              text_field( $data['wlw_general_info_module'][ $key ]['logo_url'], 'wlw_general_info_module['.$key.'][logo_url]' );
    $output .= '        </p>';
    
    $output .= '        <p>';
    $output .= '          <label><b>'. __( 'Promotion Logo', 'balance' ) .':</b></label><br>';
    $output .=              upload_field( $data['wlw_general_info_module'][ $key ]['promotion_logo'], 'wlw_general_info_module['.$key.'][promotion_logo]' );
    $output .= '        </p>';
    
    $output .= '        <p>';
    $output .= '          <label><b>'. __( 'Promotion Logo Url', 'balance' ) .':</b></label><br>';
    $output .=              text_field( $data['wlw_general_info_module'][ $key ]['promotion_logo_url'], 'wlw_general_info_module['.$key.'][promotion_logo_url]' );
    $output .= '        </p>';
    
    $output .= '        <p>';
  $output .= '          <br><label><b>'. __( 'Include account/member numbers', 'balance' ) .':</b></label>&nbsp;&nbsp;&nbsp;';
  //$output .=              radiobuttonlist_field( $data['wlw_general_info_module'][ $key ]['include_account_numbers'], 'wlw_general_info_module['.$key.'][include_account_numbers]', array( 'y' => __( 'Yes', 'balance' ), 'n' => __( 'No', 'balance' ) ), 'y', true);
  $acc_numbers = $data['wlw_general_info_module'][ $key ]['include_account_numbers'];
  if(isset($acc_numbers) && $acc_numbers=='on')$monstatus='checked';else $monstatus=''; 
  $output .= '<input type="checkbox" class="toggleCheck" name="wlw_general_info_module['.$key.'][include_account_numbers]" '.$monstatus.' style="padding-top:10px !important">'; 
  $output .= '        </p>';
/*
  $output .= '        <p>';
  $output .= '          <label><b>'. __( 'Disable chat', 'balance' ) .':</b></label><br>';
  $output .=              radiobuttonlist_field( $data['wlw_general_info_module'][ $key ]['disable_chat'], 'wlw_general_info_module['.$key.'][disable_chat]', array( 'y' => __( 'Yes', 'balance' ), 'n' => __( 'No', 'balance' ) ), 'n', true);
  $output .= '        </p>';
*/
  $output .= '        <p>';
  $output .= '          <br><label><b>'. __( 'Disable Login / Registration', 'balance' ) .':</b></label>&nbsp;&nbsp;&nbsp;';
  //$output .=              radiobuttonlist_field( $data['wlw_general_info_module'][ $key ]['disable_login_register'], 'wlw_general_info_module['.$key.'][disable_login_register]', array( 'y' => __( 'Yes', 'balance' ), 'n' => __( 'No', 'balance' ) ), 'n', true);
  $login_disable = $data['wlw_general_info_module'][ $key ]['disable_login_register'];
  if(isset($login_disable) && $login_disable=='on')$monstatus='checked';else $monstatus=''; 
  $output .= '<input type="checkbox" class="toggleCheck" name="wlw_general_info_module['.$key.'][disable_login_register]" '.$monstatus.' style="padding-top:10px !important">'; 
 
  
  $output .= '        </p>';

	$output .= '    </div>';
	$output .= '  </div>';
	$output .= '</div>';
	if($ptab==8) {
	$output .= '<script>function hidediv(){jQuery(".wlw-general-info-module-wrapper-0").addClass("module-wrapper wlw-general-info-module-wrapper-0 hidden")}';
	$output .='jQuery(document).ready(function() {hidediv();});';
	$output .= '</script>';
	}
	return $output;
}
?>
