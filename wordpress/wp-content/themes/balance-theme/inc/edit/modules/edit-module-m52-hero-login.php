<?php
/*
 *  WP Edit module: M52
 *  Description: Module with login.  M52 - Hero w/Login
 */

function m52_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array( 'media_buttons' => false, 'quicktags' => true ) ) {
	global $data;

	if ( empty( $data['m52_module'][ $key ] ) ) {
		$data = init_array_on_var_and_array_key($data, 'm52_module');
		$data['m52_module'][ $key ] = array(
			'title' => '',
			'copy' => '',
			'image' => array(
				'attachment_id' => '',
				'filename' => '',
				'fullpath' => '',
			),
		);
	}

	if ( empty( $data['m52_module'][ $key ]['login'] ) ) {
		$data['m52_module'][ $key ]['login'] = '';
	}

	$showlogin = array(
		'loginshow' => __( 'Show', 'balance' ),
	);

	  $array_of_types = array(
    0 => array(
      'id' => '0',
      'title' => 'Select',
    ),
    1000 => array(
      'id' => '1000',
      'title' => '1000',
    ),
    2000 => array(
      'id' => '2000',
      'title' => '2000'
    ),
    3000 => array(
      'id' => '3000',
      'title' => '3000'
    ),
    4000 => array(
      'id' => '4000',
      'title' => '4000'
    ),
    5000 => array(
      'id' => '5000',
      'title' => '5000'
    ),
    6000 => array(
      'id' => '6000',
      'title' => '6000'
    ),
    7000 => array(
      'id' => '7000',
      'title' => '7000'
    ),
    8000 => array(
      'id' => '8000',
      'title' => '8000'
    ),
    9000 => array(
      'id' => '9000',
      'title' => '9000'
    ),
    10000 => array(
      'id' => '10000',
      'title' => '10000'
    )
  );


	$output = '';
	$output .= '<a name="m52-module-wrapper-'. $key .'"></a>';
	$output .= '<div class="module-wrapper m52-module-module-wrapper-'. $key .'" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
	$output .= '  <div class="postbox postbox-custom m52-module-list-wrapper-'. $key .'">';
	/*$output .= '    <h3>'. $module_title . ( intval( $key ) > 0 ? ' #'.( intval( $key )+1 ) : '' ) .'<a class="description fright section-expander is-collapsed" data-toggle-title="'. __( 'Collapse', 'balance' ) .'" href="javascript:;">'. __( 'Expand', 'balance' ) .'</a></h3>';*/
	$output .= '    <h3>M52: Hero Background Image<a class="description fright section-expander is-collapsed" data-toggle-title="'. __( 'Collapse', 'balance' ) .'" href="javascript:;">'. __( 'Expand', 'balance' ) .'</a></h3>';
	$output .= '    <div class="inside hidden">';

/*	$output .= '      <p>';
	$output .= '        <label><b>'. __( 'Show login', 'balance' ) .':</b></label><br>';
	$output .=          checkboxlist_field( $data['m52_module'][ $key ]['login'], 'm52_module['.$key.'][login]', $showlogin );
	$output .= '      </p>';*/

	$output .= '        <p>';
	$output .= '           <label><b>'. __( 'Background image', 'balance' ) .':  </b><i>[Image dimensions should be 1399 * 415 pixel]</i></b></label><br/>';
	$output .=             upload_field( $data['m52_module'][ $key ]['image'], 'm52_module['.$key.'][image]' );
	$output .= '        </p>';
	$output .= '          <label>'. __( 'Title', 'balance' ) .':</label>';
	$output .=              text_field( $data['m52_module'][ $key ]['title'], 'm52_module['.$key.'][title]' );
	$output .= '        </p>';

	$output .= '            <p>';
	$output .= '              <label><b>'. __( 'Copy', 'balance' ) .':</b></label><br/><br>';
	$output .=                textarea_field( $data['m52_module'][ $key ]['copy'], 'm52_module['.$key.'][copy]', true, 0, '', '', $custom_settings );
	$output .= '            </p>';

	$output .= '    </div>';
	$output .= '  </div>';
	$output .= '</div>';


	$output .= '<div class="module-wrapper m52-module-module-wrapper-'. $key .'" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
	$output .= '  <div class="postbox postbox-custom m52-module-list-wrapper-'. $key .'">';
	$output .= '    <h3>M52: Carousel / Slider<a class="description fright section-expander is-collapsed" data-toggle-title="'. __( 'Collapse', 'balance' ) .'" href="javascript:;">'. __( 'Expand', 'balance' ) .'</a></h3>';
	$output .= '    <div class="inside hidden">';

	$output .= '      <p>';
	$output .= '        <label><b>'. __( 'Show Slider', 'balance' ) .':</b></label><br>';
	$output .=          checkboxlist_field( $data['m52_module'][ $key ]['showslider'], 'm52_module['.$key.'][showslider]', $showlogin );
	$output .= '      </p>';
	$output .= '        <script>
				jQuery(window).load(function() {
					 if(jQuery(".sliderImage4").css("display") == "inline-block"){
							jQuery(".addIcon").css("display","none");
					 }
				});

				function duplicateSliderImage(){
					if(jQuery(".sliderImage2").css("display") == "none"){
						jQuery(".sliderImage2").removeAttr("style");
						jQuery(".sliderImage2").css("width","200px");
						jQuery(".sliderImage2").css("display","inline-block");
						jQuery(".sliderImage2").css("margin","5px");
						jQuery(".sliderImage2").css("vertical-align","top");
						if(jQuery(".addIcon").css("display") == "none"){
							jQuery(".addIcon").css("display","block");
						}
					} else if(jQuery(".sliderImage3").css("display") == "none"){
						jQuery(".sliderImage3").removeAttr("style");
						jQuery(".sliderImage3").css("width","200px");
						jQuery(".sliderImage3").css("display","inline-block");
						jQuery(".sliderImage3").css("margin","5px");
						jQuery(".sliderImage3").css("vertical-align","top");
						if(jQuery(".addIcon").css("display") == "none"){
							jQuery(".addIcon").css("display","block");
						}
					} else if(jQuery(".sliderImage4").css("display") == "none"){
						jQuery(".sliderImage4").removeAttr("style");
						jQuery(".sliderImage4").css("width","200px");
						jQuery(".sliderImage4").css("display","inline-block");
						jQuery(".sliderImage4").css("margin","5px");
						jQuery(".sliderImage4").css("vertical-align","top");
						jQuery(".addIcon").css("display","none");
					} 
					/*else if(jQuery(".sliderImage5").css("display") == "none"){
						jQuery(".sliderImage5").removeAttr("style");
						jQuery(".sliderImage5").css("width","200px");
						jQuery(".sliderImage5").css("display","inline-block");
						jQuery(".sliderImage5").css("margin","5px");
						jQuery(".sliderImage5").css("vertical-align","top");
					} else if(jQuery(".sliderImage6").css("display") == "none"){
						jQuery(".sliderImage6").removeAttr("style");
						jQuery(".sliderImage6").css("width","200px");
						jQuery(".sliderImage6").css("display","inline-block");
						jQuery(".sliderImage6").css("margin","5px");
						jQuery(".sliderImage6").css("vertical-align","top");
					} else if(jQuery(".sliderImage7").css("display") == "none"){
						jQuery(".sliderImage7").removeAttr("style");
						jQuery(".sliderImage7").css("width","200px");
						jQuery(".sliderImage7").css("display","inline-block");
						jQuery(".sliderImage7").css("margin","5px");
						jQuery(".sliderImage7").css("vertical-align","top");
					} else if(jQuery(".sliderImage8").css("display") == "none"){
						jQuery(".sliderImage8").removeAttr("style");
						jQuery(".sliderImage8").css("width","200px");
						jQuery(".sliderImage8").css("display","inline-block");
						jQuery(".sliderImage8").css("margin","5px");
						jQuery(".sliderImage8").css("vertical-align","top");
					} else if(jQuery(".sliderImage9").css("display") == "none"){
						jQuery(".sliderImage9").removeAttr("style");
						jQuery(".sliderImage9").css("width","200px");
						jQuery(".sliderImage9").css("display","inline-block");
						jQuery(".sliderImage9").css("margin","5px");
						jQuery(".sliderImage9").css("vertical-align","top");
					} else if(jQuery(".sliderImage10").css("display") == "none"){
						jQuery(".sliderImage10").removeAttr("style");
						jQuery(".sliderImage10").css("width","200px");
						jQuery(".sliderImage10").css("display","inline-block");
						jQuery(".sliderImage10").css("margin","5px");
						jQuery(".sliderImage10").css("vertical-align","top");
					}*/
				}
	</script>';

	$output .= '        <style>
.sliderImg .upload-wrapper .upload-img-data {
	display: none;
}
	</style>';
	$output .= '        </p>';
	$output .= '        <p>';
	$output .= '           <label><b>'. __( 'Slider image', 'balance' ) .': </b><i>[Image dimensions should be 1399 * 415 pixel]</i></label><br/><b>Note:</b> Upload maximum 4 images<br/>';
	$output .= '        	<div class="sliderImage1 sliderImg" style="width: 200px;display: inline-block;margin: 5px;vertical-align: top;">';
	$output .=             	upload_field( $data['m52_module'][ $key ]['slider_image1'], 'm52_module['.$key.'][slider_image1]' );
	$output .= '        <p>';
	$output .= '          <label>'. __( 'Slider URL', 'balance' ) .':</label>';
	$output .=              text_field( $data['m52_module'][ $key ]['slider_image1_url'], 'm52_module['.$key.'][slider_image1_url]' );
	$output .= '        </p>';
	$output .= '        	</div>';
if(isset($data['m52_module'][ $key ]['slider_image2']) && $data['m52_module'][ $key ]['slider_image2'] != '' && strlen($data['m52_module'][ $key ]['slider_image2']['image']['filename']) > 0 ){
	$output .= '        	<div class="sliderImage2 sliderImg" style="width: 200px;display: inline-block;margin: 5px;vertical-align: top;">';
	$output .=             	upload_field( $data['m52_module'][ $key ]['slider_image2'], 'm52_module['.$key.'][slider_image2]' );
	$output .= '        <p>';
	$output .= '          <label>'. __( 'Slider URL', 'balance' ) .':</label>';
	$output .=              text_field( $data['m52_module'][ $key ]['slider_image2_url'], 'm52_module['.$key.'][slider_image2_url]' );
	$output .= '        </p>';
	$output .= '        	</div>';
} else {
	$output .= '        	<div class="sliderImage2 sliderImg" style="display: none;">';
	$output .=             	upload_field( $data['m52_module'][ $key ]['slider_image2'], 'm52_module['.$key.'][slider_image2]' );
	$output .= '        <p>';
	$output .= '          <label>'. __( 'Slider URL', 'balance' ) .':</label>';
	$output .=              text_field( $data['m52_module'][ $key ]['slider_image2_url'], 'm52_module['.$key.'][slider_image2_url]' );
	$output .= '        </p>';
	$output .= '        	</div>';
}
if(isset($data['m52_module'][ $key ]['slider_image3']) && $data['m52_module'][ $key ]['slider_image3'] != ''  && strlen($data['m52_module'][ $key ]['slider_image3']['image']['filename']) > 0 ){
	$output .= '        	<div class="sliderImage3 sliderImg" style="width: 200px;display: inline-block;margin: 5px;vertical-align: top;">';
	$output .=             	upload_field( $data['m52_module'][ $key ]['slider_image3'], 'm52_module['.$key.'][slider_image3]' );
	$output .= '        <p>';
	$output .= '          <label>'. __( 'Slider URL', 'balance' ) .':</label>';
	$output .=              text_field( $data['m52_module'][ $key ]['slider_image3_url'], 'm52_module['.$key.'][slider_image3_url]' );
	$output .= '        </p>';
	$output .= '        	</div>';
} else {
	$output .= '        	<div class="sliderImage3 sliderImg" style="display: none;">';
	$output .=             	upload_field( $data['m52_module'][ $key ]['slider_image3'], 'm52_module['.$key.'][slider_image3]' );
	$output .= '        <p>';
	$output .= '          <label>'. __( 'Slider URL', 'balance' ) .':</label>';
	$output .=              text_field( $data['m52_module'][ $key ]['slider_image3_url'], 'm52_module['.$key.'][slider_image3_url]' );
	$output .= '        </p>';
	$output .= '        	</div>';
}
if(isset($data['m52_module'][ $key ]['slider_image4']) && $data['m52_module'][ $key ]['slider_image4'] != ''  && strlen($data['m52_module'][ $key ]['slider_image4']['image']['filename']) > 0 ){
	$output .= '        	<div class="sliderImage4 sliderImg" style="width: 200px;display: inline-block;margin: 5px;vertical-align: top;">';
	$output .=             	upload_field( $data['m52_module'][ $key ]['slider_image4'], 'm52_module['.$key.'][slider_image4]' );
	$output .= '        <p>';
	$output .= '          <label>'. __( 'Slider URL', 'balance' ) .':</label>';
	$output .=              text_field( $data['m52_module'][ $key ]['slider_image4_url'], 'm52_module['.$key.'][slider_image4_url]' );
	$output .= '        </p>';
	$output .= '        	</div>';
} else {
	$output .= '        	<div class="sliderImage4 sliderImg" style="display: none;">';
	$output .=             	upload_field( $data['m52_module'][ $key ]['slider_image4'], 'm52_module['.$key.'][slider_image4]' );
	$output .= '        <p>';
	$output .= '          <label>'. __( 'Slider URL', 'balance' ) .':</label>';
	$output .=              text_field( $data['m52_module'][ $key ]['slider_image4_url'], 'm52_module['.$key.'][slider_image4_url]' );
	$output .= '        </p>';
	$output .= '        	</div>';
}
if(isset($data['m52_module'][ $key ]['slider_image5']) && $data['m52_module'][ $key ]['slider_image5'] != '' && strlen($data['m52_module'][ $key ]['slider_image5']['image']['filename']) > 0 ){
	$output .= '        	<div class="sliderImage5 sliderImg" style="width: 200px;display: inline-block;margin: 5px;vertical-align: top;">';
	$output .=             	upload_field( $data['m52_module'][ $key ]['slider_image5'], 'm52_module['.$key.'][slider_image5]' );
	$output .= '        	</div>';
} else {
	$output .= '        	<div class="sliderImage5 sliderImg" style="display:none;">';
	$output .=             	upload_field( $data['m52_module'][ $key ]['slider_image5'], 'm52_module['.$key.'][slider_image5]' );
	$output .= '        	</div>';
}

if(isset($data['m52_module'][ $key ]['slider_image6']) && $data['m52_module'][ $key ]['slider_image6'] != '' && strlen($data['m52_module'][ $key ]['slider_image6']['image']['filename']) > 0 ){
	$output .= '        	<div class="sliderImage6 sliderImg" style="width: 200px;display: inline-block;margin: 5px;vertical-align: top;">';
	$output .=             	upload_field( $data['m52_module'][ $key ]['slider_image6'], 'm52_module['.$key.'][slider_image6]' );
	$output .= '        	</div>';
} else {
	$output .= '        	<div class="sliderImage6 sliderImg" style="display:none;">';
	$output .=             	upload_field( $data['m52_module'][ $key ]['slider_image6'], 'm52_module['.$key.'][slider_image6]' );
	$output .= '        	</div>';
}
if(isset($data['m52_module'][ $key ]['slider_image7']) && $data['m52_module'][ $key ]['slider_image7'] != ''  && strlen($data['m52_module'][ $key ]['slider_image7']['image']['filename']) > 0 ){
	$output .= '        	<div class="sliderImage7 sliderImg" style="width: 200px;display: inline-block;margin: 5px;vertical-align: top;">';
	$output .=             	upload_field( $data['m52_module'][ $key ]['slider_image7'], 'm52_module['.$key.'][slider_image7]' );
	$output .= '        	</div>';
} else {
	$output .= '        	<div class="sliderImage7 sliderImg" style="display:none;">';
	$output .=             	upload_field( $data['m52_module'][ $key ]['slider_image7'], 'm52_module['.$key.'][slider_image7]' );
	$output .= '        	</div>';
}
if(isset($data['m52_module'][ $key ]['slider_image8']) && $data['m52_module'][ $key ]['slider_image8'] != ''  && strlen($data['m52_module'][ $key ]['slider_image8']['image']['filename']) > 0 ){
	$output .= '        	<div class="sliderImage8 sliderImg" style="width: 200px;display: inline-block;margin: 5px;vertical-align: top;">';
	$output .=             	upload_field( $data['m52_module'][ $key ]['slider_image8'], 'm52_module['.$key.'][slider_image8]' );
	$output .= '        	</div>';
} else {
	$output .= '        	<div class="sliderImage8 sliderImg" style="display: none;">';
	$output .=             	upload_field( $data['m52_module'][ $key ]['slider_image8'], 'm52_module['.$key.'][slider_image8]' );
	$output .= '        	</div>';
}

if(isset($data['m52_module'][ $key ]['slider_image9']) && $data['m52_module'][ $key ]['slider_image9'] != ''  && strlen($data['m52_module'][ $key ]['slider_image9']['image']['filename']) > 0 ){
	$output .= '        	<div class="sliderImage9 sliderImg" style="width: 200px;display: inline-block;margin: 5px;vertical-align: top;">';
	$output .=             	upload_field( $data['m52_module'][ $key ]['slider_image9'], 'm52_module['.$key.'][slider_image9]' );
	$output .= '        	</div>';
} else {
	$output .= '        	<div class="sliderImage9 sliderImg" style="display:none;">';
	$output .=             	upload_field( $data['m52_module'][ $key ]['slider_image9'], 'm52_module['.$key.'][slider_image9]' );
	$output .= '        	</div>';
}

if(isset($data['m52_module'][ $key ]['slider_image10']) && $data['m52_module'][ $key ]['slider_image10'] != ''  && strlen($data['m52_module'][ $key ]['slider_image10']['image']['filename']) > 0 ){
	$output .= '        	<div class="sliderImage10 sliderImg" style="width: 200px;display: inline-block;margin: 5px;vertical-align: top;">';
	$output .=             	upload_field( $data['m52_module'][ $key ]['slider_image10'], 'm52_module['.$key.'][slider_image10]' );
	$output .= '        	</div>';
} else {
	$output .= '        	<div class="sliderImage10 sliderImg" style="display:none;">';
	$output .=             	upload_field( $data['m52_module'][ $key ]['slider_image10'], 'm52_module['.$key.'][slider_image10]' );
	$output .= '        	</div>';
}
	$output .= '        	<div class="addIcon">';
	$output .= '				<span onclick="duplicateSliderImage()"><i class="fa fa-plus" aria-hidden="true"></i></span>';
	$output .= '        	</div>';
	$output .= '        </p>';

	$output .= '        <p>';
  $output .= '      <label>';
  $output .= '        <span class="text">'. __( 'Slider Duration: ') .'</span>';
  $output .= '      </label>';
  $output .=        dropdown_field( $data['slider_duration'], 'slider_duration', '', $array_of_types, '', false );
  $output .= '      <label>';
  $output .= '        <span class="text">'. __( 'miliseconds ') .'<i>(By default 5000 miliseconds)</i></span>';
  $output .= '      </label>';
	$output .= '        </p>';
	$output .= '    </div>';
	$output .= '  </div>';
	$output .= '</div>';
	return $output;

}

?>
