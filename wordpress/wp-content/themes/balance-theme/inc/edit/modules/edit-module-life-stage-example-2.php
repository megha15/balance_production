<?php
/*
 *  WP Edit module: Life Stage Example 2
 */
function life_stage_example_2_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array() ) {
  global $data;

  if( !empty( $data['life_stage_example_2_module'] ) ){
    $data['life_stage_example_2_module'][ $key ] = array(
      'title' => '',
    );
  }

  $output = '';
  $output .= '<a name="life_stage_example_2-module-wrapper-'. $key .'"></a>';
  $output .= '<div class="module-wrapper life_stage_example_2-module-wrapper-'. $key .' hidden" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
  $output .= '  <div class="postbox postbox-custom life_stage_example_2-module-list-wrapper-'. $key .'">';
  $output .= '    <h3>'. $module_title . (intval($key) > 0 ? ' #'.(intval($key)+1) : '') .'<a class="description fright section-expander is-collapsed" data-toggle-title="'. __( 'Collapse', 'balance' ) .'" href="javascript:;">'. __( 'Expand', 'balance' ) .'</a></h3>';
  $output .= '    <div class="inside hidden">';
  $output .= '    </div>';
  $output .= '  </div>';
  $output .= '</div>';
  return $output;
}
?>
