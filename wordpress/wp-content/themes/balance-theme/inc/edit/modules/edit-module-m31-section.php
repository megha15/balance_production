<?php
/*
 *  WP Edit module: M31
 *  Description: Module section: M31 - Headline, picture on left, blurb, CT; M32 - Headline, picture on right, blurb, CTA; M33 – Headline, blurb, CTA; M34 – Headline, blurb; M35 – Blurb, picture on left, CTA; M36 – Blurb, picture on right, CTA; M37 – Blurb, CTA; M38 – Blurb; M39 – Blurb, picture on left; M40 – Blurb, picture on right; M41 – Name, blurb, picture on left, CTA; M42 – Name, blurb, picture on right, CTA; M43 – Name, blurb, CTA; M44 – Name, blurb; M54 - BalanceTrack Content
 */

function m31_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array('media_buttons' => true, 'quicktags' => true ) ) {
  global $data;

  if( empty( $data['m31_module'][ $key ] ) ){
    $data = init_array_on_var_and_array_key($data, 'm31_module');
    $data['m31_module'][ $key ] = array(
      'title' => '',
      'copy' => '',
      'image' => array(
        'attachment_id' => '',
        'filename' => '',
        'fullpath' => '',
      ),
      'leftright' => '',
      'buttontext' => '',
      'linkfield' => array(
        'url_type' => '1',
        'url' => '',
        'url_page_id' => '',
        'url_onclick_js' => ''
      ),
      'buttonimage' => array(
        'attachment_id' => '',
        'filename' => '',
        'fullpath' => '',
      ),
    );
  }


  //Left right image choices
  $image_pos_choices = array(
    'left' => __( 'Left', 'balance' ),
    'right'  => __( 'Right', 'balance' ),
  );

  $output = '';
  $output .= '<a name="m31-module-wrapper-'. $key .'"></a>';
  $output .= '<div class="module-wrapper m31-module-module-wrapper-'. $key .'" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
  $output .= '  <div class="postbox postbox-custom m31-module-list-wrapper-'. $key .'">';
  $output .= '    <h3>'. $module_title . (intval($key) > 0 ? ' #'.(intval($key)+1) : '') .'<a class="description fright section-expander is-collapsed" data-toggle-title="'. __( 'Collapse', 'balance' ) .'" href="javascript:;">'. __( 'Expand', 'balance' ) .'</a></h3>';
  $output .= '    <div class="inside hidden">';

  $output .= '      <p>';
  $output .= '       <label><b>'. __( 'Title', 'balance' ) .':</b></label>';
  $output .=          text_field( $data['m31_module'][ $key ]['title'], 'm31_module['.$key.'][title]');
  $output .= '      </p>';

  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Description', 'balance' ) .':</b></label><br/>';
  $output .=          textarea_field( $data['m31_module'][ $key ]['copy'], 'm31_module['.$key.'][copy]', true, 0, '', '', $custom_settings );
  $output .= '      </p>';

  $output .= '     <p>';
  $output .= '        <label><b>'. __( 'Image position', 'balance' ) .':</b></label><br>';
  $output .=          radiobuttonlist_field( $data['m31_module'][ $key ]['leftright'], 'm31_module['.$key.'][leftright]', $image_pos_choices, 'left', true);
  $output .= '      </p>';

  $output .= '     <p>';
  $output .= '       <label><b>'. __( 'Image', 'balance' ) .':</b></label><br/>';
  $output .=          upload_field( $data['m31_module'][ $key ]['image'], 'm31_module['.$key.'][image]' );
  $output .= '     </p>';

  $output .= '      <p>';
  $output .= '       <label><b>'. __( 'Button text', 'balance' ) .':</b></label>';
  $output .=          text_field( $data['m31_module'][ $key ]['buttontext'], 'm31_module['.$key.'][buttontext]');
  $output .= '      </p>';

  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Button link', 'balance' ) .':</b></label><br/>';
  $output .=          links_to_field( $data['m31_module'][ $key ]['linkfield'], 'm31_module['.$key.'][linkfield]');
  $output .= '      </p>';

  $output .= '     <p>';
  $output .= '       <label><b>'. __( 'Button Icon', 'balance' ) .':</b></label><br/>';
  $output .=          upload_field( $data['m31_module'][ $key ]['buttonimage'], 'm31_module['.$key.'][buttonimage]' );
  $output .= '     </p>';

  $output .= '    </div>';
  $output .= '  </div>';
  $output .= '</div>';
  return $output;

}

?>
