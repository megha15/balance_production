<?php
/*
 *  WP Edit module: M33
 *  Description: Module section: M33 – Headline, blurb, CTA
 */

function m33_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array('media_buttons' => false, 'quicktags' => true )) {
  global $data;

  if( empty( $data['m33_module'][ $key ] ) ){
    $data = init_array_on_var_and_array_key($data, 'm33_module');
    $data['m33_module'][ $key ] = array(
      'title' => '',
      'copy' => '',
      'buttontext' => '',
      'linkfield' => array(
        'url_type' => '1',
        'url' => '',
        'url_page_id' => '',
        'url_onclick_js' => ''
      ),
    );
  }

  $output = '';
  $output .= '<a name="m33-module-wrapper-'. $key .'"></a>';
  $output .= '<div class="module-wrapper m33-module-module-wrapper-'. $key .'" '. ( $visible_on != "all" ? "data-visible-on='" . $visible_on ."'" : "" ) .'>';
  $output .= '  <div class="postbox postbox-custom m33-module-list-wrapper-'. $key .'">';
  $output .= '    <h3>'. $module_title . (intval($key) > 0 ? ' #'.(intval($key)+1) : '') .'<a class="description fright section-expander is-collapsed" data-toggle-title="'. __( 'Collapse', 'balance' ) .'" href="javascript:;">'. __( 'Expand', 'balance' ) .'</a></h3>';
  $output .= '    <div class="inside hidden">';

  $output .= '      <p>';
  $output .= '       <label><b>'. __( 'Title', 'balance' ) .':</b></label>';
  $output .=          text_field( $data['m33_module'][ $key ]['title'], 'm33_module['.$key.'][title]');
  $output .= '      </p>';

  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Copy', 'balance' ) .':</b></label><br/>';
  $output .=          textarea_field( $data['m33_module'][ $key ]['copy'], 'm33_module['.$key.'][copy]', true, 0, '', '', $custom_settings );
  $output .= '      </p>';

  $output .= '      <p>';
  $output .= '       <label><b>'. __( 'Button text', 'balance' ) .':</b></label>';
  $output .=          text_field( $data['m33_module'][ $key ]['buttontext'], 'm33_module['.$key.'][buttontext]');
  $output .= '      </p>';

  $output .= '      <p>';
  $output .= '        <label><b>'. __( 'Button link', 'balance' ) .':</b></label><br/>';
  $output .=          links_to_field( $data['m33_module'][ $key ]['linkfield'], 'm33_module['.$key.'][linkfield]');
  $output .= '      </p>';

  $output .= '    </div>';
  $output .= '  </div>';
  $output .= '</div>';

  return $output;

}

?>
