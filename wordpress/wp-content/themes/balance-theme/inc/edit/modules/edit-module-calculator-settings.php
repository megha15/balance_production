<?php
/*
 *  WP Edit module: calculator
 *  Description: Module with calculator settings.
 */

function calculator_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings) {
  global $data;

  $output = simple_edit_module($key, 'calculator', array(
    array(
      'type' => 'text',
      'name' => 'init',
      'label' => 'Initialization script',
      'default_value' => ''
    ),
    array(
      'type' => 'text',
      'name' => 'support',
      'label' => 'Support script',
      'default_value' => ''
    )
  ), $data, $module_title, $visible_on);


  return $output;

}

?>
