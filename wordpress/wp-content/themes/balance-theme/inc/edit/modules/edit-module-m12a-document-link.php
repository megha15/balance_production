<?php
/*
 *  WP Edit module: M12
 *  Description: Module hero. M12a – Document Link
 */

function m12a_module_form( $key, $visible_on = 'all', $module_title = '', $custom_settings = array()) {
  global $data;

$output = simple_edit_module($key, 'm12a', array(
    array(
      'type' => 'upload',
      'name' => 'pdf',
      'label' => 'PDF',
      'default_value' => '',
      'additional_params' => array(
        '',
        'PDF',
        'application/pdf'
      )
    )
  ), $data, $module_title, $visible_on);


  return $output;

}

?>
