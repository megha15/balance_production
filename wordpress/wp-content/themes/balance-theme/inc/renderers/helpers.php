<?php
/**
 * Helper functions for renderers
 */

function build_link( $link_value_holder, $additional_attributes='', $returnempty = false ) {
    $link = '';

    $onclick = '';
    if ( isset( $link_value_holder["url_onclick_js"] ) && !empty( $link_value_holder["url_onclick_js"] ) ) {
        $onclick = ' onclick="' . $link_value_holder["url_onclick_js"] . '" ';
    }

    if ( !empty( $link_value_holder["url_page_id"] ) && !empty( $link_value_holder["url_type"] ) ) { //linked page
        $link = '<a ' . $onclick . ' href="' . get_relative_permalink( $link_value_holder["url_page_id"] ) . '" class="%s" ' . $additional_attributes . '> %s</a>';
    } else if ( empty( $link_value_holder["url_type"] ) && !empty( $link_value_holder["url"] ) ) { //custom url
        $link = '<a ' . $onclick . ' href="' . $link_value_holder["url"] . '" class="%s" ' . $additional_attributes . '> %s</a>';
    } else {
        if( $returnempty ){
            return '';
        }
        $link = '<a ' . $onclick . ' href="#" class="%s no-link" ' . $additional_attributes . '> %s</a>';
    }
    return $link;
}


function build_link_aria_label( $link_value_holder, $additional_attributes='', $aria_label , $returnempty = false ) {
    $link = '';

    $onclick = '';
    if ( isset( $link_value_holder["url_onclick_js"] ) && !empty( $link_value_holder["url_onclick_js"] ) ) {
        $onclick = ' onclick="' . $link_value_holder["url_onclick_js"] . '" ';
    }

    if ( !empty( $link_value_holder["url_page_id"] ) && !empty( $link_value_holder["url_type"] ) ) { //linked page
        $link = '<a ' . $onclick . ' aria-label="'. $aria_label .'" href="' . get_relative_permalink( $link_value_holder["url_page_id"] ) . '" class="%s" ' . $additional_attributes . '> %s</a>';
    } else if ( empty( $link_value_holder["url_type"] ) && !empty( $link_value_holder["url"] ) ) { //custom url
        $link = '<a ' . $onclick . ' aria-label="'. $aria_label .'" href="' . $link_value_holder["url"] . '" class="%s" ' . $additional_attributes . '> %s</a>';
    } else {
        if( $returnempty ){
            return '';
        }
        $link = '<a ' . $onclick . ' href="#" aria-label="'. $aria_label .'" class="%s no-link" ' . $additional_attributes . '> %s</a>';
    }
    return $link;
}


/**
 * Function which checks if there is link in linkfield
 */
function is_linkable( $link_value_holder ){
    if( ( empty( $link_value_holder['url_type'] ) && !empty( $link_value_holder['url'] ) ) || ( !empty( $link_value_holder['url_type'] ) && !empty( $link_value_holder["url_page_id"] ) ) ){
        return true;
    }
    return false;
}

function get_related_resource_info( $resources ) {
  $data = array();

  // var_dump($resources).die();

  foreach( $resources as $r ) {
    if( !empty( $r['side_related_resources'][0] ) && !empty($r['link_text'])) {
      $resource_post= get_post($r['side_related_resources']);
      if ( !empty( $resource_post ) ) {
        $data[] = array(
          'link' => get_relative_permalink($r['side_related_resources']),
          'link_text' => $r['link_text'],
          'icon_type' => $resource_post->post_type
        );
      }
    }
  }
  return $data;
}

function get_program_pages( $resource_ids, $current_page ) {
  global $wpdb;
  $prefix = $wpdb->prefix;
  $resources_table_name = $prefix . 'resources';
  $post_table_name = $prefix . 'posts';
  $post_table_id = $prefix . 'post_id';
  $sql = "
    SELECT title, post_title, $post_table_id, status, count($post_table_id) as pages FROM $resources_table_name
    WHERE $post_table_id IN ($resource_ids) AND status = 'publish'
    GROUP BY $post_table_id
    ORDER BY field($post_table_id, $resource_ids)
  ";
  $program_resources = $wpdb->get_results( $sql , ARRAY_A );
  $page_info = array( 'page' => 0, 'total_pages' => 0, 'resource_id' => 0, 'chapters' => array() );
  foreach($program_resources as $idx => $resource){
    $prev = $page_info['total_pages'];
    $page_info['total_pages'] += $resource['pages'];
    $chapter = array(
      'title' => $resource['post_title'],
      'slug' => get_relative_permalink($post->ID).'page/'.(intval($prev) + 1),
      'selected' => false
    );
    if( $page_info['total_pages'] >= $current_page && empty($page_info['page']) ){
      $page_info['page'] = $current_page - $prev;
      $page_info['resource_id'] = $resource[$post_table_id];
      $chapter['selected'] = true;
    }
    $page_info['chapters'][] = $chapter;
  }
  return $page_info;
}

function get_resource_cache_page($resource_id, $resource_page){
  global $wpdb;
  $prefix = $wpdb->prefix;
  $resources_table_name = $prefix . 'resources';
  $post_table_name = $prefix . 'posts';
  $post_table_id = $prefix . 'post_id';
  $sql = "
    SELECT * FROM $resources_table_name
    WHERE $post_table_id = $resource_id AND status = 'publish' AND page_order = $resource_page
    LIMIT 1
  ";
  return $wpdb->get_row( $sql , ARRAY_A );
}

// function which returns the wrap of each field in gravity forms
function get_form_field_wrap( $field, $form_id = '' ) {
  $wrapper_class = 'input';
  // assign correct wrapper class
  switch ($field['type']) {
    case 'date':
      $wrapper_class = 'datepicker-wrap';
      break;
    case 'textarea':
    case 'captcha':
      $wrapper_class = 'textarea';
      break;
    default:
      $wrapper_class = 'input';
      break;
  }
  $output = '';
  $output .= '<!-- M70-M71-M80-M81: SECTION -->';
  $output .= '<span class="label">';
  $output .= '  <label class="" for="input_' . $field['id'] . '_' . $form_id . '">' . $field['label'] . ( $field['isRequired'] ? '<span class="gfield_required">*</span>' : '' ) . '</label>';
  $output .= '</span>';
  $output .= '<div class="' . $wrapper_class . ' visible-placeholder ginput_container_' . $field['type'] . '">';
  
  $output .= '%s'; // specific field content comes here 
  
  $output .= '</div>';
  if ( !empty( $field['description'] ) ) {
    $output .= '<div class="note">';
    $output .= '  <p>' . $field['description'] . '</p>';
    $output .= '</div>';
  }
  if ( $field['failed_validation'] && $field['validation_message'] != '' ) {
    $output .= '<div class="validation_message note">';
    $output .= $field['validation_message'];
    $output .= '</div>';
  }
  $output .= '<!-- end M70-M71-M80-M81: SECTION -->';
  return $output;
}
