<?php

function render_m1_hero( $module_data, $style = 'short' ) {
	$output = '';

	if ( !empty( $module_data['bgimage']['image']['fullpath'] ) && empty( $module_data['hidehero'] ) ) {

		if ( empty( $module_data['style'] ) ) {
			$module_data['style'] = $style;
		}

		$output .= '<!-- M1/M57: HERO -->';
		$output .=  '<div style="background-image: url('.$module_data['bgimage']['image']['fullpath'].');" class="banner'.( !empty( $module_data['bottomradius'] ) ? ' banner-curve' : '' ).' text-center">';
		$output .=    '<div class="container">';
		$output .=      '<div class="row">';
		$output .=        '<div class="banner-block ' . $module_data['style'] . '">';
		$output .=          '<div class="banner-text">';
		$output .=            !empty( $module_data['title'] ) ? '<h1>'.$module_data['title'].'</h1>' : '';
		$output .=            !empty( $module_data['copy'] ) ? apply_filters( 'the_content', html_entity_decode( $module_data['copy'] ) ) : '';
		if ( !empty( $module_data['buttontext'] ) ) {
			$output .= !empty( $module_data['linkfield'] ) ? sprintf(
				build_link( $module_data['linkfield'] ),
				'btn btn-default',
				$module_data['buttontext']
			) : '';
		}
		$output .=          '</div>';
		$output .=        '</div>';
		$output .=      '</div>';
		$output .=    '</div>';
		$output .=  '</div>';
		$output .= '<!-- end M1/M57: HERO -->';
	}

	return stripslashes( $output );
}
