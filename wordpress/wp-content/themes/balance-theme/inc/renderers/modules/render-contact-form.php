<?php
/**
 *
 *
 * @note https://www.gravityhelp.com/documentation/article/api-functions/#submit_form
 */
function render_contact_form( $form_id ) {
	global $post;	
	$getval = explode('?',$_SERVER['REQUEST_URI']);
	
	$output = '';
	
	/*if ( isset( $_GET['submit'] ) && $_GET['submit'] == 'success' ) {
		return '<p>Thanks for contacting us! We will get in touch with you shortly.</p>';
	} */
error_reporting(E_ALL);
error_reporting(-1);
ini_set('error_reporting', E_ALL);
date_default_timezone_set('America/Los_Angeles');

	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < 5; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}

session_start(); 

	if(isset($getval[1]) && $getval[1]=='submit=success'){
		//print_r($_POST);
		if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
			//check ip from share internet
					$ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
			//to check ip is pass from proxy
					$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
					$ip = $_SERVER['REMOTE_ADDR'];
			}
			$logDetails = 'client IP: '.$ip.' | Request to speak to a counselor: '.$_POST['input_1'].' | Primary Concern: '.$_POST['input_2'].' | Secondary Concern: '.$_POST['input_8'].' | First Name & Last Name: '.$_POST['input_9'].' | Email: '.$_POST['input_5'].' | Where did you hear about us?: '.$_POST['input_10'];
			error_log($logDetails);
			
		/** code to generate error log **/
			
		    $log_dir = $_SERVER['DOCUMENT_ROOT']."/wp-content";
		    if (!file_exists($log_dir))
		    {
		        // create directory/folder uploads.
		        mkdir($log_dir, 0777, true);
		    }
			$logfile='contact.log';
			if (!file_exists($logfile))
		    {
		        fopen($logfile, "w");
		    }


		    $log_file_data = $log_dir.'/' . $logfile;
			$logDetails = '['.date('Y-m-d H:i:s').']'.$logDetails;
		    file_put_contents($log_file_data, $logDetails . "\r\n", FILE_APPEND);

		    //wp_mail('swati.y@xekera.com','Contact Balance',$logDetails)

		return '<p style="color: #4b6b68;font-weight: 700;">Thanks for contacting us! We will get in touch with you shortly.</p>';
	}else if(isset($getval[1]) && $getval[1]=='submit=restricted'){
		//print_r($_COOKIE);
		if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
			//check ip from share internet
					$ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
			//to check ip is pass from proxy
					$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
					$ip = $_SERVER['REMOTE_ADDR'];
			}
			$logDetails = 'client IP: '.$ip.' | Request to speak to a counselor: '.$_COOKIE['requestVals'].' | Primary Concern: '.$_COOKIE['primaryConcern'].' | Secondary Concern: '.$_COOKIE['secondaselectryConcern'].' | First Name & Last Name: '.$_COOKIE['firstLastName'].' | Email: '.$_COOKIE['emailValue'].' | Where did you hear about us?: '.$_COOKIE['referMedium'];
			error_log($logDetails);
			
		/** code to generate error log **/
			
		    $log_dir = $_SERVER['DOCUMENT_ROOT']."/wp-content";
		    if (!file_exists($log_dir))
		    {
		        // create directory/folder uploads.
		        mkdir($log_dir, 0777, true);
		    }
			$logfile='contactru.log';
			if (!file_exists($logfile))
		    {
		        fopen($logfile, "w");
		    }


		    $log_file_data = $log_dir.'/' . $logfile;
			$logDetails = '['.date('Y-m-d H:i:s').']'.$logDetails;
		    file_put_contents($log_file_data, $logDetails . "\r\n", FILE_APPEND);


		return '<p style="color: #4b6b68;font-weight: 700;">Thanks for contacting us! We will get in touch with you shortly.</p>';
		session_destroy();
	}
	else {

		if ( !empty( $_GET ) ) {
			$output .=  '<p style="color: #4b6b68;font-weight: 700;">There has been an error while submitting the form. Please try again later.</p>';
		}

		$output .='<form method="post" id="gform_'.$form_id.'" action="'.get_relative_permalink( $post->ID ).'?submit=success" class="submit-form" novalidate="novalidate"  onsubmit="return valiEmail(event);">';

		$output .=  '<fieldset>';
		$output .=    '<div class="row">';
		$output .=      '<div class="col-sm-12">';
		$output .=        '<div class="input-group">';
		$output .=  '<input type="hidden" class="formHiddenData" id="formHiddenData">';
		$output .=          '<select aria-label="Request to speak to a counselor" name="input_1" id="input_'.$form_id.'_1" class="select1 form-control inner-select" tabindex="1">';
		$output .=            '<option value="Request to speak to a counselor">Request to speak to a counselor</option>';
		$output .=            '<option value="General contact">General contact</option>';
		$output .=          '</select>';
		$output .=        '</div>';
		$output .=        '<div class="input-group">';
		$output .=          '<select aria-label="Primary Concern" name="input_2" id="input_'.$form_id.'_2" class="select2 form-control inner-select" tabindex="2">';
		$output .=            '<option value="Primary Concern">Primary Concern</option>';
		$output .=            '<option value="Debt/Budget Counseling">Debt/Budget Counseling</option>';
		$output .=            '<option value="Bankruptcy Services">Bankruptcy Services</option>';
		$output .=            '<option value="Foreclosure Prevention Counseling">Foreclosure Prevention Counseling</option>';
		$output .=            '<option value="Student Loan Counseling">Student Loan Counseling</option>';
		$output .=            '<option value="Credit Report Review">Credit Report Review</option>';
		$output .=            '<option value="Other">Other</option>';
		$output .=          '</select>';
		$output .=        '</div>';
		$output .=        '<div class="input-group">';
		$output .=          '<select aria-label="Secondary Concern" name="input_8" id="input_'.$form_id.'_8" class="select3 form-control inner-select" tabindex="3">';
		$output .=            '<option value="Secondary Concern">Secondary Concern</option>';
		$output .=            '<option value="Debt/Budget Counseling">Debt/Budget Counseling</option>';
		$output .=            '<option value="Bankruptcy Services">Bankruptcy Services</option>';
		$output .=            '<option value="Foreclosure Prevention Counseling">Foreclosure Prevention Counseling</option>';
		$output .=            '<option value="Student Loan Counseling">Student Loan Counseling</option>';
		$output .=            '<option value="Credit Report Review">Credit Report Review</option>';
		$output .=            '<option value="Other">Other</option>';
		$output .=          '</select>';
		$output .=        '</div>';
		$output .=        '<div class="input-group">';
		$output .=          '<input aria-label="Input for first name and last name" name="input_9" id="input_'.$form_id.'_9" type="text" placeholder="First Name &amp; Last Name" data-required="true" class="form-control" tabindex="4">';
		$output .=        '</div>';
		$output .=        '<div class="input-group">';
		$output .=          '<input aria-label="Input for email" name="input_5" id="input_'.$form_id.'_5" type="email" placeholder="Email" data-required="true" class="form-control" tabindex="5">';
		$output .=        '<p id="emailError" style="color:red"></p>';
		$output .=        '</div>';
		$output .=        '<div class="input-group">';
		$output .=          '<select aria-label="Where did you hear about us?" name="input_10" id="input_'.$form_id.'_10" class="select4 form-control inner-select" tabindex="6">';
		$output .=            '<option value="Where did you hear about us?">Where did you hear about us?</option>';
		$output .=            '<option value="Google">Google</option>';
		$output .=            '<option value="Credit">Credit Union</option>';
		$output .=            '<option value="Friend">Friend</option>';
		$output .=          '</select>';
		$output .=        '</div>';
		// new msg start testing
		// $output .=        '<div class="input-group">';
		// $output .=          '<select aria-label="Message dropdown" name="input_10" id="input_'.$form_id.'_10" class="select4 form-control inner-select" tabindex="6">';
		// $output .=            '<option value="Message dropdown">Message dropdown</option>';
		// $output .=            '<option value="msg1">msg1</option>';
		// $output .=            '<option value="msg2">msg2</option>';
		// $output .=            '<option value="msg3">msg3</option>';
		// $output .=          '</select>';
		// $output .=        '</div>';

		$output .=        '<div class="input-group gfield gform_validation_container">';
		$output .=          '<input aria-label="validation container" name="input_11" id="input_'.$form_id.'_11" type="text" placeholder="Phone" class="form-control" tabindex="11">';
		$output .=        '</div>';
		
		$output .='<script src="https://www.google.com/recaptcha/api.js" async defer ></script>';
		$output .='<div class="g-recaptcha" data-sitekey="6Lfoef0UAAAAAFLg_9otF-qvWMaqHziBqPo77Gsd"  data-callback="enableBtn"></div> <p id="grecaptchamsg" style="color:red"></p>';
		
		/*$output .=        '<div class="input-group">';
		$output .=        '<input type="hidden" id="captchidhidden" value="'.$randomString.'">';
		
		$output .= 			'Enter the code : <span style="background-color:#ccc;padding:10px;font-weight:bold;font-size:20px;">'.$randomString.'</span><br><br><input type="text"  class="form-control" id="captchid" autofill="off">';
		
		
		$output .=        '<p id="grecaptchamsg" style="color:red"></p>';
		$output .=        '</div>';*/
		
		$output .=      '</div>';
		// $output .=      '<div class="col-sm-6">';
		// $output .=        '<div class="input-group">';
		// $output .=          '<textarea aria-label="Text area for message" name="input_7" id="input_'.$form_id.'_7" cols="30" rows="10" placeholder="Message" data-required="true" class="form-control" tabindex="7"></textarea>';
		// $output .=        '</div>';
		// $output .=      '</div>';
		$output .= '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>';
		$output .= "<script>function captchValid(){	
			var captchid = $('#captchid').val().trim();
			var captchidhidden = $('#captchidhidden').val();		
			
			if (captchid==captchidhidden) {
				$('#grecaptchamsg').html('');
				$('#grecaptchamsg').removeClass('pmsg');
				
				return true;
				
			} else {            
				$('#grecaptchamsg').html('Please enter captcha value.');
				$('#grecaptchamsg').addClass('pmsg')		
				
				return false;
			}	
		}
		function enableBtn(){
        document.getElementById('gform_submit_button_1').disabled = false;
    }
		function valiEmail(event){
        event.preventDefault();
			var requestVal = $('select[name=input_1]').val();
			var primaryConcern = $('select[name=input_2]').val();
			var secondaryConcern = $('select[name=input_8]').val();
			var firstLastName = $('input[name=input_9]').val();
			var emailValue = $('input[name=input_5]').val();
			var referMedium = $('select[name=input_10]').val();
			
			if(firstLastName != '' && emailValue != '') {
			if (emailValue.includes('.ru') == false) {	
        		document.getElementById('gform_1').submit();
				return true;
			}else{
		//		Session['requestVal'] = requestVal;
				$.cookie('requestVals', requestVal);
				$.cookie('primaryConcern', primaryConcern);
				$.cookie('secondaselectryConcern', secondaryConcern);
				$.cookie('firstLastName', firstLastName);
				$.cookie('emailValue', emailValue);
				$.cookie('referMedium', referMedium);

				window.location.href = '/about-us/contact-us/?submit=restricted';
				return false;
			}
		}
		}

		</script>";
		
		
		
		
		$output .=    '</div>';
		
		
		
		$output .=    '<div class="submit-holder">';
		$output .=      '<input type="submit" id="gform_submit_button_'.$form_id.'" value="Submit" class="but btn btn-primary form-control" tabindex="8" name="submitx" disabled style="float:left;">';
		$output .=  '<input type="hidden" class="gform_hidden" name="is_submit_'.$form_id.'" value="1">';
		$output .=  '<input type="hidden" class="gform_hidden" name="gform_submit" value="'.$form_id.'">';
		$output .=  '<input type="hidden" class="gform_hidden" name="gform_unique_id" value="">';
		$output .=  '<input type="hidden" class="gform_hidden" name="state_'.$form_id.'" value="WyJbXSIsIjM5ZGZhYzgwZTZiYTdjOGNlODRlYjNmNDg4NTk4MzZjIl0=">';
		$output .=  '<input type="hidden" class="gform_hidden" name="gform_target_page_number_'.$form_id.'" id="gform_target_page_number_'.$form_id.'" value="0">';
		$output .=  '<input type="hidden" class="gform_hidden" name="gform_source_page_number_'.$form_id.'" id="gform_source_page_number_'.$form_id.'" value="1">';
		$output .=  '<input type="hidden" name="gform_field_values" value="">';
		$output .=    '</div>';
		$output .=  '</fieldset>';
		$output .='</form>';

		return stripslashes( $output );

	}

}
