<?php
function print_args($args) {
  var_dump(func_get_args($args));
}
function render_individual_resources( $resources, $section_title = "" ) {
  global $resources_cpts;

  $output = '';
  if( !empty( $resources ) ) {
    $args = array(
      'posts_per_page'   => -1,
      'offset'           => 0,
      'category'         => '',
      'category_name'    => '',
      'orderby'          => 'date',
      'order'            => 'DESC',
      'include'          => '',
      'exclude'          => '',
      'meta_key'         => '',
      'meta_value'       => '',
      'post_type'        => $resources_cpts,
      'post_mime_type'   => '',
      'post_parent'      => '',
      'author'     => '',
      'post_status'      => 'publish',
      'suppress_filters' => true ,
      'post__in' => explode( ',', $resources ),
    );
    $resource_data = get_posts($args);
    foreach( $resource_data as $resource ) {
      $resource_thumbnail = get_post_thumbnail_id($resource->ID);

      $resource_thumbnail = wp_get_attachment_url( $resource_thumbnail );

      if( !empty($resource_thumbnail) ) {

        $resource_thumbnail = '
          <span class="icon">
            <img alt="'.stripslashes($resource->post_title).'" src="'.$resource_thumbnail.'" />
          </span>
        ';
      }else{
        $resource_thumbnail = '
          <span class="icon">
            <span class="icon-program-'.get_post_type($resource->ID).'"></span>
          </span>
        ';
      }
      $output .= '
        <div class="col-sm-6 col-md-4">
          <article class="finance-wrap" aria-label="article thumbnail '.str_replace(array("'",'"'),'', $resource->post_title." Section:". $section_title).'">
            '.$resource_thumbnail.'
            <div class="text-holder">
              <h2>'.stripslashes($resource->post_title).'</h2>
              <p class="height-finance-wrap" style="height: 50px;">'.stripslashes($resource->post_excerpt).'</p>
              <a aria-label="Read more resource '.$resource->ID.'" href="'.get_relative_permalink($resource->ID).'" class="text-read-more text-warning">'.__('READ MORE', 'balance').'</a>
            </div>
          </article>
        </div>
      ';
    }
  }
  return $output;
}

function render_m56b_program_sections( $module_data ) {
  $output = '';

  foreach ($module_data as $key => $section) {
    if( !empty( $section['title'] ) && !empty( $section['list'] ) ){

      $individual_resources = render_individual_resources($section['list'], $section['title']);

      $output.='
        <div aria-label="'.$section['title'].'" class="border-divider"></div>
        <section aria-label="'.$section['title'].'"  class="inner-block same-height-holder">
          <div class="container">
            <div class="row">
              <h1 class="text-center text-info">'.$section['title'].'</h1>
              <div class="col-holder">
                '.$individual_resources.'
              </div>
            </div>
          </div>
        </section>';
    }
  }
  return stripslashes( $output );
}
