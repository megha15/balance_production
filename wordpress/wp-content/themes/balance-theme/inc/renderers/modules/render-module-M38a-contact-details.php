<?php

function render_m38a_contact_details( $module_data ) {
	$output = '';

	$output .=	'<section aria-label="contacts" class="block background-white">';
	$output .=	  '<div class="container">';
	$output .=	    '<div class="row">';

	$output .=     !empty( $module_data['title'] ) ? '<h1 class="text-info text-touch">'.$module_data['title'].'</h1>' : '';

	$output .=     !empty( $module_data['address'] ) ? '<address class="address modules">'.preg_replace('/\r\n|[\r\n]/', '<br>', $module_data['address']).'</address>' : '';

	if( !empty( $module_data['phone'] ) || !empty( $module_data['fax'] ) ){

			$output .= '<span class="tel-holder hidden-xs">';

			$output .=     !empty( $module_data['phone'] ) ? '<span class="tel-wrap">T:<a href="tel:'.str_replace("-", "", $module_data['phone']).'"> ' . $module_data['phone'] . '</a></span>' : '';

			$output .=     !empty( $module_data['phone'] ) && !empty( $module_data['fax'] ) ? "|" : "";

			$output .=     !empty( $module_data['fax'] ) ? '<span class="tel-wrap">F:<a href="tel:'.str_replace("-", "", $module_data['fax']).'"> ' . $module_data['fax'] . '</a></span>' : '';

			$output .= '</span>';

		}

		/*Get id of the form and render it if Contact Us*/
		$forms = GFAPI::get_forms();

		foreach ($forms as $form) {
			if( $form['title'] == "Contact Us" ){
				$output .= render_contact_form($form['fields'][0]['formId']);
				break;
			}
		}

		$output .=	     '</div>';
		$output .=	   '</div>';
		$output .=	'</section>';

	return stripslashes( $output );
}
