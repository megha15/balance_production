<?php

function render_m89_form_upload_field( $field, $form_id = '', $value = '' ) {
  $output_field = '';
  if ( $value == '' ) {
    $value = 'Select File...';
  }
  $output_field .=  '<div class="upload-wrapper btn btn-warning">';
  $output_field .=  '  <span>Upload</span>';
  $output_field .=  '  <input id="input_' . $field['id'] . '_' . $form_id . '" name="input_' . $field['id'] . '" type="file" value="' . $value . '" data-value="' . $value . '" class="upload">';
  $output_field .=  '</div>';
  $output_field .=  '<label class="uploaded-file">' . $value . '</label>';

  $output = '';
  $output = sprintf( get_form_field_wrap( $field, $form_id ), $output_field );

  return stripslashes( $output );
}
