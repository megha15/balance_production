<?php

function render_article_short_quiz( $module_data, $successMessage = 'Correct', $failMessage = 'Incorrect', $wide = false) {

  $output = '';
  $answers = '';
  if(empty($successMessage)){
    $successMessage = __('Correct', 'balance');
  }

  if(empty($failMessage)){
    $failMessage = __('Correct', 'balance');
  }


  foreach($module_data[0]['answers'] as $idx=>$answer) {
    $correct = empty($answer['is_correct']) ? '' : ' correct="correct" ';
    if( !empty($answer['answer']) ) {
      $answers .= '   <li>';
      $answers .= '     <label for="input'.$idx.'">';
      $answers .= '       <input id="input'.$idx.'" type="radio" name="radio-group" '.$correct.'>';
      $answers .= '       <span class="fake-input"></span>';
      $answers .= '       <span class="fake-label">'.stripslashes($answer['answer']).'</span>';
      $answers .= '     </label>';
      $answers .= '   </li>';
    }
  }
  if( !empty( $answers ) ){
    $output = '
    <article style="background-color: #02a69c;" class="text-block" aria-label="article short quiz">
      <div class="container">
        <div class="row">
          <div class="testing-block col-sm-8">
            <h1><span class="icon-quiz hidden-xs"></span>'.__('Test Your Knowledge').'</h1>
            <p>'.stripslashes($module_data[0]['question']).'</p>
            <form action="#" class="knowledge-form">
              <fieldset>
                <div class="quiz-questions">
                  <ul class="choose-list chose-option">
                    '.$answers.'
                  </ul>
                  <div class="btn-holder visible-xs">
                    <input type="submit" value="'.__('Submit', 'balance').'" class="btn btn-warning">
                  </div>
                  <div class="btn-holder hidden-xs">
                    <input type="submit" value="'.__('Submit Answer', 'balance').'" class="btn btn-warning">
                  </div>
                </div>
              </fieldset>
            </form>
            <div class="note-block success">
              <a href="#" class="icon-check"></a>
              <span class="text">'.stripslashes($successMessage).'</span>
            </div>
            <div class="note-block fail">
              <a href="#" class="icon-close"></a>
              <span class="text">'.stripslashes($failMessage).'</span>
            </div>
          </div>
        </div>
      </div>
    </article>';
  }
  return $output;
}