<?php

function render_m52_hero_with_login( $module_data ) {
	$output = '';

	// output hero with login form if login enabled
	if ( !empty( $module_data['login']['loginshow'] ) ) {
		if ( !empty( $module_data['image']['image']['fullpath'] ) ) {
			$module_data['bgimage'] = $module_data['image'];
			$output .= '<!-- M52: HERO WITH LOGIN -->';

			$output .='	    <div class="banner-wrap">';
			$output .='        <div class="hero-block">';
			$output .='          <div style="background: url(' . $module_data['bgimage']['image']['fullpath'] . ') no-repeat 50% 50%/cover" class="image-sm"></div>';
			$output .='          <div style="background: url(' . $module_data['bgimage']['image']['fullpath'] . ') no-repeat 50% 50%/cover" class="image"></div>';
			$output .='          <div class="hero">';
			$output .='            <div class="container">';
			$output .='              <div class="row">';
			$output .='                <div class="col-sm-7 col-md-8">';
			$output .='                  <div class="description">';
			$output .=                      !empty( $module_data['title'] ) ? '<h1>'.$module_data['title'].'</h1>' : '';
			$output .=   										!empty( $module_data['copy'] ) ? apply_filters( 'the_content', html_entity_decode( $module_data['copy'] ) ) : '';
			$output .='                  </div>';
			$output .='                </div>';
			$output .='              </div>';
			$output .='            </div>';
			$output .='          </div>';
			$output .='        </div>';
			$output .='        <div id="login-wrap" class="login-form-section">';
			$output .='          <div class="container">';
			$output .='            <div class="row">';
			$output .='              <div class="col-sm-5 col-sm-offset-7 col-md-4 col-md-offset-8">';
			$output .='                <div id="accordion-form" role="tablist" aria-multiselectable="true" class="login-form-wrap">';
			$output .='                  <div class="panel">';
			$output .='                    <form id="login-form2" action="{{login_form_action}}" aria-expanded="true" class="login-form2 collapse in" method="POST">';
			$output .='                      {{login_form_token}}';
			$output .='                      <div class="input-section">';
			$output .='                        <div class="title"><strong class="h1">' . __('Log In', 'balance') . '</strong>';
			$output .='                          <p>' . __('Sign in to your BALANCE account.', 'balance') . '</p>';
			$output .='                        </div>';
			$output .='						   {{errors}}';
			$output .='                        <div class="input-block-group">';
			$output .='                          <div class="input-wrap">';
			$output .='                            <label for="email" class="visible-xs-inline-block">' . __('Email', 'balance') . '</label>';
			$output .='                            <input id="email" name="email" type="email" placeholder="' . __('Email', 'balance') . '">';
			$output .='                          </div>';
			$output .='                          <div class="input-wrap">';
			$output .='                            <label for="password" class="visible-xs-inline-block">' . __('Password', 'balance') . '</label>';
			$output .='                            <input id="password" name="password" type="password" placeholder="' . __('Password', 'balance') . '">';
			$output .='                            <div class="note visible-xs">';
			$output .='                              <p>' . __('8 characters, no spaces', 'balance') . '</p>';
			$output .='                            </div>';
			$output .='                          </div>';
			$output .='                        </div>';
			$output .='                        <div class="button">';
			$output .='                          <input type="submit" value="' . __('Log In', 'balance') . '" class="btn btn-warning">';
			$output .='                        </div>';
			$output .='                        <div class="links text-capitalize"><a role="button" data-toggle="collapse" href="#forgot-form2" aria-expanded="false" aria-controls="forgot-form2" data-parent="#accordion-form">' . __('Forgot password ?', 'balance') . '</a></div>';
			$output .='                        <div class="links text-capitalize">'.  __("Don't have an account?", 'balance') . ' <a data-toggle="modal" data-target="#Modal1" href="#Modal1" class="dropdown-toggle">' . __('Create one now.', 'balance') . '</a></div>';
			$output .='                      </div>';
			$output .='                      <div class="input-section hide">';
			$output .='                        <div class="title"><strong class="h1">' . __('Log In', 'balance') . '</strong>';
			$output .='                          <p>' . __('Sign in to your Balance account.', 'balance') . '</p>';
			$output .='                        </div>';
			$output .='                        <div class="input-block-group block-group-2">';
			$output .='                          <div class="input-wrap">';
			$output .='                            <label for="email-1" class="visible-xs-inline-block">' . __('Email', 'balance') . '</label>';
			$output .='                            <input id="email-1" name="" type="email" placeholder="' . __('Email','balance').'">';
			$output .='                            <div class="note">';
			$output .='                              <p>' . __('We will send a password reset link to your email address if you have an account with us.', 'balance') . '</p>';
			$output .='                            </div>';
			$output .='                          </div>';
			$output .='                        </div>';
			$output .='                        <div class="button">';
			$output .='                          <input type="submit" value="'.__('Reset Password', 'balance') . '" class="btn btn-warning">';
			$output .='                        </div>';
			$output .='                        <div class="links login"><a href="#">'.__('Log in', 'balance') . '</a></div>';
			$output .='                      </div>';
			$output .='                      <div class="input-section reset-success hide">';
			$output .='                        <div class="title visible-xs"><strong class="h1">'.__('Log In', 'balance') . '</strong></div>';
			$output .='                        <div class="text-wrap">';
			$output .='                          <p>'.__('We found an account matching your email address. Please check your email for a password reset email from us and follow the instructions in that email.', 'balance') . '</p>';
			$output .='                        </div>';
			$output .='                        <div class="button visible-xs">';
			$output .='                          <input type="submit" value="'.__('Log In', 'balance') . '" class="btn btn-warning">';
			$output .='                        </div>';
			$output .='                        <div class="links hidden-xs"><a href="#">'.__('Login', 'balance') . '</a></div>';
			$output .='                      </div>';
			$output .='                    </form>';
			$output .='                  </div>';
			$output .='                  <div class="panel">';
			$output .='                    <form id="forgot-form2" action="{{forgot_password_form_action}}" class="forgot-form2 collapse" method="POST">';
			$output .='                      {{forgot_password_form_token}}';
			$output .='                      <div class="input-section">';
			$output .='                        <div class="title"><strong class="h1">'.__('Forgotten Password', 'balance') . '</strong>';
			$output .='                          <p>'.__('Reset your password using your registered email with BALANCE. To provide a secure process, we will send this information to you via e-mail.', 'balance') . '</p>';
			$output .='                        </div>';
			$output .='                        <div class="input-block-group">';
			$output .='                          <div class="input-wrap">';
			$output .='                            <label for="email2" class="visible-xs-inline-block">' . __('Email', 'balance') . '</label>';
			$output .='                            <input id="email2" type="email" placeholder="' . __('Email', 'balance') . '">';
			$output .='                            <div class="visible-xs mobile-block">';
			$output .='                              <p>'. __('Click on the Reset Password link to reset your password with a new one. If the e-mail address you provided is not in the system you will not receive an e-mail.', 'balance') . '</p>';
			$output .='                            </div>';
			$output .='                          </div>';
			$output .='                        </div>';
			$output .='                        <div class="form-text hidden-xs">';
			$output .='                          <p>'. __('Click on the Reset Password link to reset your password with a new one. If the e-mail address you provided is not in the system you will not receive an e-mail.', 'balance') . '</p>';
			$output .='                        </div>';
			$output .='                        <div class="button">';
			$output .='                          <input type="submit" class="btn btn-warning reset-btn" value="' . __('Reset Password', 'balance') . '">';
			$output .='                        </div>';
			$output .='                        <div class="links text-capitalize"><a role="button" data-toggle="collapse" href="#login-form2" aria-expanded="false" aria-controls="login-form2" data-parent="#accordion-form">' . __('Login', 'balance') . '</a></div>';
			$output .='                      </div>';
			$output .='                    </form>';
			$output .='                  </div>';
			$output .='                  <div class="panel">';
			$output .='                    <div id="pw-reset" class="pw-reset-block collapse">';
			$output .='                      <div class="center-block-holder">';
			$output .='                        <div class="center-block">';
			$output .='                          <div class="title visible-xs"><strong class="h1">' . __('Log In', 'balance') . '</strong></div><strong class="message">' . __('We found an account matching your email address. Please check your email for a password reset email from us and follow the instructions in that email.', 'balance') . '</strong>';
			$output .='                          <div class="links text-capitalize"><a role="button" data-toggle="collapse" href="#login-form2" aria-expanded="false" aria-controls="login-form2" data-parent="#accordion-form" class="hidden-xs">' . __('Login', 'balance') . '</a><a role="button" data-toggle="collapse" href="#login-form2" aria-expanded="false" aria-controls="login-form2" data-parent="#accordion-form" class="btn btn-warning visible-xs">' . __('Login', 'balance') . '</a></div>';
			$output .='                        </div>';
			$output .='                      </div>';
			$output .='                    </div>';
			$output .='                  </div>';
			$output .='                </div>';
			$output .='              </div>';
			$output .='            </div>';
			$output .='          </div>';
			$output .='        </div>';
			$output .='      </div>';
			$output .= '<!-- end M52: HERO WITH LOGIN -->';
		}
	} else { // output plain hero if no login
		if ( !empty( $module_data['image']['image']['fullpath'] ) ) {
			// update the correct index
			$module_data['bgimage'] = $module_data['image'];
			$output = render_m1_hero( $module_data ) ;
		}
	}

	return stripslashes( $output );
}
