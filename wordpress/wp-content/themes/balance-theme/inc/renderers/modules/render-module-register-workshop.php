<?php

function render_register_workshop( $data, $secondary_markup = true ) {
	date_default_timezone_set('America/Los_Angeles');
	$output = '';
	
	$output .= '<main><article name="" class="text-block article default-content-style" aria-label="module for article Workshops"> <div class="container">    <div class="row">';
	$output .= '<table class="table-wor">';
	$output .= '<thead><tr><th>Title</th><th>Date</th><th>Time(PST)</th><th>Registration Open Date</th><th>Link to Register</th></tr></thead>';
	$output .= '<tbody>';
	$total_register = count($data['workshop']);	
    
	if($total_register>0){
		$linktocloser = 'Link to be announced closer to event';	
		foreach($data['workshop'] as $m=>$v){	
			$date = date('l, F d, Y',strtotime($v['workshop_end_date']));
			
			$workshop_Link = trim($v['workshop_Link']);
			if($workshop_Link!=''){
				$has_link = stristr($workshop_Link, 'http://') ?: stristr($workshop_Link, 'https://');
				if($has_link){
					$register_link='<a href="'.$workshop_Link.'" target="_blank"><button class="btn">Register</button></a>';
				}
				else $register_link = $workshop_Link;
			}
			else $register_link = $linktocloser;
			
			$workshop_open_date = trim($v['workshop_open_date']);
			if(!empty($workshop_open_date)){
				$open_date =date('m/d/y',strtotime($v['workshop_open_date']));
				$open_date_compare = date('Y-m-d',strtotime($v['workshop_open_date']));			 
			    if($open_date_compare>date('Y-m-d')) {
					$has_link = stristr($workshop_Link, 'http://') ?: stristr($workshop_Link, 'https://');
					if($has_link || empty($workshop_Link)){
						$register_link = $linktocloser;
					}
					else $register_link = $workshop_Link;					
				}		
			}
			else {
				$open_date ='TBA';	
				if($has_link){
					$register_link = $linktocloser;
				}	
			}	
			$end_minute = trim($v['workshop_end_min']);
			if(strlen($end_minute)==1) $end_minute='0'.$end_minute;
			//$end_minute = date("H:i", strtotime($end_minute));
			
			$start_minute = trim($v['workshop_start_min']);
			if(strlen($start_minute)==1) $start_minute='0'.$start_minute;
			//$start_minute = date("H:i", strtotime($start_minute));
			
			$start_time = $v['workshop_start_hour'].':'.$start_minute.' '.$v['workshop_start_am'];
			$end_time = $v['workshop_end_hour'].':'.$end_minute.' '.$v['workshop_end_am'];			
			
			$last_expiry_time = date('Y-m-d H:i:s',strtotime($v['workshop_end_date'].' '.$end_time));			
			
			$current_time=date('Y-m-d H:i:s');	
			if($last_expiry_time<$current_time) continue;//'Link to be announced closer to event';	
			//if($last_expiry_time<$current_time) $register_link = $last_expiry_time.'<'.$current_time;
			
					
			$output .= '<tr>';
			$output .= '<td data-label="Title">'.$v['workshop_register_title'].'</td>';
			$output .= '<td data-label="Date">'.$date.'</td>';
			$output .= '<td data-label="Time">'.$start_time.' to '.$end_time.'</td>';
			$output .= '<td data-label="Registration">'.$open_date.'</td>';
			$output .= '<td data-label="Link">'.$register_link.'</td>';
			$output .= '</tr>';
		}
	}	
	$output .= '</tbody>';
	$output .= '</table>';
	$output .= '</div>  </div></article';
	

	return stripslashes( $output );
}

/*

removed past rec

expiry date->date

EVENT DATE FROM TO IN FORM ADMIN INSTEAD OF EXPIRY DATE

TBA


*/