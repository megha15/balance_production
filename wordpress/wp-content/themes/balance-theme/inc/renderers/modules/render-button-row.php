<?php
  function render_button_row($link, $text = 'Download') {
    $output = '';
    if( !empty( $link ) && !empty( $text ) ){
      $output = '
        <div class="container">
          <div class="row">
            <div class="btn-holder"><a href="'.$link.'" class="btn btn-warning" target="_blank">'.__($text).'</a></div>
          </div>
        </div>';
    }
    return $output;
  }
