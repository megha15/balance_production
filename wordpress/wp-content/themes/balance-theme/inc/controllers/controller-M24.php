<?php

	/**
	 * Parses quiz results, inserts the data into the quiz table and returns data
	 * needed for presentation on frontend.
	 *
	 * @param  [array] Submitted answers (post request).
   * @param  [array] Quiz data returned from rendered component.
	 * @param  [string] if the user is anonymous and we gather his/hers data at the end of the quiz
	 * @return [array] Returns data to be presented on frontend.
	 */
	function m24_parse_quiz_results( $submitted_answers, $quiz_data, $registration_on_the_end ) {
    unset( $submitted_answers['quiz-submission'] );
    if ( $registration_on_the_end && !empty( $submitted_answers['register'] ) && isset( $submitted_answers['wlw'] ) ) {
      $submitted_answers['register']['white_label_website_id'] = $submitted_answers['wlw'];
      $_SESSION['quiz_registration_form_data'] = json_encode($submitted_answers['register']);
      unset( $submitted_answers['register'] );
    }
    unset( $submitted_answers['wlw'] );

		$correct_answers = 0;
		$quiz_results = array();
		foreach ( $submitted_answers as $key => $answer ) {
			$question_index = explode( '-', $key )[1];

      // An index should be numeric
      if ( !is_numeric($question_index) ) continue;

			$question_data = $quiz_data[ $question_index ];
			$correct = m24_is_answer_correct( $question_data, $answer );
			$answer_results = array(
				'correct' => $correct['correct'],
				'correct_index' => $correct['correct_answer'],
				'question_index' => $question_index,
				'answered' => $answer,
			);
			if ( $correct['correct'] ) {
				$correct_answers++;
			}
			$quiz_results[ $question_index ] = $answer_results;
		}
		$not_answered = 0;
		foreach( $quiz_data as $question_index => $question ) {
			if ( ! isset( $quiz_results[ $question_index ] ) ) {
				$not_answered++;
				$answer_results = array(
					'correct' => false,
					'correct_index' => m24_get_correct_index( $question['answers'] ),
					'question_index' => $question_index,
					'answered' => -1,
				);
				$quiz_results[ $question_index ] = $answer_results;
			}
		}
		$total = count( $quiz_data );
		$score = ( $correct_answers / $total ) * 100;

		$current_post_id = get_the_ID();
		$meta = get_post_meta( $current_post_id, '_page_edit_data', true );
		$quiz_data = $meta['quiz_module'][0];

		$max_tries = false;
		$cooldown = false;
		$passed = true;
		$tries_left = false;

		if ( isset( $quiz_data['max_tries'] ) && isset( $quiz_data['cooldown'] ) ) {
			$max_tries = $quiz_data['max_tries'];
			$cooldown = $quiz_data['cooldown'];
			if ( isset( $quiz_data['success_rate'] ) ) {
				$passed = ( $score >= $quiz_data['success_rate'] ) ? true : false;
			}
		}

		$data = array(
			'score' => $score,
			'correct' => $correct_answers,
			'total' => $total,
			'quiz_results' => $quiz_results,
			'max_tries' => $max_tries,
			'cooldown' => $cooldown,
			'passed' => $passed,
		);
		m24_insert_quiz_results( $submitted_answers, $data, $registration_on_the_end );
		return $data;
	}

	/**
	 * Insert quiz results into database.
	 *
	 * @param  [array] Submitted answers (post request).
   * @param  [array] Quiz reuslts data.
   * @param  [bool] if the user is anonymous and we gather his/hers data at the end of the quiz
	 */
	function m24_insert_quiz_results( $submitted_answers, $data, $registration_on_the_end ) {
		global $wpdb;

    if ( !is_user_logged_in() && $registration_on_the_end && !empty($_SESSION['quiz_registration_form_data']) && $data['passed'] ) {
      register_and_login_custom_user(json_decode($_SESSION['quiz_registration_form_data']));
      unset($_SESSION['quiz_registration_form_data']);
    }

		if ( is_user_logged_in() ) {
			$answers = json_encode( $submitted_answers );
			$wpdb->insert(
				$wpdb->prefix . 'quiz_results',
				array(
					'score' => $data['score'],
					'num_correct' => $data['correct'],
					'answers_data' => $answers,
					'wp_user_id' => get_current_user_id(),
					'wp_post_id' => get_the_ID(),
          'transferred' => 0
				)
			);
		}
	}

	/**
	 * Check if user can answer the quiz or not.
	 * @return [boolean]
	 */
	function m24_check_quiz_validity( $quiz_data ) {
		global $wpdb;

		if ( is_user_logged_in() ) {
			$user_id = get_current_user_id();
			$current_post_id = get_the_ID();
			$meta = get_post_meta( $current_post_id, '_page_edit_data', true );
			$quiz_data = $meta['quiz_module'][0];

			if ( isset( $quiz_data['max_tries'] ) && isset( $quiz_data['cooldown'] ) ) {
				$cooldown_period = ( int ) $quiz_data['cooldown'];
				$max_tries = ( int ) $quiz_data['max_tries'];
				$cooldown = $last_try = false;
				$date_try_again = new DateTime('now');

				// check if user has used up all attempts in the last 12 hours (with insufficient score)
				$table_name = $wpdb->prefix . 'quiz_results';
				$sql = $wpdb->prepare( "SELECT * FROM $table_name WHERE wp_user_id = %d AND wp_post_id = %d AND timestamp > NOW() - INTERVAL 12 HOUR AND score < %f ", $user_id, $current_post_id, $quiz_data['success_rate'] );
				$results = $wpdb->get_results( $sql, ARRAY_A );

				if ( count( $results ) > 1 ) {
					$last_try = $results[ count($results) - 1 ]['timestamp'];
					$last_try = new DateTime( $last_try );
					// if a cooldown period has not yet passed from the last try, do not allow reattempting the quiz
					$date_try_again = clone $last_try;
					$date_try_again->add(new DateInterval('PT'.$cooldown_period.'H'));
					$diff = $date_try_again->diff(new DateTime('now'));
					if ( count( $results ) >= $max_tries ) {
						$now = new DateTime('now');
						if ( $date_try_again > $now ) {
							$cooldown = true;
						}
					}
				}
				return array(
					'reattempt'       => true,
					'cooldown'        => $cooldown,
					'last_try'        => $last_try,
					'try_again'       => $date_try_again,
					'attempts_left'   => $max_tries - count( $results ),
					'success_rate'	  => $quiz_data['success_rate'],
					'message_fail'    => $quiz_data['message_fail'],
					'message_success' => $quiz_data['message_success'],
				);
			}
			return true;
		}
		return true;
	}

	/**
	 * Check if provided answer is correct.
	 * @param  [array]  $question_data
	 * @param  [string] $answer
	 * @return [boolean]
	 */
	function m24_is_answer_correct( $question_data, $answer ) {
		$response = array(
			'correct_answer' => false,
			'correct'        => false,
		);

		foreach( $question_data['answers'] as $key => $answer_data ) {
			if ( isset( $answer_data['is_correct'] ) ) {
				$response['correct_answer'] = $key;
				if ( $key == $answer ) {
					$response['correct'] = true;
					break;
				}
			}
		}

		return $response;
	}

	function m24_get_correct_index( $answers ) {
		foreach( $answers as $key => $answer ) {
			if( isset( $answer['is_correct'] ) ) {
				return $key;
			}
		}
	}
