<?php
/**
 * Function that registers menus
 */
function balance_menus_setup() {

  // Register menus that are in basic layout
  register_nav_menus( array(
    'topheader' => __( 'Top header menu', 'balance_theme' ),
    'header' => __( 'Header menu', 'balance_theme' ),
    'footer' => __( 'Footer menu', 'balance_theme' ),
  ));

}
add_action( 'after_setup_theme', 'balance_menus_setup' );

/**
 * Helper to get array of menu items which are not organized in hierarchy (calls wp_get_nav_menu_items())
 *
 * @param $menu_location_string_id string one of the registered menu locations defined in roidna_setup()
 *
 * @return array list of nonhierarchical menu items
 */
function _get_nonhierarchical_menu( $menu_location_string_id ) {
  $locations = get_registered_nav_menus();
  $menus = wp_get_nav_menus();
  $menu_locations = get_nav_menu_locations();
  $menu_items = array();
  if (isset($menu_locations[  $menu_location_string_id ])) {
    foreach ($menus as $menu) {
      // If the ID of this menu is the ID associated with the location we're searching for
      if ($menu->term_id == $menu_locations[ $menu_location_string_id ]) {
        // This is the correct menu
        // Get the items for this menu
        $menu_items = wp_get_nav_menu_items($menu->term_id);
        break;
      }
    }
  }
  return $menu_items;
}

/**
 * Main function to retrieve menu array, hierarchical organized with applied flags for current page
 *
 * @param $menu_location_id string  one of the registered menu locations defined in roidna_setup()
 * @param $parent int id of the parent for which we want to retrieve menu tree (default is 0 which means the whole menu tree)
 * @param $depth int recursive call limit (for how many levels we want to check and run tree generation)
 * @param $startCounterAt int counter at which we want to start counting the menu items (to reset counter to 0 on each recursive call for sub levels)
 *
 * @return array list of hierarchical menu items hierarchical organized with applied flags for current page
 */
function _generate_menu_item_tree($datas, $parent = 0, $depth=0, $startCounterAt=0) {
  $counter = $startCounterAt;
  $return_menu_items = array();

  if ( $depth > 10 ) {
    return $return_menu_items;
  }

  for($i=0, $ni=count($datas); $i < $ni; $i++) {
    if($datas[$i]->menu_item_parent == $parent){

      $url = '';
      $permalink = get_relative_permalink($datas[$i]->object_id);

      if ($datas[$i]->object != 'custom' && !empty($permalink)) {
        $url = $permalink;
      } else {
        $url = $datas[$i]->url;
      }

      // check if there is redirect url set for this object and set for url
      $post_custom = get_post_custom($datas[$i]->object_id);

      if (isset($post_custom) && !empty($post_custom)) {
        if ( isset($post_custom['redirect_url'][0]) && !empty($post_custom['redirect_url'][0]) ) {
          $url = $post_custom['redirect_url'][0];
        }
      }

      $return_menu_items[$datas[$i]->ID] = array(
        'menu_ID' => $datas[$i]->ID,
        'menu_title' => $datas[$i]->title,
        'menu_url' => $url,
        'menu_target' => $datas[$i]->target,
        'menu_description' => $datas[$i]->description,
        'menu_classes' => $datas[$i]->classes,
        'menu_attr_title' => $datas[$i]->attr_title,
        'menu_level' => $depth,
        'menu_order' => $counter,
        'menu_flag' => '', // active, current flag (gets populated when viewing site)
        'menu_parent_ID' => $datas[$i]->menu_item_parent,
        'object' => $datas[$i]->object, // category, custom, page, post, [custom_post_type_id] (customer, partner,...)
        'object_type' => $datas[$i]->type, // taxonomy, custom, post_type
        'object_ID' => $datas[$i]->object_id, // id of the object from the table (term_id, post_id)
        'object_title' => $datas[$i]->post_title,
        'object_parent' => $datas[$i]->post_parent,
        'object_slug' => $datas[$i]->post_name,
        'childs' => _generate_menu_item_tree($datas, $datas[$i]->ID, $depth+1, 0)
      );

      $counter++;
    }
  }
  return $return_menu_items;
}

/**
 * Main function to retrieve menu array, hierarchical organized with applied flags for current page
 *
 * @param $menu_location_id string one of the registered menu locations defined in roidna_setup()
 * @param $parent int id of the parent for which we want to retrieve menu tree (default is 0 which means the whole menu tree)
 * @param $depth int recursive call limit (for how many levels we want to check and run tree generation)
 * @param $startCounterAt int counter at which we want to start counting the menu items (to reset counter to 0 on each recursive call for sub levels)
 * @param $current_lang string slug from Polylang plugin of current language
 *
 * @return array list of hierarchical menu items hierarchical organized with applied flags for current page
 */
function get_menu( $menu_location_id, $parent = 0, $depth=0, $startCounterAt=0 ) {
  global $post;

  /**
   * @todo Menu transients need to be added. Current code is not working properly as it forms problem on menu update. It doesn't show updated values
   */
  // $menu_query = array();
  // $menu_query = get_transient( $menu_location_id . '_query' );
  // if ( $menu_query === false ) {
  //     // if ( !empty($menu_query) ) {
  //     //   set_transient( $menu_location_id . '_query', $menu_query, 60*60*24*365 ); // expiration date one year
  //     // }
  // }

  $menu_query = _generate_menu_item_tree(_get_nonhierarchical_menu($menu_location_id), $parent, $depth, $startCounterAt);

  // apply active and current flag based on current viewed page
  $menu_query_return = _set_flags_on_menu_items( $menu_query );

  return $menu_query_return;
}


/**
 * Helper to set flag on menu items which page are we currently browsing (compares current url with url of the menu items)
 *
 * @param $menu_items_array array list of menu items to organize in hierarchy
 *
 * @return array same array from params with applied 'current' flags
 */
function _set_flags_on_menu_items( $menu_items_array ) {
  global $post;
  global $wp;
  if( isset( $post ) && !empty( $post ) ){
    foreach($menu_items_array as & $menu_item) {
      if ($menu_item['object_ID'] == $post->ID) {
        $menu_item['menu_flag'] = 'current active';
      }
      $menu_item['childs'] = _set_flags_on_menu_items( $menu_item['childs'] );
    }
  }
  return $menu_items_array;
}

?>
