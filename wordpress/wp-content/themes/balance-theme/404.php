<?php
header("HTTP/1.0 404 Not Found - Archive Empty");
$wp_query->set_404(); get_header(); 
?>

<article class="text-block article default-content-style" aria-label="article page not found">
	<div class="container">
		<div class="row">
			<h1 class="text-info text-center"><?php _e( '404 Not Found', 'balance' ); ?></h1>
			<p class="text-center"><?php _e( 'The page you requested cannot be found.' ); ?></p>
		</div>
	</div>
</article>

<?php get_footer(); ?>
