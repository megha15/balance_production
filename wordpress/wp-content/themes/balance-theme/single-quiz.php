<?php
/*
Modules: {"m34[0]":{"name":"M34: Section"},"quiz_settings[0]":{"name":"Quiz Settings"},"m24[0]":{"name":"M24: Quiz Questions"}}
*/

?>
<?php
global $additional_body_class, $post, $data;
Balance_Resources_Public::increment_resource_view_count( $post->ID, 1, 0 );
$additional_body_class = 'single-quiz';
get_custom_data();

// get header based on user type
$plain = getHeaderType();
get_header( $plain );

get_template_part( 'partials/quiz' );

get_footer( $plain );

?>
