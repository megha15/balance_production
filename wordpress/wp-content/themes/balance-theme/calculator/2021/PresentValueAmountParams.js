


  KJE.parameters.set("CALC_PV_GOAL",true);
  KJE.parameters.set("COMPOUND_INTEREST",KJE.DatePeriods.PERIOD_YEAR);
  KJE.parameters.set("ERROR_MSG2","Time elapsed must be at least one period");
  KJE.parameters.set("FUTURE_DATE","NEXT_MONTH");
  KJE.parameters.set("MSG_CAT_LABEL1","Present value of initial amount");
  KJE.parameters.set("MSG_CAT_LABEL3","Present value");
  KJE.parameters.set("MSG_ENTER_INFO","Enter all fields below to calculate Present Value");
  KJE.parameters.set("MSG_RESULT","Calculated Present Value is PRESENT_VALUE");
  KJE.parameters.set("PRESENT_DATE","TODAY");
  KJE.parameters.set("PRESENT_VALUE",0);
  KJE.parameters.set("RATE_OF_RETURN",KJE.Default.RORMarket);
  KJE.parameters.set("MSG_DROPPER_TITLE", "Present value inputs:");
KJE.parameters.set("MSG_GRAPH_TITLE","Present Value");
KJE.parameters.set("AMOUNT_ONLY",true);




/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2022 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="https://www.dinkytown.net">https://www.dinkytown.net</A>
 -->
 */


