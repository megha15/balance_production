


    KJE.parameters.set("ADJUSTED_GROSS_INCOME",40000);
    KJE.parameters.set("AGE_OF_RETIREMENT",65);
    KJE.parameters.set("ANNUAL_CONTRIBUTION",3000);
    KJE.parameters.set("CONTRIBUTE_MAX",false);
    KJE.parameters.set("CURRENT_AGE",29);
    KJE.parameters.set("CURRENT_TAX_RATE",KJE.Default.TaxRate);
    KJE.parameters.set("EMPLOYER_YESNO",true);
    KJE.parameters.set("MARRIED_YESNO",true);
    KJE.parameters.set("RATE_OF_RETURN",KJE.Default.RORMarket);
    KJE.parameters.set("RETIREMENT_TAX_RATE",KJE.Default.TaxRateRetire);

    

/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2022 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="https://www.dinkytown.net">https://www.dinkytown.net</A>
 -->
 */


