


KJE.parameters.set("SHOW_HEALTH_PREMIUMS",true);
KJE.parameters.set("WITHHOLD_COUNT",2);
KJE.parameters.set("HEALTH_PREMIUMS",0);
KJE.parameters.set("HOURS_IN_PERIOD1",0);
KJE.parameters.set("HOURS_IN_PERIOD2",0);
KJE.parameters.set("HOURS_IN_PERIOD3",0);
KJE.parameters.set("POST_REIMBURSE",0);
KJE.parameters.set("POST_TAX",0);
KJE.parameters.set("PRE_OTHER",0);
KJE.parameters.set("RETIRE_PLAN_PERCENT",0);
KJE.parameters.set("STATE_TAX",0.00);
KJE.parameters.set("WAGE_IN_PERIOD1",0);
KJE.parameters.set("WAGE_IN_PERIOD2",0);
KJE.parameters.set("WAGE_IN_PERIOD3",0);

KJE.parameters.set("MSG_HOURS_IN_PERIOD1","Regular hours worked");
KJE.parameters.set("MSG_HOURS_IN_PERIOD2","Overtime hours worked");
KJE.parameters.set("MSG_HOURS_IN_PERIOD3","Other hours worked");
KJE.parameters.set("MSG_WAGE_IN_PERIOD1","Regular pay per hour");
KJE.parameters.set("MSG_WAGE_IN_PERIOD2","Overtime pay per hour");
KJE.parameters.set("MSG_WAGE_IN_PERIOD3","Other pay per hour");



/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2022 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="https://www.dinkytown.net">https://www.dinkytown.net</A>
 -->
 */


