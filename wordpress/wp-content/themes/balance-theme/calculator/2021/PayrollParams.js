


  KJE.parameters.set("SHOW_HEALTH_PREMIUMS",true);

  KJE.parameters.set("FILING_STATUS",0);
  KJE.parameters.set("GROSS_EARNINGS1",1000);
  KJE.parameters.set("GROSS_EARNINGS2",1000);
  KJE.parameters.set("HEALTH_PREMIUMS",true);
  KJE.parameters.set("PAY_PERIOD",1);
  KJE.parameters.set("POST_REIMBURSE1",0);
  KJE.parameters.set("POST_REIMBURSE2",0);
  KJE.parameters.set("POST_TAX1",0);
  KJE.parameters.set("POST_TAX2",0);
  KJE.parameters.set("PRE_OTHER1",0);
  KJE.parameters.set("PRE_OTHER2",0);
  KJE.parameters.set("RETIRE_PLAN_PERCENT1",5.0);
  KJE.parameters.set("RETIRE_PLAN_PERCENT2",5.0);
  KJE.parameters.set("STATE_TAX1",6);
  KJE.parameters.set("STATE_TAX2",6);
  KJE.parameters.set("W4_TWOJOBS",false);
  KJE.parameters.set("QUALIFYING_CHILDREN",0);
  KJE.parameters.set("OTHER_DEPENDENTS",0);




/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2022 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="https://www.dinkytown.net">https://www.dinkytown.net</A>
 -->
 */


