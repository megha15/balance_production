


KJE.parameters.set("INTEREST_ONLY_TERM",7);
KJE.parameters.set("INTEREST_RATE",KJE.Default.RateAdj);
KJE.parameters.set("LOAN_AMOUNT",KJE.Default.MortgageAmt);
KJE.parameters.set("SHOW_ALL_MAXIMUM",30);
KJE.parameters.set("TERM",30);
KJE.parameters.set("TERM_MAXIMUM",40);
KJE.parameters.set("TERM_SHOW_ALL",false);
KJE.parameters.set("PREPAY_TYPE",KJE.Default.PREPAY_NONE);
KJE.parameters.set("PREPAY_STARTS_WITH",1);
KJE.parameters.set("PREPAY_AMOUNT","0");



/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2022 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="https://www.dinkytown.net">https://www.dinkytown.net</A>
 -->
 */


