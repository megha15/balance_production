


  KJE.parameters.set("ADJUSTABLE_MONTHS_FIXED2",12);
  KJE.parameters.set("ADJUSTABLE_RATE_CAP2",12);
  KJE.parameters.set("ADJUSTABLE_RATE_FEQ2",12);
  KJE.parameters.set("ADJUSTABLE_RATE_INCR2",0.25);
  KJE.parameters.set("INTEREST_RATE1",KJE.Default.RateFix30);
  KJE.parameters.set("INTEREST_RATE2",KJE.Default.RateAdj);
  KJE.parameters.set("LOAN_AMOUNT",KJE.Default.MortgageAmt);
  KJE.parameters.set("TERM",30);


/**V3_CUSTOM_CODE**/
/* <!--
  Financial Calculators, &copy;1998-2022 KJE Computer Solutions, Inc.
  For more information please see:
  <A HREF="https://www.dinkytown.net">https://www.dinkytown.net</A>
 -->
 */


