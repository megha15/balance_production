<?php
global $data, $post, $wpdb, $wp_query;

if(!empty($data['m56b_module']) && !empty( $data['m56b_module'][0] ) && !empty( $data['m56b_module'][0]['resource_as_program'] )){
  $resources = $data['m56b_module'][0]['resource_as_program'];
  $page = get_query_var( 'paged', 1 );
  if($page < 1){
    $page = 1;
  }

  $page_info = get_program_pages($resources, $page);
  if( empty($page_info['page']) ){
    require('../404.php');
  }
  $out = get_resource_cache_page($page_info['resource_id'], $page_info['page']);
  if( empty($out) ) {
    require('../404.php');
  }

  if ( !empty( $data['m1_module'] ) && !empty( $data['m1_module'][0] ) ) {
    echo render_m1_hero( $data['m1_module'][0] );
  }
  echo render_m65_chapter_select($page_info['chapters']);

  echo $out['html'];

  echo render_m11_pager('/programs/'.$post->post_name.'/', $page_info['total_pages'], $page);

}else{
  require('../404.php');
}