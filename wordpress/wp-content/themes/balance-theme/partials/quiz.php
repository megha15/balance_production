<?php
global $data, $quiz_data;
/* M34 renderer */
$use_data = empty($quiz_data) ? $data : $quiz_data;
if ( !empty( $use_data['m34_module'] ) && !empty( $use_data['m34_module'][0] ) ) {
	echo render_m34_article_content( $use_data['m34_module'][0], '', true, 'article default-content-style quiz-header', 'text-center' );
}

/* M24 renderer */
if ( !empty( $use_data['quiz_module'] ) && !empty( $use_data['quiz_module'][0] ) ) {
	echo render_m24_quiz_questions( $use_data['quiz_module'][0] );
}
