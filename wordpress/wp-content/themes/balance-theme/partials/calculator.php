<?php
global $data;
if ( !empty( $data['m34_module'] ) && !empty( $data['m34_module'][0] ) ) {
  echo render_m34_section_title_copy( $data['m34_module'][0] );
}

if ( !empty( $data['calculator_module'] ) && !empty( $data['calculator_module'][0] ) ) {
  echo render_m02_section_calculator( $data['calculator_module'][0] );
}
