<?php
/*
Modules: {"m5[0]":{"name":"Podcast"}}

*/

global $additional_body_class, $data, $post;
Balance_Resources_Public::increment_resource_view_count( $post->ID, 1, 0 );
$additional_body_class = 'single-podcast';
get_custom_data();

get_header();

get_template_part('partials/downloadable-resource');
echo render_m23a_may_also_like_resources();
get_footer();
