<?php
/*
Template Name: T52: List of programs
Modules: {"m1[0]":{"name":"M1: Hero"}}
*/

?>
<?php
global $additional_body_class, $data;
$additional_body_class = 'programs-list';
get_custom_data();

get_header();

if ( !empty( $data['m1_module'] ) && !empty( $data['m1_module'][0] ) ) {
	echo render_m1_hero( $data['m1_module'][0] );
}

//render listed programs
echo render_m41b_programs();

get_footer();
?>
