<?php
/*
Modules: {"white_label_websites_tabs[0]":{"name":"Tabs"},"white_label_websites_general_info[0]":{"name":"General Info","visible":"tab-0"},"m52[0]":{"name":"M52: Hero with Login","params":{"media_buttons":true,"quicktags":true},"visible":"tab-1"},"m34[0]":{"name":"M54: Welcome Message","visible":"tab-1"},"m53[0]":{"name":"M53: Special Promo","visible":"tab-1"},"m56_m17[0]":{"name":"M17: Key Selling Points","visible":"tab-1"},"m34[1]":{"name":"M34: Section","params":{"media_buttons":true,"quicktags":true},"visible":"tab-2"},"m18[0]":{"name":"M18: Simple form","visible":"tab-2"},"m53[1]":{"name":"M53: Special Promo","visible":"tab-2"},"m34[2]":{"name":"M34: Section","params":{"media_buttons":true,"quicktags":true},"visible":"tab-3"},"white_label_websites_resources[0]":{"name":"Settings","visible":"tab-3"},"white_label_websites_programs[0]":{"name":"Settings","visible":"tab-4"},"m1[0]":{"name":"M1: Hero","visible":"tab-5"},"m34[3]":{"name":"M34: Section","params":{"media_buttons":true,"quicktags":true},"visible":"tab-5"},"white_label_websites_admins[0]":{"name":"Administrators","visible":"tab-6"}}
*/
?>
<?php
global $additional_body_class;
$additional_body_class = 'single-white-label-website';

get_header();

get_footer();

?>
