<?php
/*
Template Name: T13: Contact us
Modules: {"m1[0]":{"name":"M1: Hero"},"m34[0]":{"name":"M34: Section"},"m38a[0]":{"name":"M38a: Contact details"}}
*/

?>
<?php
global $additional_body_class, $data;
$additional_body_class = 'contact-us';
global $wpdb;
  //require_once dirname( __DIR__ ) . '/wp-load.php';

  $chat_value_data = $wpdb->get_results( "SELECT * FROM wp_chat_setting" );
$chat_value_data = unserialize($chat_value_data[0]->chat_value);
if(isset($chat_value_data['contact_page']) && trim($chat_value_data['contact_page'])=='on'){
get_custom_data();

get_header();

/* M01/M57 renderer */
if ( !empty( $data['m1_module'] ) && !empty( $data['m1_module'][0] ) ) {
	echo render_m1_hero( $data['m1_module'][0] );
}

/* SECTION - TITLE, COPY */
if(isset($chat_value_data['contact_address']) && trim($chat_value_data['contact_address'])=='on'){
    if (!empty($data['m34_module']) && !empty($data['m34_module'][0])) {
        echo render_m34_section_title_copy($data['m34_module'][0]);
    }
}

/* CONTACT FORM AND CONTACT DETAILS */
 if(isset($chat_value_data['contact_form']) && trim($chat_value_data['contact_form'])=='on'){             
    if (!empty($data['m38a_module']) && !empty($data['m38a_module'][0])) {
    echo render_m38a_contact_details($data['m38a_module'][0]);
	}
}
get_footer();
} else {
	echo '<style>
body {
    margin: 0;
    padding: 0;
    width: 100%;
    color: #B0BEC5;
    display: table;
    font-weight: 300;
    font-family: "Lato";
}
.container {
    text-align: center;
    position: fixed;
	  top: 50%;
	  left: 50%;
	  transform: translate(-50%, -50%);
}
.content {
    text-align: center;
    display: inline-block;
}
.title {
    font-size: 72px;
    margin-bottom: 40px;
}
	</style>';
echo '<div class="container">';
echo '    <div class="content">';
echo '        <div class="title">This page is not found.</div>';
echo '    </div>';
echo '</div>';
/*$output .= '<div class="container">';
$output .= '    <div class="content">';
$output .= '        <div class="title">This page is not found.</div>';
$output .= '    </div>';
$output .= '</div>';*/	
}
?>
