
    </main>

    <!-- Footer-->
    <footer class="footer" style="background-image: url(<?php echo get_template_directory_uri();?>/images/img01.png);">
      <div class="container">
        <div class="row">
          <div class="logo-holder"><a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri();?>/images/logo-primary.png" width="198" height="44" alt="Balance"></a></div>
          <ul class="menu-f">
            <?php
            //Footer 1-level menu
            $footer_menu = get_menu("footer", 0, 1, 0); ?>
            <?php if (!empty($footer_menu)){ ?>
                <?php foreach ($footer_menu as $menu_item){?>
                  <li>
                    <a  aria-label="footer menu" href="<?php echo $menu_item['menu_url']; ?>" class="<?php echo (strpos($menu_item['menu_flag'], 'active') !== false ? 'active':''); ?>" <?php echo ( !empty( $menu_item['menu_target'] ) ? ' target="' . $menu_item['menu_target'] . '"' : '' ); ?>><?php echo $menu_item['menu_title']; ?></a>
                  </li>
                <?php } ?>
            <?php } ?>
          </ul>
          <?php
          $settings = get_option( "social_settings" );
          ?>
          <ul class="social-links">
          	<?php
          	$social_icons = array( 'facebook', 'twitter', 'google', 'youtube' );
          	foreach ($social_icons as $social_icon) {
          		if ( !empty( $settings[$social_icon] ) ) { ?>
			<li><a href="<?php echo $settings[$social_icon]; ?>" target="_blank"><span class="icon-<?php echo $social_icon; ?>"></span></a></li>
          		<?php }
          	}
          	?>
          </ul>
          <div class="copyright-footer">
            &copy; <?php echo date("Y"); ?> BALANCE. All rights reserved.
          </div>
        </div>
      </div>
    </footer>
    <script src="https://www.google.com/recaptcha/api.js?render=explicit" async defer></script>
    <?php echo get_option('footer_scripts', ''); ?>

    <?php wp_footer(); ?>
  </body>
</html>
