<?php
/*
Template Name: T17: Life Stages
Modules: {"m1[0]":{"name":"M1: Hero"},"m55[0]":{"name":"Life stages listing"}}
*/

?>
<?php
global $additional_body_class, $data;
$additional_body_class = 'life-stages';
get_custom_data();

get_header();

if ( !empty( $data['m1_module'] ) && !empty( $data['m1_module'][0] ) ) {
	echo render_m1_hero( $data['m1_module'][0] );
}

/* LISTED LIFE STAGES */
if (!empty($data['m55_module']) && !empty($data['m55_module'][0])) {
    echo render_m55_listed_life_stages($data['m55_module'][0]);
}

get_footer();
?>
