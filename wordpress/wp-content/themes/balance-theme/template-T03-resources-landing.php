<?php
/*
Template Name: T03: Resources Landing Page
Modules: {"m34[0]":{"name":"M34: Section"}}
*/

?>
<?php
$url = $_SERVER['REQUEST_URI'];
// and return an associative array which contains its various components 
$url_components = parse_url($url); 

// Use the parse_str() function to parse the 
// string passed via the URL 
parse_str($url_components['query'], $params); 
    
global $additional_body_class, $data;
$additional_body_class = 'resources-landing';
get_custom_data();

get_header();

if ( !empty( $data['m34_module'] ) && !empty( $data['m34_module'][0] ) ) {
  echo render_m34_section_title_copy( $data['m34_module'][0], true );
}
$type = $params['type'];
$pager = $params['pager'];
//echo $type.'--this is type';
//echo $pager.'--this is type pager';
echo render_search_main_design($type, $pager);

get_footer();
?>
