<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://roidna.com
 * @since      1.0.0
 *
 * @package    Balance_Life_Stages
 * @subpackage Balance_Life_Stages/admin/partials
 */
?>


<?php
//Create an instance of our package class...
$table = new WP_Life_Stages_List_Table( 'life_stage' );
//Fetch, prepare, sort, and filter our data...
$table->prepare_items();
//delete all auto saved life stages
$table->delete_revisions_auto_save();
?>
<div class="wrap life-stage-wrap">
    <h1>Life stages <a href="<?php echo admin_url( 'post-new.php?post_type=life_stage' );?>" class="page-title-action">Add New</a></h1>
    <?php
    $unassigned_count = count( $table->get_unassigned_life_experiences() );
    if ( $unassigned_count > 0 ) {
      $class = "update-nag";
      switch ($unassigned_count) {
        case 1:
          $message = "There is <b>$unassigned_count</b> unassigned Life Experience. Assign it a parent otherwise it will not be displayed under the life stages.";
          break;
        default:
          $message = "There are <b>$unassigned_count</b> unassigned Life Experiences. Assign them parent Life Stage otherwise they will not be displayed under the life stages.";
          break;
      }
      echo "<div class=\"update-nag\"> <p>$message</p></div>";
    }
    ?>
    <br><br>
    <ul class="subsubsub">
      <li class="publish">
        <a href="<?php echo admin_url( 'admin.php?page=life_stages_list&post_status=publish'); ?>" class="<?php echo $table->post_status == 'publish' ? 'current' : '' ?>">Published <span class="count">(<?php echo wp_count_posts( 'life_stage' )->publish - $unassigned_count; ?>)</span>
        </a> | 
      </li>
      <li class="unassigned">
        <a href="<?php echo admin_url( 'admin.php?page=life_stages_list&post_status=unassigned'); ?>" class="<?php echo $table->post_status == 'unassigned' ? 'current' : '' ?>">Unassigned Life Experience <span class="count">(<?php echo $unassigned_count ?>)</span>
        </a> | 
      </li>
      <li class="draft">
        <a href="<?php echo admin_url( 'admin.php?page=life_stages_list&post_status=draft'); ?>" class="<?php echo $table->post_status == 'draft' ? 'current' : '' ?>">Draft <span class="count">(<?php echo wp_count_posts( 'life_stage' )->draft ?>)</span>
        </a> | 
      </li>
      <li class="trash">
        <a href="<?php echo admin_url( 'admin.php?page=life_stages_list&post_status=trash'); ?>" class="<?php echo $table->post_status == 'trash' ? 'current' : '' ?>">Trash <span class="count">(<?php echo wp_count_posts( 'life_stage' )->trash ?>)</span>
        </a>
      </li>
    </ul>
    <form id="life-stages" class="<?php echo $table->post_status; ?>" method="get">
        <!-- For plugins, we also need to ensure that the form posts back to our current page -->
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
        <!-- Now we can render the completed list table -->
        <?php $table->display() ?>
    </form>
    
</div>

