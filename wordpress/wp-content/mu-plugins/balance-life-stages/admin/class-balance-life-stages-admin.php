<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://roidna.com
 * @since      1.0.0
 *
 * @package    Balance_Life_Stages
 * @subpackage Balance_Life_Stages/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Balance_Life_Stages
 * @subpackage Balance_Life_Stages/admin
 * @author     ROIDNA <devs@roidna.com>
 */
class Balance_Life_Stages_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Balance_Life_Stages_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Balance_Life_Stages_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/balance-life-stages-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Balance_Life_Stages_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Balance_Life_Stages_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/balance-life-stages-admin.js', array( 'jquery', 'jquery-ui-sortable' ), $this->version, false );

	}

   /**
   * Creates a life stages custom post type
   *
   * @since     1.0.0
   * @uses      register_post_type()
   */
  public function new_cpt_life_stage() {
    $cap_type = 'post';
    $plural = 'Life Stages';
    $single = 'Life Stage';
    $opts['can_export'] = TRUE;
    $opts['capability_type'] = $cap_type;
    $opts['description'] = '';
    $opts['exclude_from_search'] = FALSE;
    $opts['has_archive'] = FALSE;
    $opts['hierarchical'] = TRUE;
    $opts['map_meta_cap'] = TRUE;
    $opts['menu_icon'] = 'dashicons-networking';
    $opts['menu_position'] = 24;
    $opts['public'] = TRUE;
    $opts['publicly_querable'] = TRUE;
    $opts['query_var'] = TRUE;
    $opts['register_meta_box_cb'] = '';
    $opts['rewrite'] = array(
      'slug' => 'life-stages',
      'with_front' => true
    );
    $opts['show_in_admin_bar'] = TRUE;
    $opts['show_in_menu'] = FALSE;
    $opts['show_in_nav_menu'] = TRUE;
    $opts['show_ui'] = TRUE;
    $opts['supports'] = array( 'title', 'page-attributes' );
    $opts['taxonomies'] = array();
    $opts['capabilities']['delete_others_posts'] = "delete_others_{$cap_type}s";
    $opts['capabilities']['delete_post'] = "delete_{$cap_type}";
    $opts['capabilities']['delete_posts'] = "delete_{$cap_type}s";
    $opts['capabilities']['delete_private_posts'] = "delete_private_{$cap_type}s";
    $opts['capabilities']['delete_published_posts'] = "delete_published_{$cap_type}s";
    $opts['capabilities']['edit_others_posts'] = "edit_others_{$cap_type}s";
    $opts['capabilities']['edit_post'] = "edit_{$cap_type}";
    $opts['capabilities']['edit_posts'] = "edit_{$cap_type}s";
    $opts['capabilities']['edit_private_posts'] = "edit_private_{$cap_type}s";
    $opts['capabilities']['edit_published_posts'] = "edit_published_{$cap_type}s";
    $opts['capabilities']['publish_posts'] = "publish_{$cap_type}s";
    $opts['capabilities']['read_post'] = "read_{$cap_type}";
    $opts['capabilities']['read_private_posts'] = "read_private_{$cap_type}s";
    $opts['labels']['add_new'] = __( "Add New {$single}", 'balance' );
    $opts['labels']['add_new_item'] = __( "Add New {$single}", 'balance' );
    $opts['labels']['all_items'] = __( $plural, 'balance' );
    $opts['labels']['edit_item'] = __( "Edit {$single}" , 'balance' );
    $opts['labels']['menu_name'] = __( $plural, 'balance' );
    $opts['labels']['name'] = __( $plural, 'balance' );
    $opts['labels']['name_admin_bar'] = __( $single, 'balance' );
    $opts['labels']['new_item'] = __( "New {$single}", 'balance' );
    $opts['labels']['not_found'] = __( "No {$plural} Found", 'balance' );
    $opts['labels']['not_found_in_trash'] = __( "No {$plural} Found in Trash", 'balance' );
    $opts['labels']['parent_item_colon'] = __( "Parent {$plural} :", 'balance' );
    $opts['labels']['search_items'] = __( "Search {$plural}", 'balance' );
    $opts['labels']['singular_name'] = __( $single, 'balance' );
    $opts['labels']['view_item'] = __( "View {$single}", 'balance' );
    register_post_type( 'life_stage', $opts );
  } // new_cpt_life_stage()




  public function life_stages_menu()
  {
    $page_title = 'Life Stages';
    $menu_title = 'Life Stages';
    $capability = 'activate_plugins';
    $menu_slug  = 'life_stages';
    $function   = array( $this, 'life_stages_admin_page' );// Callback function which displays the page content.
    $icon_url   = 'dashicons-networking';
    $position   = 23;

    add_menu_page($page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position);

    $life_stages_submenu_pages = array();

    $life_stages_submenu_pages[] = array(
      'parent_slug'   => 'life_stages',
      'page_title'    => 'All Life Stages',
      'menu_title'    => 'All Life Stages',
      'menu_slug'     => 'life_stages_list',
      'capability'    => 'activate_plugins', //Ensure only admins see this
      'function'      => array( $this, 'life_stages_admin_page' )
    );

    $life_stages_submenu_pages[] = array(
      'parent_slug'   => 'life_stages',
      'page_title'    => 'Add New',
      'menu_title'    => 'Add New',
      'menu_slug'    => 'post-new.php?post_type=life_stage',
      'capability'    => 'activate_plugins', //Ensure only admins see this
      'function'      => ""
    );

    foreach($life_stages_submenu_pages as $submenu){
        add_submenu_page(
            $submenu['parent_slug'],
            $submenu['page_title'],
            $submenu['menu_title'],
            $submenu['capability'],
            $submenu['menu_slug'],
            $submenu['function']
        );
    }

    //Remove Access Groups
    remove_submenu_page('life_stages','life_stages');

    add_filter('parent_file', array( $this, 'life_stages_set_current_menu' ));
  }

  public function life_stages_admin_page()
  {
    include( plugin_dir_path( __FILE__ ) . 'partials/balance-life-stages-admin-display.php' );
  }

  /**
  * Sets the curent menu to the overview one
  *
  * @since   1.0.0
  * @access  public
  */
  function life_stages_set_current_menu( $parent_file ) {
      global $current_screen, $submenu_file, $pagenow;

      if( $current_screen->post_type == 'life_stage' && $current_screen->action != 'add' ) {
        $parent_file = 'life_stages';
        $submenu_file = 'life_stages_list';
      } elseif ( $pagenow == 'admin.php' && $current_screen->base = 'toplevel_page_resources' ) {
      		$submenu_file = 'life_stages_list';
	}

      return $parent_file;

  }

  public function life_stages_redirect_menu(){
    global $pagenow;
    /* Check current admin page. */
    if($pagenow == 'edit.php' && isset($_GET['post_type']) && $_GET['post_type'] == 'life_stage'){
        wp_redirect(admin_url('/admin.php?page=life_stages_list'), 301);
        exit;
    }
  }

   /**
   * Saves type as _wp_page_template meta and parent as post_parent for life stages
   *
   * @since   1.0.0
   * @access  public
   * @param   int     $post_id    The post ID
   * @param   object    $object     The post object
   * @return  void
   */
  public function save_life_stage_type_and_parent( $post_id, $object ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return $post_id; }
    if ( ! current_user_can( 'edit_post', $post_id ) ) { return $post_id; }
    if ( ! current_user_can( 'edit_page', $post_id ) ) { return $post_id; }
    if( $object->post_type == 'life_stage' ) { // if the type is life_stage
      global $wpdb;
      $prefix = $wpdb->prefix;
      $posts_table_name = $prefix . 'posts';

      $data = get_post_custom( $post_id );
      $unserialized_data = '';

      if ( isset( $data['_page_edit_data'] ) ) {
        $unserialized_data = @unserialize( $data['_page_edit_data'][0] );
      }
      if ( $unserialized_data === false){
        $unserialized_data = array();
      }

      $data = $unserialized_data;

      if ( !empty( $data['life_stage_type'][0]['type'] ) ) {
        $type = $data['life_stage_type'][0]['type'];
        $post_parent = '';

        // if life experience then set its parent to the life stage inserted in autcomplete meta field (of course parent needs to be selected and the post type is not in trash)
        if ( $type == 'life_experience' && !empty( $data['life_stage_type'][0]['multiautocompletefield'] ) && $object->post_status != 'trash' ) {
          $post_parent = $data['life_stage_type'][0]['multiautocompletefield'];

          //if the parent is changed or item has no parent then we update menu_order to max
          if( $object->post_parent != $post_parent || empty($object->post_parent) ){

            //get max menu order number
            $max_menu_order = $wpdb->get_results("SELECT MAX(" . $wpdb->posts . ".menu_order) as max_menu_order FROM $wpdb->postmeta INNER JOIN $wpdb->posts ON " . $wpdb->postmeta . ".post_id = " . $wpdb->posts . ".ID WHERE " . $wpdb->posts . ".post_parent = " . $post_parent, ARRAY_A );

            $update = $wpdb->update(
                $posts_table_name, // table
                array( // columns
                  'menu_order' => $max_menu_order[0]['max_menu_order'] + 1,
                ),
                array( // where
                  'ID' => $data['ID'],
                ),
                array( // format of the columns
                  '%d'
                ),
                array( // where format
                  '%d'
                )
            );

          }

        }

        $update = $wpdb->update(
            $posts_table_name, // table
            array( // columns
              'post_parent' => $post_parent,
            ),
            array( // where
              'ID' => $post_id,
            ),
            array( // format of the columns
              '%d'
            ),
            array( // where format
              '%d'
            )
        );

        // set page template the same as type selection on edit screen (is it life_stage (parent) or life_experience (child))
        update_post_meta($post_id, '_wp_page_template', $data['life_stage_type'][0]['type'] );

        // find all childs of this post type and delete the ones that this post type is parent of
        if ( $type == 'life_experience' || $object->post_status == 'trash' ) {
          $current_post_childs = get_posts( array( 'post_parent' => $object->ID, 'posts_per_page' => '-1', 'post_type' => 'life_stage' ));
          if ( $current_post_childs ) {
            foreach ( $current_post_childs as $current_post_child ) {
              // update post_parent field
              $update = $wpdb->update(
                $posts_table_name, // table
                array( // columns
                  'post_parent' => '',
                ),
                array( // where
                  'ID' => $current_post_child->ID,
                ),
                array( // format of the columns
                  '%d'
                ),
                array( // where format
                  '%d'
                )
              );
              // get the edit module value
              $child_data = get_post_meta( $current_post_child->ID, '_page_edit_data', true );
              // update the edit module value
              if ( !empty( $child_data['life_stage_type'][0]['multiautocompletefield'] ) ) {
                $child_data['life_stage_type'][0]['multiautocompletefield'] = '';
                update_post_meta( $current_post_child->ID, '_page_edit_data', $child_data );
              }
            }
          }
        }
      }
    }
  } // save_meta()


  function set_posts_menu_order_callback() {
    global $wpdb; // this is how you get access to the database
    $prefix = $wpdb->prefix;
    $posts_table_name = $prefix . 'posts';

    $element_list = $_POST['elements'];

    foreach ($element_list as $key => $child_id) {

      $update = $wpdb->update(
          $posts_table_name, // table
          array( // columns
            'menu_order' => $key + 1,
          ),
          array( // where
            'ID' => $child_id,
          ),
          array( // format of the columns
            '%d'
          ),
          array( // where format
            '%d'
          )
      );

    }

    // echo $element_list[0];

    wp_die(); // this is required to terminate immediately and return a proper response
  }

}
