<?php

if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

/**
 * Extended WP_List_Table class for resources overview  listing
 *
 *
 * @package    Balance_Resources
 * @subpackage Balance_Resources/admin
 * @author     ROIDNA <devs@roidna.com>
 */
class WP_Resources_List_Table extends WP_List_Table {

     /**
     * The post_types of this class.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $post_types    The current post_types for which we are building the wp list table.
     */

    private $resource_data;

    /** ************************************************************************
     * REQUIRED. Set up a constructor that references the parent constructor. We
     * use the parent reference to set some default configs.
     ***************************************************************************/
    function __construct( $post_types ){
        global $status, $page;

        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'Resource',     //singular name of the listed records
            'plural'    => 'Resources',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );

        $this->resource_data = array();
        foreach ($post_types as $post_type) {
          $count_post_type_posts = wp_count_posts( $post_type );
          $this->resource_data[] = array(
              'ID'  => $post_type,
              'published'    => $count_post_type_posts->publish,
              'drafts'  => $count_post_type_posts->draft,
              'trashed' => $count_post_type_posts->trash
          );
        }
    }

    /** ************************************************************************
     * Recommended. This is a custom column method and is responsible for what
     * is rendered in any column with a name/slug of 'title'. Every time the class
     * needs to render a column, it first looks for a method named
     * column_{$column_title} - if it exists, that method is run. If it doesn't
     * exist, column_default() is called instead.
     *
     * This example also illustrates how to implement rollover actions. Actions
     * should be an associative array formatted as 'slug'=>'link html' - and you
     * will need to generate the URLs yourself. You could even ensure the links
     *
     *
     * @see WP_List_Table::::single_row_columns()
     * @param array $item A singular item (one full row's worth of data)
     * @return string Text to be placed inside the column <td> (movie title only)
     **************************************************************************/
    function column_title($item){

        //Build row actions
        $actions = array(
          'add' => '<a href="' . admin_url( 'post-new.php?post_type=' . $item['ID'] ) . '">Add New</a>'
        );
        //Return the title contents
        return sprintf('%1$s %2$s',
            /*$1%s*/ '<a class="row-title" href="' . admin_url( 'edit.php?post_type=' . $item['ID'] ) . '">' . ucfirst( $item['ID'] ) . 's</a>',
            /*$2%s*/ $this->row_actions($actions)
        );
    }

    function column_published($item){
        if ( $item['published'] == '0' ) {
          return '—';
        } else {
          return '<a class="row-title" href="' . admin_url( 'edit.php?post_status=publish&post_type=' . $item['ID'] ) . '">' . $item['published'] . '</a>';
        }
    }

    function column_drafts($item){
        if ( $item['drafts'] == '0' ) {
          return '—';
        } else {
          return '<a class="row-title" href="' . admin_url( 'edit.php?post_status=draft&post_type=' . $item['ID'] ) . '">' . $item['drafts'] . '</a>';
        }
    }

    function column_trashed($item){
        if ( $item['trashed'] == '0' ) {
          return '—';
        } else {
          return '<a class="row-title trashed" href="' . admin_url( 'edit.php?post_status=trash&post_type=' . $item['ID'] ) . '">' . $item['trashed'] . '</a>';
        }
    }

    /** ************************************************************************
     * REQUIRED! This method dictates the table's columns and titles. This should
     * return an array where the key is the column slug (and class) and the value
     * is the column's title text. If you need a checkbox for bulk actions, refer
     * to the $columns array below.
     *
     * The 'cb' column is treated differently than the rest. If including a checkbox
     * column in your table you must create a column_cb() method. If you don't need
     * bulk actions or checkboxes, simply leave the 'cb' entry out of your array.
     *
     * @see WP_List_Table::::single_row_columns()
     * @return array An associative array containing column information: 'slugs'=>'Visible Titles'
     **************************************************************************/
    function get_columns(){
        $columns = array(
            'title'     => 'Resource',
            'published' => 'Published',
            'drafts' => 'Drafts',
            'trashed' => 'Trashed'
        );
        return $columns;
    }

    /** ************************************************************************
     * REQUIRED! This is where you prepare your data for display. This method will
     * usually be used to query the database, sort and filter the data, and generally
     * get it ready to be displayed. At a minimum, we should set $this->items and
     * $this->set_pagination_args(), although the following properties and methods
     * are frequently interacted with here...
     *
     * @global WPDB $wpdb
     * @uses $this->_column_headers
     * @uses $this->items
     * @uses $this->get_columns()
     * @uses $this->get_sortable_columns()
     * @uses $this->get_pagenum()
     * @uses $this->set_pagination_args()
     **************************************************************************/
    function prepare_items() {
        global $wpdb; //This is used only if making any database queries

        /**
         * First, lets decide how many records per page to show
         */

        /**
         * REQUIRED. Now we need to define our column headers. This includes a complete
         * array of columns to be displayed (slugs & titles), a list of columns
         * to keep hidden, and a list of columns that are sortable. Each of these
         * can be defined in another method (as we've done here) before being
         * used to build the value for our _column_headers property.
         */
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        /**
         * REQUIRED. Finally, we build an array to be used by the class for column
         * headers. The $this->_column_headers property takes an array which contains
         * 3 other arrays. One for all columns, one for hidden columns, and one
         * for sortable columns.
         */
        $this->_column_headers = array($columns, $hidden, $sortable);

        /**
         * Optional. You can handle your bulk actions however you see fit. In this
         * case, we'll handle them within our package just to keep things clean.
         */
        $this->process_bulk_action();

        /**
         * Instead of querying a database, we're going to fetch the example data
         * property we created for use in this plugin. This makes this example
         * package slightly different than one you might build on your own. In
         * this example, we'll be using array manipulation to sort and paginate
         * our data. In a real-world implementation, you will probably want to
         * use sort and pagination data to build a custom query instead, as you'll
         * be able to use your precisely-queried data immediately.
         */
        $data = $this->resource_data;

        /**
         * REQUIRED for pagination. Let's check how many items are in our data array.
         * In real-world use, this would be the total number of items in your database,
         * without filtering. We'll need this later, so you should always include it
         * in your own package classes.
         */
        $total_items = count($data);

        /**
         * REQUIRED. Now we can add our *sorted* data to the items property, where
         * it can be used by the rest of the class.
         */
        $this->items = $data;

        /**
         * REQUIRED. We also have to register our pagination options & calculations.
         */
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $total_items,                     //WE have to determine how many items to show on a page
            'total_pages' => 1   //WE have to calculate the total number of pages
        ) );
    }
}
