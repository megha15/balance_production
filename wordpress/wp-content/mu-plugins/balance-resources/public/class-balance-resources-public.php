<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://roidna.com
 * @since      1.0.0
 *
 * @package    Balance_Resources
 * @subpackage Balance_Resources/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Balance_Resources
 * @subpackage Balance_Resources/public
 * @author     ROIDNA <devs@roidna.com>
 */
class Balance_Resources_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param string  $plugin_name The name of the plugin.
	 * @param string  $version     The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Balance_Resources_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Balance_Resources_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/balance-resources-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Balance_Resources_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Balance_Resources_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/balance-resources-public.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * update the view count data in the resources view count table
	 *
	 * @since   1.0.0
	 * @access  public
	 * @param int     $post_id                The post ID
	 * @param int     $page                   The page of the resource we are viewing (if it is a multipage article which is split in multiple pages, defaults to 1 for others)
	 * @param int     $white_label_website_id The id of the white label website we are viewing (if it is a main site then it defaults to 0)
	 * @return  void
	 */
	public static function increment_resource_view_count( $post_id, $page = 1, $white_label_website_id = 0 ) {
		if (!isset($page) || empty($page)) return;
		if ( !isset( $_SESSION ) ) {
			session_start();
		}
		// check if visitor already viewed the resource in this session. If not then increment the view
		if ( empty( $_SESSION['viewed'][$post_id] ) ) {
			// increment the view count
			global $wpdb;
			$prefix = $wpdb->prefix;
			$resources_view_count_table_name  = $prefix . 'resources_view_count';
			$post_table_name = $prefix . 'posts';
			$post_table_id = $prefix . 'post_id'; // the name of the column in db table

			$exists = $wpdb->get_results(
				"
					SELECT
						ID
					FROM
						$resources_view_count_table_name
					WHERE
						$post_table_id = $post_id
						AND
						page = $page
					"
			);

			if ( $exists ) {
				$wpdb->query(
					"
						UPDATE
							$resources_view_count_table_name
							SET view_count = view_count +  1
						WHERE
							$post_table_id = $post_id
							AND
							page = $page
						"
				);
			} else {
				$wpdb->insert(
					$resources_view_count_table_name,
					array(
						'white_label_website_id' => $white_label_website_id,
						'page' => $page,
						'view_count' => 1,
						$post_table_id => $post_id
					),
					array( // format of the columns
						'%d',
						'%d',
						'%d',
						'%d',
					)
				);
			}
			// set the session var
			$_SESSION['viewed'][$post_id] = 1;
		}
	}

}
