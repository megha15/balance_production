<?php
class Balance_Access_Groups_DB {
	public static $table_name;
	public static $post_type;
	public static $post_table_id;

	public static function install() {
		global $wpdb;
		global $access_groups_db_version;

		$prefix = $wpdb->prefix;

		self::$table_name = $prefix . 'access_groups';
		self::$post_type = 'access_groups';
		self::$post_table_id = $prefix . 'post_id';

		$table_name = self::$table_name;
		$post_table_id = self::$post_table_id;

		//DB Delta does not support IF NOT EXISTS
		if ( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) != $table_name ) {
			$post_type = self::$post_type;
			$sql = <<<SQL
				CREATE TABLE  `$table_name`
			         (
			                      `access_group_id` BIGINT(20) NOT NULL auto_increment comment ' unique identifier, key for access group, auto generated',
			                      `title`           VARCHAR(255) NOT NULL comment ' name of the access group',
			                      `post_type` LONGTEXT NOT NULL comment ' all the content types that should be included in the group (post type slugs should be used, splitted with comma)',
			                      `term__in` LONGTEXT NULL comment ' all the topics that should be included in the group (term_ids should be used, splitted with comma)',
			                      `term__not_in` LONGTEXT NULL comment 'all the topics that should be excluded in the group (term_ids should be used, splitted with comma)',
			                      `post__in` LONGTEXT NULL comment ' all specific content types that should be included (post_ids should be used, splitted with comma)',
			                      `post__not_in` LONGTEXT NULL comment ' all specific content types that should be excluded (post_ids should be used, splitted with comma)',
			                      `status`     VARCHAR(255) NOT NULL comment ' status of the access group, could be “publish” and “trash”',
			                      `$post_table_id` BIGINT(20) UNSIGNED NULL UNIQUE comment 'foreign key, id of the access_group custom post type from wp_posts table ',
			                      PRIMARY KEY (`access_group_id`),
			                      FOREIGN KEY (`$post_table_id`) REFERENCES `wp_posts`(`ID`)
			       ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
SQL;
			require_once ABSPATH . 'wp-admin/includes/upgrade.php';

			dbDelta( $sql );
			add_option( 'access_groups_db_version', $access_groups_db_version );

		}

	}

	//Get multiple entries, by status
	public static function getAll( $status = "publish", $orderBy = null, $orderDir = null, $offset = null, $limit = null, $search = null ) {
		global $wpdb;
		$table_name = self::$table_name;
		$sql = "SELECT * FROM $table_name WHERE status='$status'";

		if ( !empty( $search ) ) {
			$sql .= " AND title LIKE '%" . esc_attr( $search ) . "%'";
		}

		if ( !empty( $orderBy ) ) {
			$sql.= " ORDER BY $orderBy $orderDir";
		}

		if ( !empty( $limit ) ) {
			$sql.= " LIMIT $offset, $limit";
		}

		return $wpdb->get_results( $sql, ARRAY_A );
	}

	//Count by status
	public static function count( $status = "" ) {
		global $wpdb;
		$table_name = self::$table_name;
		$sql = "SELECT COUNT(*) FROM $table_name";

		if ( !empty( $status ) ) {
			$sql.= " WHERE status='" . $status . "'";
		}

		$count = $wpdb->get_var( $sql );
		if ( !is_null( $count ) ) {
			return $count;
		}

		return false;
	}


	public static function get( $access_group_id = null, $post_id = null ) {
		global $wpdb;

		$table_name = self::$table_name;
		$post_table_id = self::$post_table_id;
		$sql = "SELECT * FROM $table_name";

		if ( !empty( $access_group_id ) ) {
			$sql.= " WHERE access_group_id=" . $access_group_id;
		}
		elseif ( !empty( $post_id ) ) {
			$sql.= " WHERE $post_table_id=" . $post_id;
		}

		// Shoudn't be necessary, as post and access group are 1-1 in an ideal scenario
		$sql.= " LIMIT 0,1";
		$results = $wpdb->get_results( $sql, ARRAY_A );
		if ( count( $results ) > 0 ) {
			return $results[0];
		}

		return false;
	}

	// Gets all posts of resource type with names LIKE argument using WP Query
	public static function getResources( $name = "" ) {
		global $resources_cpts;

		$arrResources = array();
		$name = sanitize_text_field( $name );

		$args = array(
			'post_type' => $resources_cpts,
			's' => $name,
			'posts_per_page' => '-1',
			'posts_status' => 'publish',
			'orderby' => 'post_title',
			'order' => 'ASC',
		);

		// The Query
		$query = get_posts( $args );
		foreach ( $query as $post ) {
			$arr = array(
				"id" => $post->ID,
				"name" => $post->post_title . ' <i style="color:#ccc;font-size:10px;">(' . $post->post_type . ')</i>',
				"excerpt" => get_the_excerpt()
			);
			$arrResources[] = $arr;
		}

		return $arrResources;
	}

	// Gets all content types and labels
	public static function getContentTypes( $query = "" ) {
		global $resources_cpts;

		$arrContentTypes = array();

		foreach ( $resources_cpts as $cpt ) {
			$cpt_obj = get_post_type_object( $cpt );
			$ct = wp_count_posts( $cpt );
			$arrContentTypes[] = array(
				"id" => $cpt,
				"name" => $cpt_obj->label,
				"count" => $ct->publish
			);
		}

		return $arrContentTypes;
	}

	// Gets all tags with names LIKE argument using WP Query
	public static function getTags( $name = "" ) {
		$search = sanitize_text_field( $name );
		$taxonomies = array(
			"resource_tag"
		);
		$args = array(
			'orderby' => 'name',
			'order' => 'ASC',
			'hide_empty' => true,
			'exclude' => array() ,
			'exclude_tree' => array() ,
			'include' => array() ,
			'number' => '',
			'fields' => 'all',
			'slug' => '',
			'parent' => '',
			'hierarchical' => true,
			'child_of' => 0,
			'childless' => false,
			'get' => '',
			'name__like' => $search,
			'description__like' => '',
			'pad_counts' => false,
			'offset' => '',
			'search' => '',
			'cache_domain' => 'core'
		);
		$terms = get_terms( $taxonomies, $args );
		$arrTags = array();
		foreach ( $terms as $tag ) {
			$arrTags[] = array(
				"name" => $tag->name,
				"id" => $tag->term_taxonomy_id,
				"count" => $tag->count
			);
		}

		return $arrTags;
	}

	public static function insert( $title, $wp_post_id, $post_type, $term_in, $term_not_in, $post_in, $post_not_in ) {
		global $wpdb;

		$post_table_id = self::$post_table_id;
		// Check WP Post exists
		$post_count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->posts WHERE ID=" . $wp_post_id );
		if ( intval( $post_count ) < 1 ) {
			return false;
		}

		$data = array(
			"title" => $title,
			"status" => "publish",
			$post_table_id => $wp_post_id
		);
		if ( is_string( $post_type ) ) {
			$data["post_type"] = $post_type;
		}

		if ( is_string( $term_in ) ) {
			$data["term__in"] = $term_in;
		}

		if ( is_string( $term_not_in ) ) {
			$data["term__not_in"] = $term_not_in;
		}

		if ( is_string( $post_in ) ) {
			$data["post__in"] = $post_in;
		}

		if ( is_string( $post_not_in ) ) {
			$data["post__not_in"] = $post_not_in;
		}

		$wpdb->insert( self::$table_name, $data );
		return $wpdb->insert_id;
	}

	// Shorter function for changing between publish/trash
	public static function updateStatus( $wp_post_id, $status ) {
		global $wpdb;
		$post_table_id = self::$post_table_id;
		$arrStatus = array(
			"publish",
			"trash"
		);

		if ( in_array( $status, $arrStatus ) ) {
			$data = array(
				"status" => $status
			);
			$where = array(
				$post_table_id => $wp_post_id
			);
			$data_format = array();
			$where_format = array( // where format
				'%d'
			);
			$update = $wpdb->update( self::$table_name, $data, $where, $data_format, $where_format );
			if ( $status == 'trash' ) {
				self::delete_from_white_label_website( $wp_post_id );
			}
			return $update;
		}

		return false;
	}

	public static function update( $title, $wp_post_id, $post_type = "", $term_in = "", $term_not_in = "", $post_in = "", $post_not_in = "" ) {
		global $wpdb;
		$post_table_id = self::$post_table_id;
		if ( !isset( $wpdb ) ) //For callbacks without WPDB
			{
			$wpdb = new WPDB;
		}

		$data = array(
			"title" => $title
		);
		$where = array(
			$post_table_id => $wp_post_id
		);
		$data_format = array();
		$where_format = array( // where format
			'%d'
		);
		if ( is_string( $term_in ) ) {
			$data["term__in"] = $term_in;
		}

		if ( is_string( $term_not_in ) ) {
			$data["term__not_in"] = $term_not_in;
		}

		if ( is_string( $post_in ) ) {
			$data["post__in"] = $post_in;
		}

		if ( is_string( $post_not_in ) ) {
			$data["post__not_in"] = $post_not_in;
		}

		if ( is_string( $post_type ) ) {
			$data["post_type"] = $post_type;
		}

		$data_format = array_fill( 0, count( $data ) , "%s" ); //Add formatters for specified fields
		$update = $wpdb->update( self::$table_name, $data, $where, $data_format, $where_format );
		if ( get_post_status( $wp_post_id) == 'trash' ) {
			self::delete_from_white_label_website( $wp_post_id );
		}
		return $update;
	}

	public static function delete( $post_id ) {
		global $wpdb;
		$post_table_id = self::$post_table_id;
		self::delete_from_white_label_website( $post_id );
		return $wpdb->delete( self::$table_name, array( // where
				$post_table_id => $post_id
			) , array( // where format
				'%d'
			) );

	}
	// remove from the white label websites when deleted.
	public static function delete_from_white_label_website( $access_group_id ) {
		global $wpdb;
		$post_table_id = self::$post_table_id;
		// update metadata stored values
		// get all white label websites
		$white_label_websites = get_posts( array( 'posts_per_page' => -1, 'post_type' => 'white_label_website', 'post_status' => 'any' ) );
		if ( $white_label_websites ) {
			foreach ( $white_label_websites as $white_label_website ) {
				// get the edit module value
				$custom_data = get_post_meta( $white_label_website->ID, '_page_edit_data', true );
				// update the edit module value
				if ( !empty( $custom_data['wlw_general_info_module'][0]['accesstoresources'] ) ) {
					$access_groups = explode( ",", $custom_data['wlw_general_info_module'][0]['accesstoresources'] );
					// remove deleted access group from white label website
					$access_groups_cleaned = array_diff( $access_groups, array( 0 => $access_group_id ) );
					$custom_data['wlw_general_info_module'][0]['accesstoresources'] = implode( ",", $access_groups_cleaned );
					update_post_meta( $white_label_website->ID, '_page_edit_data', $custom_data );
				}
				// update custom table stored values
				$prefix = $wpdb->prefix;
				$white_label_websites_access_groups_table_name = $prefix . 'white_label_website_access_group';
				$wpdb->delete(
					$white_label_websites_access_groups_table_name, // table,
					array( // where
						'access_group_id' => $access_group_id,
						$post_table_id => $white_label_website->ID
					),
					array(
						'%d',
						'%d'
					)
				);
			}
		}
	}

	/* Helper functions for making data user friendly
	 */

	public static function h_content_types( $content_types = array() ) {
		$arrCpt = array();
		foreach ( $content_types as $cpt ) {
			if ( !empty( $cpt ) ) {
				$cpt_obj = get_post_type_object( $cpt );
				if ( !empty( $cpt_obj ) ) {
					$arrCpt[] = $cpt_obj->label;
				}
			}
		}

		return implode( ", ", $arrCpt );
	}

	public static function h_tags( $tags = array() ) {
		$arr = array();
		foreach ( $tags as $tag ) {
			$obj = get_term( $tag, "resource_tag" );
			if ( !empty( $obj->name ) ) {
				$arr[] = $obj->name;
			}
		}

		return implode( ", ", $arr );
	}

	public static function h_resources( $resources = array() ) {
		$arr = array();
		foreach ( $resources as $res ) {
			$obj = get_post( $res );
			if ( !empty( $obj ) ) {
				$arr[] = $obj->post_title;
			}
		}

		return implode( ", ", $arr );
	}

}
