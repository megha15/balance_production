<?php

function sendEmail($recipients, $subject, $html, $from='info@balancepro.org') {
  $headers  = 'MIME-Version: 1.0' . "\r\n";
  $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
  // Create email headers
  $eol = "\r\n";
  $headers .= 'From: '.$from.$eol
             .'Reply-To: '.$from.$eol
             .'X-Mailer: PHP/' . phpversion().$eol
             ."Content-Transfer-Encoding: base64".$eol.$eol;

  $response = array();

  if ( !is_array( $recipients ) ) {
    $recipients = array( $recipients );
  }

  foreach ($recipients as $key => $to) {
    $response[] = mail($to, $subject, rtrim( chunk_split( base64_encode( $html ) ) ), $headers);
  }

  return $response;
}
