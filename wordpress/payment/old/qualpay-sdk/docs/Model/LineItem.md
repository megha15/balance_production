# LineItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**quantity** | **int** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length, up to 3 N&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;The count of items. | 
**description** | **string** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length AN&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;Description of the item. | 
**unit_cost** | **double** | &lt;strong&gt;Format: &lt;/strong&gt;Variable length, up to 8,2 AN&lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;Cost per unit, up to 2 decimal places. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


