# GetBillingCardsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**billing_cards** | [**\qpPlatform\Model\BillingCard[]**](BillingCard.md) | &lt;br&gt;&lt;strong&gt;Description: &lt;/strong&gt;An array of billing cards. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


