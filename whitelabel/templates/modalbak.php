<div class="login-modal">
        <div id="Modal1" aria-label="register dialog modal" tabindex="-1" role="dialog" class="modal fade">
            <div role="document" class="modal-dialog">
                <div class="modal-content">
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">X</span></button>    <!-- &#215; -->
                    <div class="login-form">
                        <fieldset>
                            <div class="tab-holder">
                                <div class="pop-up-tab visible-xs">
                                    <ul role="tablist" class="nav nav-tabs">
                                        <li role="presentation" class="active"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">Register</a></li>
                                        <li><a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab">Log In</a></li>
                                    </ul>
                                    <button aria-label="Close" data-dismiss="modal" type="button" class="close visible-xs"><span aria-hidden="true">x</span></button>
                                </div>
                                <div class="tab-content col-holder">
                                    <div id="tab4" role="tabpanel" class="tab-panel active">
                                        <div class="form-content">
                                            <h1 class="text-info">Sign up for your BALANCE account.</h1>
                                            <p>Please register as a new user to access our online education programs.</p>
                                            <div class="form-fields hidden-xs">
                                                <form class="login-form" enctype= "multipart/form-data" autocomplete="off" id='frm_register'>
                                                    <fieldset>
                                                        <input type="hidden" name="registerfrm" value="1">
                                                        <input type="hidden" name="action" value="do_register">
                                                        <div class="form-block">
                                                            <div class="input-group">
                                                                <span class="label"><label for="firstname">First Name<span class="formRequired">*</span></label></span>
                                                                <input type="text" id="firstname" name="firstname" placeholder="" value="" required>
																<p id='firstnamemsg'></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <span class="label"><label for="lastname">Last Name<span class="formRequired">*</span></label></span>
                                                                <input type="text" id="lastname" name="lastname" placeholder="" value="" required>
																<p id='lastnamemsg'></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <span class="label"><label for="email">Email<span class="formRequired">*</span></label></span>
                                                                <input type="Email" id="email" name="email" placeholder="Your email will be your username." value="" required><p id='emailmsg'></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <span class="label"><label for="streetAddress">Street Address</label></span>
                                                                <input type="text" id="streetAddress" name="streetAddress" placeholder="" value="">
																<p id='streetAddressmsg'></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <span class="label"><label for="city">City</label></span>
                                                                <input type="text" id="city" name="city" placeholder="" value="">
																<p id='citymsg'></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <span class="label"><label for="state">State</label></span>
                                                                <input type="text" id="state" name="state" placeholder="" value="">
																<p id='statemsg'></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <span class="label"><label for="zip">Zip</label></span>
                                                                <input type="text" id="zip" name="zip" placeholder="" value="">
																<p id='zipmsg'></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <span class="label"><label for="memberNumber" style="white-space: break-spaces;                                                                 
                                                                    text-align: initial;">Last four digits of account/member number</label></span>
                                                                <input type="text" id="memberNumber" name="memberNumber" placeholder="" value="">
																<p id='memberNumbermsg'></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <span class="label"><label for="password">Password<span class="formRequired">*</span></label></span>
                                                                <input type="password" name="password" placeholder="8 characters, no spaces" id="password" required>
																<p id='passwordmsg'></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <span class="label"><label for="password_confirmation">Confirm Password<span class="formRequired">*</span></label></span>
                                                                <input type="password" name="password_confirmation" placeholder="" id="password_confirmation" required>
																<p id='password_confirmationmsg'></p>
                                                            </div>
                                                            <div class="input-group checkbox">
                                                                <span class="label"><label style="white-space: break-spaces;                                                                 
                                                                    text-align: initial; padding-left: 0px;">Are you a member/client or employee?</label></span>
                                                                <select name="is_employee" id="is_employee">
                                                                    <option value="0">Member/client</option>
                                                                    <option value="1">Employee</option>
                                                                </select>
																<p id='is_employeemsg'></p>
                                                            </div>
                                                            <div class="input-group checkbox counselingAndPrivacy check">
                                                                <label  style="position: relative;">
                                                                    <input type="checkbox" name="counseling_and_privacy_agreement" id="input12">
                                                                    <span class="fake-input"></span>
                                                                    <span class="fake-label ">
                                                                        I agree with
                                                                        <a href="https://www.balancepro.org/counseling-agreement-and-privacy-policy/" target="_blank">
                                                                            Counseling Agreement and Privacy Policy
                                                                        </a>
                                                                        <span class="formRequired">*</span>
                                                                        
                                                                    <!-- <input type="checkbox" name="counseling_and_privacy_agreement" id="input12"> -->
                                                                    </span>
                                                                   
                                                                </label>
                                                            </div>
                                                            <div id="registerLegend">
                                                                <ul style="list-style-type: none;">
                                                                    <li><span class="formRequired">*</span>Required</li>
                                                                </ul>
                                                            </div>
                                                          <!--div class="g-recaptcha" data-sitekey="<?//=$site_key?>"></div> <p id='grecaptchamsg'></p-->
                                                            
                                                            <div class="btn-holder" id='frmsubmitbtn'>
                                                                <!--input type="submit" class="but btn btn-primary" value="Register"-->
																<button type="submit" class="btn btn btn-primary">Register</button>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <div class="form-fields visible-xs">
                                                <form class="login-form" id='frm_register_mob' method="POST" autocomplete="off" onsubmit="return validRegister()">
												<input type="hidden" name="registerfrm" value="1">
                                                <input type="hidden" name="action" value="do_register">
                                                    <fieldset>
                                                        <input type="hidden" name="_token" value=" ">
                                                        <div class="form-block">
                                                            <div class="input-group">
                                                                <input type="text" id="firstname" name="firstname" placeholder="First name" value="">
																<p id='firstnamemsg'></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <input type="text" id="lastname" name="lastname" placeholder="Last name" value="">
																<p id='lastnamemsg'></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <input type="Email" id="email" name="email" placeholder="Email" value="">
																<p id='emailmsg'></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <input type="text" id="Street Address" name="streetAddress" placeholder="Street Address" value=""><p id='streetAddressmsg'></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <input type="text" id="city" name="city" placeholder="City" value=""><p id='citymsg'></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <input type="text" id="state" name="state" placeholder="State" value=""><p id='statemsg'></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <input type="text" id="zip" name="zip" placeholder="Zip Code" value=""><p id='zipmsg'></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <input type="text" id="memberNumber" name="memberNumber" placeholder="Last 4 Member #" value=""><p id='memberNumbermsg'></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <input type="password" name="password" placeholder="Password" id="password"><p id='passwordmsg'></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <input type="password" name="password_confirmation" placeholder="Confirm password" id="Confirm password"><p id='password_confirmationmsg'></p>
                                                            </div>
                                                            <div class="input-group">
                                                                <select name="is_employee" id="is_employee">
                                                                    <option value="member">Member/client</option>
                                                                    <option value="employee">Employee</option>
                                                                </select><p id='is_employeemsg'></p>
                                                            </div>
                                                            <div class="input-group checkbox counselingAndPrivacy">
                                                                <label style="position: relative;">
                                                                    <input type="checkbox" name="counseling_and_privacy_agreement" id="counseling_and_privacy_agreement">
                                                                   
                                                                        I agree with
                                                                        <a href="" target="_blank">
                                                                            Counseling Agreement and Privacy Policy
                                                                        </a>
                                                                        <span class="formRequired">*</span>
                                                                   
                                                                </label>
                                                            </div>
															<!--div class="g-recaptcha" data-sitekey="<?//=$site_key?>"></div> 
															<p id='grecaptchamsg'></p-->
															<div class="btn-holder" id='mob_frmsubmitbtn'>
                                                                <!--input type="submit" class="but btn btn-primary" value="Register"-->
																<button type="submit" class="btn btn btn-primary">Register</button>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab5" role="tabpanel" class="tab-panel">
                                        <div class="form-content">
                                            <h1 class="text-info">Already have a BALANCE account? Log in here.</h1>
                                            <div class="form-fields hidden-xs">
                                                <form class="login-form"  method="POST" autocomplete='off' id='frm_login'>
                                                    <fieldset>
                                                        <input type="hidden" name="_token" value=" ">
														<input type="hidden" name="action" value="do_login">
                                                        <div class="input-group">
                                                            <span class="label"><label for="email">Email</label></span>
                                                            <input id="user" type="email" placeholder="" name="user"><p id='usermsg' class='pmsg'></p>
                                                        </div>
                                                        <div class="input-group">
                                                            <span class="label"><label for="password">Password</label></span>
                                                            <input id="pass" type="password" placeholder="" name="pass"><p id='passmsg' ></p>
                                                        </div>
														<!--div class="g-recaptcha" data-sitekey="<?//=$site_key?>"></div> 
															<p id='logingrecaptchamsg'></p-->
                                                        <div class="forget-wrap"><a data-toggle="modal" data-target="#Modal2" href="#Modal2" class="forgot-pass hidden-xs">Forgot password?</a></div>
                                                        <div class="btn-holder" id='frmsubmitbtnlogin'>
                                                            <!--input type="submit" class="but btn btn-primary" value="Log In"-->
															<button type="submit" class="btn btn btn-primary">Log In</button>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                            <div class="form-fields visible-xs">
                                                <form class="login-form" action=" " method="POST" autocomplete='off' onsubmit="return validlogin()">
                                                    <fieldset>
                                                        <input type="hidden" name=" " value=" ">
                                                        <div class="input-group">
                                                            <input type="email" placeholder="Email" name="user" id='user'>
															<p id='usermsg'></p>
                                                        </div>
                                                        <div class="input-group">
                                                            <input type="pass" placeholder="Password" name="pass">
															<p id='passmsg' ></p>
                                                        </div>
														<!--div class="g-recaptcha" data-sitekey="<?//=$site_key?>"></div> 
															<p id='logingrecaptchamsg'></p-->
                                                        <div class="btn-block">
                                                            <div class="btn-holder text-center">
                                                                <a data-toggle="modal" data-target="#Modal2" href="#Modal2" class="forgot-pass visible-xs">Forgot password?</a>
                                                                <input type="submit" value="Log In" class="but btn btn-primary">
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
        <div id="Modal2" tabindex="-1" role="dialog" aria-label="Forgotten Password dialog" class="modal fade inner">
            <div role="document" class="modal-dialog">
                <div class="modal-content">
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">x</span></button>
                    <div class="pop-up-tab visible-xs">
                        <button aria-label="Close" data-dismiss="modal" type="button" class="close visible-xs"><span aria-hidden="true">&#215;</span></button>
                    </div>
                    <div class="form-content">
                        <h1 class="text-info">Forgotten Password</h1>
                        <p style="text-align:left;">Reset your password using your registered email with BALANCE. To provide a secure process, we will send this information to you via e-mail.</p>
                        <div class="form-fields hidden-xs">
                            <form class="login-form" action=" " method="POST">
                                <input type="hidden" name="_token" value=" ">
                                <fieldset>
                                    <div class="input-group">
                                        <span class="label"><label for="email">Email</label></span>
                                        <input id="email" type="email" name="email" placeholder="Your email will be your username">
                                    </div>
                                    <p style="text-align:left;" class="note">Click on the Reset Password link to reset your password with a new one. If the e-mail address you provided is not in the system you will not receive an e-mail.</p>
                                    <div class="note-wrap text-center">
                                        <a href="#Modal1" data-target="#Modal1" data-toggle="modal" data-rel-tab="#tab5" class="text-login text-info"><strong>Log In</strong></a>
                                    </div>
                                    <br>
                                    <div class="btn-holder">
                                        <input type="submit" class="but btn btn-primary reset-btn" value="Reset Password">
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <div class="form-fields visible-xs">
                            <form class="login-form" action=" " method="POST">
                                <input type="hidden" name="_token" value=" ">
                                <fieldset>
                                    <div class="input-group">
                                        <input type="email" name="email" placeholder="Email">
                                    </div>
                                    <p style="text-align:left;" class="note">Click on the Reset Password link to reset your password with a new one. If the e-mail address you provided is not in the system you will not receive an e-mail.</p>
                                    <div class="note-wrap text-center">
                                        <a href="#Modal1" data-target="#Modal1" data-toggle="modal" data-rel-tab="#tab5" class="text-login text-info"><strong>Log In</strong></a>
                                    </div>
                                    <br>
                                    <div class="btn-holder">
                                        <input type="submit" class="but btn btn-primary reset-btn" value="Reset Password">
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		<!-- modal messages --->
		<div id="Modalsuccess" tabindex="-1" role="dialog" aria-label="Forgotten Password dialog" class="modal fade inner">
			<div role="document" class="modal-dialog">
				<div class="modal-content">
					<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">x</span></button>
					<div class="pop-up-tab visible-xs">
						<button aria-label="Close" data-dismiss="modal" type="button" class="close visible-xs"><span aria-hidden="true">&#215;</span></button>
					</div>
					<div class="form-content" id='log_reg_msg_id'>						
						<p style="text-align:left;" >Registration successfully done</p>						
					</div>
				</div>
			</div>
		</div>


		<div id="Modalerror" tabindex="-1" role="dialog" aria-label="Forgotten Password dialog" class="modal fade inner">
			<div role="document" class="modal-dialog">
				<div class="modal-content">
					<button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">x</span></button>
					<div class="pop-up-tab visible-xs">
						<button aria-label="Close" data-dismiss="modal" type="button" class="close visible-xs"><span aria-hidden="true">&#215;</span></button>
					</div>
					<div class="form-content">
						
						<p style="text-align:left;" id='err_msg'>Somethis went wrong. Please try again!</p>
						
					</div>
				</div>
			</div>
		</div>


		<!-- end modal messages --->
    </div>
<script>	
	
</script>