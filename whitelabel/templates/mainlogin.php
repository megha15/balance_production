<?php //echo'<pre>',print_r($_SERVER),'</pre>'; ?>  

  <!-- contain main informative part of the site -->
      <main id="main" role="main">
	  <?php 
	  if(isset($_SESSION['login_error_new']) && $_SESSION['login_error_new']!=''){?>
	  <div class="alert alert-danger">
            <?=$_SESSION['login_error_new']?>
        </div>
	  
	  <?php
	  } else if(isset($_SESSION['login_error']) && $_SESSION['login_error']!=''){?>
        <div class="alert alert-danger">
            <?=$_SESSION['login_error']?>
        </div>
	  <?php  } 
	  unset($_SESSION['login_error']);
	  unset($_SESSION['login_error_new']);
	  ?>
	  <?php if(isset($_SESSION['loginmessage']) && $_SESSION['loginmessage']!=''){?>
        <div class="alert alert-danger">
            <?=$_SESSION['loginmessage']?>
        </div>
	  <?php 
	  unset($_SESSION['loginmessage']);
	  } ?>
        <!-- bottom banner section -->
        <div class="banner-wrap">
            <div class="hero-block">
        <div class="image-sm" style="background: url('<?=$base_url?>assets/img/img21-small.jpg') no-repeat 50% 50%/cover"></div>
        <div class="image" style="background: url('https://s3-us-west-2.amazonaws.com/balancepro/wp-content/uploads/2018/08/08145456/HSB-Balance-Hero.jpg') no-repeat 50% 50%/cover"></div>
        <div class="hero">
            <div class="container">
                <div class="row">
                    <div class="col-sm-7 col-md-8">
                        <div class="description">
                            <h1></h1>
    
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>        <!-- login form section -->
            <div class="login-form-section"  >
        <div class="container">
            <div class="row">
                <div class="col-sm-5 col-sm-offset-7 col-md-4 col-md-offset-8">
                    <div id="accordion-form" role="tablist" aria-multiselectable="true" class="login-form-wrap">
                    <div class="panel">
					<?php if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']!='') {
						$htreffer = $_SERVER['HTTP_REFERER'];
					}
					else {
						$htreffer =$base_url;
					}
					?>
                        <form  aria-expanded="true" class="login-form2 collapse in" method="POST" onsubmit="return validlogin();" id='login-form2' autocomplete='off'>
                            <input type="hidden" name="action" value="do_login">
                            <input type="hidden" name="HTTP_REFERER" value="<?=$htreffer?>">
                            <!-- login section -->
                            <div class="input-section">
                                <div class="title">
                                    <strong class="h1">Log In</strong>
    
                                    <!--Errors-->
                                    <div class="errors">  </div>
                                    <!--End Errors-->
    
    
                                    <p>Sign in to your BALANCE account.
									
									</p>
                                </div>
								<?php $email = isset($_SESSION['newmailid'])?$_SESSION['newmailid']:''?>
                                <div class="input-block-group">
                                    <div class="input-wrap">
                                        <label for="email" class="visible-xs-inline-block">Email</label>
                                        <input type="email" id="email" placeholder="Email" name="user" value="<?=$email?>" onfocusout="validEmail('email','emailmsgmain')">
										<span id='emailmsgmain'></span>
                                    </div>
                                    <div class="input-wrap">
                                        <label for="password" class="visible-xs-inline-block">Password</label>
                                        <input type="password" id="password" placeholder="Password" name="pass" value=""><span id='passmsg'></span>
    
                                        <div class="note visible-xs"><p>8 charactors, no spaces</p></div>
                                    </div>
									<div class="input-wrap">
									<div class="g-recaptcha" data-sitekey="<?=$site_key?>"></div> 
									<p id='grecaptchamsglogin'></p>
									</div>
                                </div>
    
                                <div class="button">
                                    <input class="btn but btn-warning" type="submit" id="log_submit" name="log_submit" value="Log In">
                                </div>
                                <div class="links text-capitalize">
                                    <a role="button" data-toggle="collapse" href="#forgot-form2" aria-expanded="false" aria-controls="forgot-form2" data-parent="#accordion-form">Forgot/Reset password?</a>
                                </div>
                                <div class="links text-capitalize">Don't have an account? <a data-toggle="modal" data-target="#myModal" href="#myModal" class="dropdown-toggle"><br>Create one now.</a></div>
                            </div>
                        </form>
                    </div>
                    <div class="panel">
                        <form id="forgot-form2" action=" " class="forgot-form2 collapse" method="POST" onsubmit="return validEmailForgot()" autocomplete='off'>
                            <input type="hidden" name="action" value="sendmail_forgot">
                            <input type="hidden" name="actiontype" value="mailpost">
                            <input type="hidden" id="chkmailforfot" value="0">
    
                            <div class="input-section">
                                <div class="title"><strong class="h1">Forgotten Password</strong>
    
                                    <p>Reset your password using your registered email with BALANCE. To provide a secure process, we will send this information to you via Email.</p>
                                </div>
                                <div class="input-block-group">
                                    <div class="input-wrap">
                                        <label for="email2" class="visible-xs-inline-block">Email</label>
                                        <input id="emailf" type="email" placeholder="Email" name="email" onfocusout="checkMailId('emailf','emailmsgf')">
										<span id='emailmsgf'></span>	
                                        <div class="visible-xs mobile-block">
                                            <p>Click on the Reset Password link to reset your password with a new one. If the Email address you provided is not in the system you will not receive an Email.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-text hidden-xs">
                                    <p>Click on the Reset Password link to reset your password with a new one. If the Email address you provided is not in the system you will not receive an Email.</p>
                                </div>
                                <div class="button">
                                    <input type="submit" class="btn btn-warning reset-btn" value="Reset Password">
                                </div>
                                <div class="links text-capitalize"><a role="button" data-toggle="collapse" href="#login-form2" aria-expanded="false" aria-controls="login-form2" data-parent="#accordion-form">Login</a></div>
                            </div>
                        </form>
                    </div>
                    
                        </div>
                </div>
            </div>
        </div>
    </div>    </div>
    </main>