<div class="jumbotron text-center" style='margin-top:100px'>
  <h1 class="display-3">Something is wrong</h1>
  <p class="lead"><strong>Please check try again</p>
  <hr>
  <p>
    Having trouble? <a href="<?=$base_url?>contact">Contact us</a>
  </p>
  <p class="lead">
    <a class="btn btn-primary btn-sm" href="<?=$base_url?>" role="button">Continue to homepage</a>
  </p>
</div>