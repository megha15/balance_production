   <main id="main">
        <div class="breadcrumb-wrapper">
            <div class="container">
                <div class="row">
                    <ol class="breadcrumb">
                        <!-- Link to home -->
                        <li><a href="http://www.devxekera.com">Home</a></li>
                        <!-- Link to resource page -->
                        <li><a aria-label="breadcrumb" href="/resources">Resources</a></li>
                        <li style="color: #005d5b !important;">Articles</li>
                        <li>Growing Families</li>
                    </ol>
                </div>
            </div>
        </div>
        <div style="background-image: url(http://www.devxekera.com/wp-content/themes/balance-theme/images/img03.jpg)" class="chapter-block">
            <div class="container">
                <div class="row">
                    <form action="#" class="chapter-form col-sm-12">
                        <div class="input-group"><span class="label">
                                <label for="chapter-select">Jump to:</label></span>
                            <div class="select-holder">
                                <select id="chapter-select" class="jump anchor-select jcf-hidden">
                                    <option value="/resources/articles/growing-families/page/1">Introduction</option>
                                    <option value="/resources/articles/growing-families/page/2">Video</option>
                                    <option value="/resources/articles/growing-families/page/3">Planning Your Family's Financial Future- Five Mistakes To Avoid</option>
                                    <option value="/resources/articles/growing-families/page/4">Podcast – Growing Families and Money</option>
                                    <option value="/resources/articles/growing-families/page/5" selected="selected">Quiz</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="intro" class="intro-block background-white">
            <!-- M34A: SECTION -->
            <article aria-label="article for Growing Families" class="text-block default-content-style article default-content-style quiz-header">
                <div class="container">
                    <div class="row">
                        <div class="">
                            <h1 class="text-info text-center" style="text-align:center;padding-top: 10px;">Growing Families</h1>
                            <p style="text-align:center;"><strong >Do you know the best ways to financially support your growing family? Take this quiz and find out! </strong><span style="-webkit-font-smoothing: antialiased; text-rendering: optimizelegibility; font-family: sans-serif; user-select: text; color: transparent; position: absolute; white-space: pre; cursor: text; transform-origin: 0px 0px; pointer-events: auto; left: 120.042px; top: 254.217px; font-size: 20px; padding: 0px; transform: scaleX(1.0264);">out!</span></p>
                        </div>
                    </div>
                </div>
            </article><!-- end M34A: SECTION -->
            <article name="balance_article" class="quiz-results text-block article background-white default-content-style" style="display: none;" aria-label="article module print score">
                <div class="container">
                    <div class="row">
                        <h1 class="text-info text-center">Your Results</h1>
                        <p>Your score was <b class="quiz-percent"> 0%</b> (<span class="quiz-number-of-correct">0</span>/<span class="quiz-number-of-questions">0</span>).</p>
                        <p>You did not receive a passing score. Please re-take the quiz by clicking on the "Retry Quiz" button below.</p>
                        <div class="btn-holder clearfix"><a href="javascript:window.location=window.location" style="border:1px solid #006164;" class="but btn btn-primary pull-left quiz-retry">Retry quiz</a></div>
                    </div>
                </div>
            </article>
            <section aria-label="quiz questions" class="quiz-questions modules-form-section">
                <div class="container">
                    <div class="row">
                        <form id="quiz-form" method="post" class="form-wrap">
                            <div class="quiz-questions">
                                <ol class="questions">
                                    <li><strong class="h3">Which one of these things should you NOT do:</strong>
                                        <ul class="choose-list">
                                            <li> <label for="answer-0"> <input id="answer-0" type="radio" name="answer-0" value="0"> <span class="fake-input"></span><span class="fake-label "> Save 20% of your household income <span class=""></span></span> </label></li>
                                            <li> <label for="answer-1"> <input id="answer-1" type="radio" name="answer-0" value="1"> <span class="fake-input"></span><span class="fake-label ">Make a baby budget when you’re expecting <span class=""></span></span> </label></li>
                                            <li> <label for="answer-2"> <input id="answer-2" type="radio" name="answer-0" value="2"> <span class="fake-input"></span><span class="fake-label "> Buy the most expensive baby clothes for your newborn <span class=""></span></span> </label></li>
                                            <li> <label for="answer-3"> <input id="answer-3" type="radio" name="answer-0" value="3"> <span class="fake-input"></span><span class="fake-label ">You shouldn’t do any of the above <span class=""></span></span> </label></li>
                                        </ul>
                                    </li>
                                    <li><strong class="h3">With a little planning, you can predict every cost that comes with raising children.</strong>
                                        <ul class="choose-list">
                                            <li> <label for="answer-4"> <input id="answer-4" type="radio" name="answer-1" value="0"> <span class="fake-input"></span><span class="fake-label ">True <span class=""></span></span> </label></li>
                                            <li> <label for="answer-5"> <input id="answer-5" type="radio" name="answer-1" value="1"> <span class="fake-input"></span><span class="fake-label ">False <span class=""></span></span> </label></li>
                                        </ul>
                                    </li>
                                    <li><strong class="h3">Setting financial goals for your family is important because</strong>
                                        <ul class="choose-list">
                                            <li> <label for="answer-6"> <input id="answer-6" type="radio" name="answer-2" value="0"> <span class="fake-input"></span><span class="fake-label ">You’ll know how much money to save <span class=""></span></span> </label></li>
                                            <li> <label for="answer-7"> <input id="answer-7" type="radio" name="answer-2" value="1"> <span class="fake-input"></span><span class="fake-label ">You’ll know how long it will take to achieve them <span class=""></span></span> </label></li>
                                            <li> <label for="answer-8"> <input id="answer-8" type="radio" name="answer-2" value="2"> <span class="fake-input"></span><span class="fake-label ">You’ll plan for long-term expenses <span class=""></span></span> </label></li>
                                            <li> <label for="answer-9"> <input id="answer-9" type="radio" name="answer-2" value="3"> <span class="fake-input"></span><span class="fake-label ">All of the above <span class=""></span></span> </label></li>
                                        </ul>
                                    </li>
                                    <li><strong class="h3">Married couples and new parents may be eligible for tax breaks.</strong>
                                        <ul class="choose-list">
                                            <li> <label for="answer-10"> <input id="answer-10" type="radio" name="answer-3" value="0"> <span class="fake-input"></span><span class="fake-label ">True <span class=""></span></span> </label></li>
                                            <li> <label for="answer-11"> <input id="answer-11" type="radio" name="answer-3" value="1"> <span class="fake-input"></span><span class="fake-label ">False <span class=""></span></span> </label></li>
                                        </ul>
                                    </li>
                                    <li><strong class="h3">You should ask about your employer’s family leave policy…</strong>
                                        <ul class="choose-list">
                                            <li> <label for="answer-12"> <input id="answer-12" type="radio" name="answer-4" value="0"> <span class="fake-input"></span><span class="fake-label "> As soon as the baby is born <span class=""></span></span> </label></li>
                                            <li> <label for="answer-13"> <input id="answer-13" type="radio" name="answer-4" value="1"> <span class="fake-input"></span><span class="fake-label ">Shortly after you become pregnant <span class=""></span></span> </label></li>
                                            <li> <label for="answer-14"> <input id="answer-14" type="radio" name="answer-4" value="2"> <span class="fake-input"></span><span class="fake-label ">The day before you’re supposed to return to work <span class=""></span></span> </label></li>
                                            <li> <label for="answer-15"> <input id="answer-15" type="radio" name="answer-4" value="3"> <span class="fake-input"></span><span class="fake-label ">None of the above <span class=""></span></span> </label></li>
                                        </ul>
                                    </li>
                                    <li><strong class="h3">Important baby expenses include a stroller, car seat, and daycare.</strong>
                                        <ul class="choose-list">
                                            <li> <label for="answer-16"> <input id="answer-16" type="radio" name="answer-5" value="0"> <span class="fake-input"></span><span class="fake-label ">True <span class=""></span></span> </label></li>
                                            <li> <label for="answer-17"> <input id="answer-17" type="radio" name="answer-5" value="1"> <span class="fake-input"></span><span class="fake-label ">False <span class=""></span></span> </label></li>
                                        </ul>
                                    </li>
                                    <li><strong class="h3">A will provides certain benefits if you pass away, such as</strong>
                                        <ul class="choose-list">
                                            <li> <label for="answer-18"> <input id="answer-18" type="radio" name="answer-6" value="0"> <span class="fake-input"></span><span class="fake-label ">It cancels your debts <span class=""></span></span> </label></li>
                                            <li> <label for="answer-19"> <input id="answer-19" type="radio" name="answer-6" value="1"> <span class="fake-input"></span><span class="fake-label ">It ensures that your assets go to the right people <span class=""></span></span> </label></li>
                                            <li> <label for="answer-20"> <input id="answer-20" type="radio" name="answer-6" value="2"> <span class="fake-input"></span><span class="fake-label ">It converts your money into lottery tickets <span class=""></span></span> </label></li>
                                            <li> <label for="answer-21"> <input id="answer-21" type="radio" name="answer-6" value="3"> <span class="fake-input"></span><span class="fake-label ">It delays your mortgage payment <span class=""></span></span> </label></li>
                                        </ul>
                                    </li>
                                    <li><strong class="h3">Life insurance financially protects your family from any day-to-day accidents.</strong>
                                        <ul class="choose-list">
                                            <li> <label for="answer-22"> <input id="answer-22" type="radio" name="answer-7" value="0"> <span class="fake-input"></span><span class="fake-label ">True <span class=""></span></span> </label></li>
                                            <li> <label for="answer-23"> <input id="answer-23" type="radio" name="answer-7" value="1"> <span class="fake-input"></span><span class="fake-label ">False <span class=""></span></span> </label></li>
                                        </ul>
                                    </li>
                                    <li><strong class="h3">One example of a savings tool designed specifically for college expenses is</strong>
                                        <ul class="choose-list">
                                            <li> <label for="answer-24"> <input id="answer-24" type="radio" name="answer-8" value="0"> <span class="fake-input"></span><span class="fake-label ">Certificate of Deposit (CD) <span class=""></span></span> </label></li>
                                            <li> <label for="answer-25"> <input id="answer-25" type="radio" name="answer-8" value="1"> <span class="fake-input"></span><span class="fake-label ">A money market account <span class=""></span></span> </label></li>
                                            <li> <label for="answer-26"> <input id="answer-26" type="radio" name="answer-8" value="2"> <span class="fake-input"></span><span class="fake-label ">A savings bond <span class=""></span></span> </label></li>
                                            <li> <label for="answer-27"> <input id="answer-27" type="radio" name="answer-8" value="3"> <span class="fake-input"></span><span class="fake-label ">A 529 plan <span class=""></span></span> </label></li>
                                        </ul>
                                    </li>
                                    <li><strong class="h3">One important step to making a budget is tracking your expenses.</strong>
                                        <ul class="choose-list">
                                            <li> <label for="answer-28"> <input id="answer-28" type="radio" name="answer-9" value="0"> <span class="fake-input"></span><span class="fake-label ">True <span class=""></span></span> </label></li>
                                            <li> <label for="answer-29"> <input id="answer-29" type="radio" name="answer-9" value="1"> <span class="fake-input"></span><span class="fake-label ">False <span class=""></span></span> </label></li>
                                        </ul>
                                    </li>
                                </ol><input type="hidden" name="quiz-submission" value="quiz-submission"> <input type="hidden" name="wlw" value="0"> <input type="submit" value="Submit" class="but btn btn-primary submit-quiz" style="border:1px solid #006164; margin-bottom:10px;">
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>'<div class="button-spacing-top"></div>';
        <nav aria-label="paging holder m11" class="paging-holder">
            <div class="container">
                <div class="row">
                    <ul class="pagination">
                        <li>
                            <a href="/resources/articles/growing-families/page/4" aria-label="Previous">
                                <span class="btn-prev"></span>
                                <span class="hidden-xs">Previous</span>
                            </a>
                        </li>
                        <li><a href="/resources/articles/growing-families/page/1">1</a></li>
                        <li><a href="/resources/articles/growing-families/page/2">2</a></li>
                        <li><a href="/resources/articles/growing-families/page/3">3</a></li>
                        <li><a href="/resources/articles/growing-families/page/4">4</a></li>
                        <li class="active"><a href="/resources/articles/growing-families/page/5">5</a></li>
                        <li>
                            <a href="/resources/articles/growing-families/page/5" aria-label="Next">
                                <span class="hidden-xs">Next</span>
                                <span class="btn-next"></span>
                            </a>
                        </li>
                    </ul>
                    <p>of 5 pages</p>
                </div>
            </div>
        </nav>
    </main>
   