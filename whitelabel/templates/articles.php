<main id="main" role="main">
    <style>
    .report-block img {
        height: 315px;
        width: auto;
    }

    .report-block .image-holder,
    .report-block .copy-holder {
        width: 50%;
    }

    .report-block .copy-holder {
        position: relative;
        padding: 0 38px;
        text-align: left;
    }

    @media  screen and (max-width: 1024px) {
        .report-block .image-holder {
            width: 100%;
        }

        .report-block .image-holder img {
            width: 100%;
            height: auto !important;
        }

        .report-block .copy-holder {
            width: 100%;
            padding-top: 25px;
            padding-bottom: 25px;
            top: 0;

            -webkit-transform: translateY(0%);
            -ms-transform: translateY(0%);
            transform: translateY(0%);
        }
    }
</style>

   
	<?php
	$relplace = "'<div class=></div>';";
	
	 $datan = str_replace('"button-spacing-top"','',$data['resource']['html']);
	 $datan = str_replace($relplace,'',$datan);
	
	//echo getValidUrlsFrompage($datan);
	echo modifyUrl($datan);
	
	$relatedResources = $data['relatedResources'];
		//	print_r($relatedResources)	
		if(count($relatedResources)>0){
	?>
	<section class="inner-block first-inner-child">
    <div class="container">
        <div class="row">
            <h1 class="text-info text-center">You Might Also Like</h1>
            <div class="info-holder col-holder same-height-holder">
           
			<?php 
			for($i=0;$i<count($relatedResources);$i++){
				$post_type = $relatedResources[$i]['post_type'];	
				if($post_type=='newsletter') $post_type='newsletters';
				if($post_type=='article') $post_type='articles';
				if($post_type=='podcast') $post_type='podcasts';
				if($post_type=='booklet') $post_type='booklets';
				if($post_type=='toolkit') $post_type='toolkits';
				if($post_type=='video') $post_type='videos';
				if($post_type=='calculator') $post_type='calculators';
			?>
              <div class="col-xs-6 col-sm-3">
                <article class="info-wrap">
                  <!--div class="img-holder"><span class="{{$icons[$resource->post_type]}}"></span></div-->
                  <div class="img-holder"><span class="icon-<?php echo $relatedResources[$i]['post_type'] ?>"></span></div>
                  <p style="height: 25px;" class="same-height">
                    <?php echo $relatedResources[$i]['post_title'] ?>
                  </p>
                  <span><!--a class="text-read-more text-warning" href="<?=$base_url?>index.php?action=resources1&type=<?=$post_type?>&id=<?=$relatedResources[$i]['post_name']?>">READ MORE</a-->
				  <a class="text-read-more text-warning" href="<?=$base_url?>resources/<?=$post_type?>/<?=$relatedResources[$i]['post_name']?>">READ MORE</a>
				  </span>
                </article>
              </div>
            <?php } ?>
            </div>
        </div>
    </div>
</section>
		<?php } ?>
	
</main>