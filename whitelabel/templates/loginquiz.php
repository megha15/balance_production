<main id="main">
<?php
/*
$getQuestionDataQuery = mysqli_query($conn,'SELECT * FROM `wp_posts` WHERE `post_type`="quiz" AND `post_status`="publish" AND `ID`="5746"');
echo $getQuestionDataQuery;

//print_r($getQuestionDataQuery);
*/

?>

    
    <!-- login quiz code start -->

        <div class="breadcrumb-wrapper">
            <div class="container">
                <div class="row">
                    <ol class="breadcrumb">
                        <!-- Link to home -->
                        <li><a href="https://www.balancepro.org">Home</a></li>
                        <!-- Link to resource page -->
                        <li>Quiz</li>
                        <li>BalanceTrack: High-Cost Financial Services</li>
                        <li><?php // print_r($checkcount);?></li>
                    </ol>
                </div>
            </div>
        </div>
		<?php
      if(isset($_SESSION['message']) && $_SESSION['message']!=''){
?>
	<div class="alert success wcolor">
  <span class="closebtn">&times;</span>  
  <?php echo $_SESSION['message']?>
	</div>
<?php
}	
//print_r($_SESSION);
unset($_SESSION['message']);

?>
        <article aria-label="article for Quiz" class="text-block default-content-style article default-content-style quiz-header">
            <div class="container">
                <div class="row">
                    <div class="">
                        <h1 class="text-info text-center">Quiz</h1>
                        <p><span style="line-height: 30px;">Take the quiz to check your understanding of this&nbsp;module. Submit your answers to view your results.&nbsp;Your results will be shared with your&nbsp;referring organization.</span></p>
                    </div>
                </div>
            </div>
        </article>
        <article name="balance_article" class="quiz-results text-block article background-white default-content-style" style="display: none;" aria-label="article module print score">
            <div class="container">
                <div class="row">
                    <h1 class="text-info text-center">Your Results</h1>
                    <p>Your score was <b class="quiz-percent"> 0%</b> (<span class="quiz-number-of-correct">0</span>/<span class="quiz-number-of-questions">0</span>).</p>
                    <p><span style="line-height: 25.7143px;">You need to score at least 80% on the quiz to successfully complete this module and receive a Certificate of Completion. Please review the module and try the quiz again.</span></p>
                    <div class="btn-holder clearfix"><a href="javascript:window.location=window.location" class="btn btn-warning pull-left quiz-retry">Retry quiz</a></div>
                </div>
            </div>
        </article>
        <section aria-label="quiz questions" class="quiz-questions modules-form-section">
            <div class="container">
                <div class="row">
                    <form id="quiz-form" method="post" class="form-wrap">
					<input type='hidden' name='action' value='submit_query'>
                        <div class="quiz-questions">
                            <ol class="questions">
                                <li><strong class="h3">Subprime credit is:</strong>
                                    <ul class="choose-list">
                                        <li> <label for="answer-0"> 
                                          <input id="answer-0" type="radio" name="answer-0" value="0"> <span class="fake-input"></span><span class="fake-label ">Illegal and predatory <span class=""></span></span></label>
                                        </li>                  

                                        <li> <label for="answer-1"> 
                                          <input id="answer-1" type="radio" name="answer-0" value="1"> <span class="fake-input"></span><span class="fake-label ">More expensive than prime credit <span class=""></span></span> </label>
                                        </li>

                                        <li> <label for="answer-2"> 
                                          <input id="answer-2" type="radio" name="answer-0" value="2"> <span class="fake-input"></span><span class="fake-label ">Never recommended <span class=""></span></span> </label>
                                        </li>
                                    </ul>
                                </li>

                                <li><strong class="h3">The Truth in Lending Act:</strong>
                                    <ul class="choose-list">
                                        <li> <label for="answer-3">
                                         <input id="answer-3" type="radio" name="answer-1" value="0"> <span class="fake-input"></span><span class="fake-label ">Ensures that lenders disclose their terms in the application or contract <span class=""></span></span> </label>
                                       </li>

                                        <li> <label for="answer-4">
                                         <input id="answer-4" type="radio" name="answer-1" value="1"> <span class="fake-input"></span><span class="fake-label ">Protects borrowers against unfair billing errors <span class=""></span></span> </label>
                                       </li>

                                        <li> <label for="answer-5"> 
                                          <input id="answer-5" type="radio" name="answer-1" value="2"> <span class="fake-input"></span><span class="fake-label ">Protects borrowers against illegal collection practices <span class=""></span></span> </label>
                                        </li>
                                    </ul>
                                </li>

                                <li><strong class="h3">Payday loans are not recommended because:</strong>
                                    <ul class="choose-list">
                                        <li>  <label for="answer-6">
                                          <input id="answer-6" type="radio" name="answer-2" value="0"> <span class="fake-input"></span><span class="fake-label "> They are secured by a home <span class=""></span></span> </label>
                                        </li>

                                        <li> <label for="answer-7"> 
                                          <input id="answer-7" type="radio" name="answer-2" value="1"> <span class="fake-input"></span><span class="fake-label ">You must repay the loan within 14 days or the lender will take immediate legal action <span class=""></span></span> </label>
                                        </li>

                                        <li> <label for="answer-8"> 
                                          <input id="answer-8" type="radio" name="answer-2" value="2"> <span class="fake-input"></span><span class="fake-label ">Their interest rates are among the highest of all loan products <span class=""></span></span> </label>
                                        </li>
                                    </ul>
                                </li>

                                <li><strong class="h3">Car title loans:</strong>
                                    <ul class="choose-list">
                                        <li> <label for="answer-9">
                                         <input id="answer-9" type="radio" name="answer-3" value="0"> <span class="fake-input"></span><span class="fake-label "> Are secured loan products <span class=""></span></span> </label>
                                       </li>

                                        <li> <label for="answer-10"> 
                                          <input id="answer-10" type="radio" name="answer-3" value="1"> <span class="fake-input"></span><span class="fake-label ">Typically have 25 percent APRs <span class=""></span></span> </label>
                                        </li>

                                        <li> <label for="answer-11"> 
                                          <input id="answer-11" type="radio" name="answer-3" value="2"> <span class="fake-input"></span><span class="fake-label ">Are only available to borrowers who have a good credit history <span class=""></span></span> </label>
                                        </li>
                                    </ul>
                                </li>

                                <li><strong class="h3">Check-cashing businesses:</strong>
                                    <ul class="choose-list">
                                        <li> <label for="answer-12"> 
                                          <input id="answer-12" type="radio" name="answer-4" value="0"> <span class="fake-input"></span><span class="fake-label "> Offer free financial services to the community <span class=""></span></span> </label>
                                        </li>
                                        <li> <label for="answer-13">
                                         <input id="answer-13" type="radio" name="answer-4" value="1"> <span class="fake-input"></span><span class="fake-label ">Usually charge about two percent of the amount of a check to provide you with the money <span class=""></span></span> </label>
                                       </li>
                                        <li> <label for="answer-14">
                                         <input id="answer-14" type="radio" name="answer-4" value="2"> <span class="fake-input"></span><span class="fake-label ">Are excellent alternatives to traditional financial institutions <span class=""></span></span> </label>
                                       </li>
                                    </ul>
                                </li>

                                <li><strong class="h3">A rent-to-own arrangement:</strong>
                                    <ul class="choose-list">
                                        <li> <label for="answer-15"> 
                                          <input id="answer-15" type="radio" name="answer-5" value="0"> <span class="fake-input"></span><span class="fake-label ">Provides an interest-free period of time before payments are due <span class=""></span></span> </label>
                                        </li>

                                        <li> <label for="answer-16">
                                         <input id="answer-16" type="radio" name="answer-5" value="1"> <span class="fake-input"></span><span class="fake-label ">Makes sense when you want a luxury item that you can't afford <span class=""></span></span> </label>
                                       </li>

                                        <li> <label for="answer-17"> 
                                          <input id="answer-17" type="radio" name="answer-5" value="2"> <span class="fake-input"></span><span class="fake-label ">Is an expensive way to purchase items <span class=""></span></span> </label>
                                        </li>
                                    </ul>
                                </li>

                                <li><strong class="h3">To recover from using high-cost credit products and services:</strong>
                                    <ul class="choose-list">
                                        <li> <label for="answer-18"> 
                                          <input id="answer-18" type="radio" name="answer-6" value="0"> <span class="fake-input"></span><span class="fake-label "> Prioritize the creditors that charge the highest interest rates and fees <span class=""></span></span> </label>
                                        </li>

                                        <li> <label for="answer-19"> 
                                          <input id="answer-19" type="radio" name="answer-6" value="1"> <span class="fake-input"></span><span class="fake-label "> Drop insurance coverage so you can pay more to your creditors <span class=""></span></span> </label>
                                        </li>

                                        <li> <label for="answer-20">
                                         <input id="answer-20" type="radio" name="answer-6" value="2"> <span class="fake-input"></span><span class="fake-label ">Completely avoid using credit now and in the future <span class=""></span></span> </label>
                                       </li>
                                    </ul>
                                </li>

                                <li><strong class="h3">A wise alternative to using a subprime loan in times of need is:</strong>
                                    <ul class="choose-list">
                                        <li> <label for="answer-21"> 
                                          <input id="answer-21" type="radio" name="answer-7" value="0"> <span class="fake-input"></span><span class="fake-label ">Cash out your retirement savings <span class=""></span></span>
                                           </label>
                                         </li>

                                        <li> <label for="answer-22"> 
                                          <input id="answer-22" type="radio" name="answer-7" value="1"> <span class="fake-input"></span><span class="fake-label ">Sell property that you don't need or use <span class=""></span></span> </label>
                                        </li>

                                        <li> <label for="answer-23"> 
                                          <input id="answer-23" type="radio" name="answer-7" value="2"> <span class="fake-input"></span><span class="fake-label "> Use all the equity in your home <span class=""></span></span> </label>
                                        </li>
                                    </ul>
                                </li>

                                <li><strong class="h3">Traditional financial institutions:</strong>
                                    <ul class="choose-list">
                                        <li> <label for="answer-24">
                                         <input id="answer-24" type="radio" name="answer-8" value="0"> <span class="fake-input"></span><span class="fake-label ">Will always lend you money when you need it <span class=""></span></span> </label>
                                       </li>

                                        <li> <label for="answer-25"> 
                                          <input id="answer-25" type="radio" name="answer-8" value="1"> <span class="fake-input"></span><span class="fake-label "> Are only available to those with jobs <span class=""></span></span> </label>
                                        </li>

                                        <li> <label for="answer-26">
                                         <input id="answer-26" type="radio" name="answer-8" value="2"> <span class="fake-input"></span><span class="fake-label ">Typically provide their members with the most cost-effective services, loans, and lines of credit <span class=""></span></span> </label>
                                       </li>
                                    </ul>
                                </li>

                                <li><strong class="h3">Rather than turning to expensive financing to purchase an item, it is usually better to:</strong>
                                    <ul class="choose-list">
                                        <li> <label for="answer-27">
                                         <input id="answer-27" type="radio" name="answer-9" value="0"> <span class="fake-input"></span><span class="fake-label ">Use a store's layaway program <span class=""></span></span> </label>
                                       </li>

                                        <li> <label for="answer-28"> 
                                          <input id="answer-28" type="radio" name="answer-9" value="1"> <span class="fake-input"></span><span class="fake-label ">Ask your employer for an advance on your paycheck <span class=""></span></span> </label>
                                        </li>

                                        <li> <label for="answer-29">
                                         <input id="answer-29" type="radio" name="answer-9" value="2"> <span class="fake-input"></span><span class="fake-label ">Cash out a life insurance policy <span class=""></span></span> </label>
                                       </li>
                                    </ul>
                                </li>
                            </ol>
                            <!--input type="hidden" name="quiz-submission" value="quiz-submission"--> <input type="hidden" id="quizId" name="quizid" value="1948"><input type="hidden" name="wlw" value="707"> <input type="submit" value="Submit" class="btn btn-warning submit-quiz">
                        </div>
                    </form>
                </div>
            </div>
        </section>
        </main>

 <!-- login quiz code end -->

