<?php
// setting error logging to be active
ini_set("log_errors", TRUE); 
// setting the logging file in php.ini
//$log_file = $_SERVER['DOCUMENT_ROOT']."/log/php.log";
//ini_set('error_log', $log_file);
 ini_set('display_errors', TRUE);

// Report simple running errors
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_USER_ERROR | E_USER_WARNING | E_USER_NOTICE);
//error_reporting(E_ALL);

/**** Function for users,quiz,contact,homepage ***/
/**** Author: Dhiraj uphat***/

include('smtp/PHPMailerAutoload.php');


/** function for user login **/
function userLogin($user,$pass){
	global $conn,$base_url;
	$user = trim($user);
	$pass = trim($pass);
	$wlw = $_SESSION['white_label_website_id'];
	$sql = "select * from whitelabel_users where email='$user' and white_label_website_id='$wlw' and status='active'";
		
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		//$_SESSION['login_error_new']='As system is updated, you have to reset the password.';	
		$row = mysqli_fetch_array($res);
		$hash = $row['password'];	
		
		if (password_verify($pass, $hash)) {
		  // echo '<br/>Password is valid!';
			$_SESSION['loggedIn']=$row['id'];
			$_SESSION['wp_user_id'] = getWPUserId($user);
			$_SESSION['user_name']=$row['firstname'].' '.$row['lastname'];
			$_SESSION['user_contact']=$row['email'];
			$_SESSION['display_name']=$row['firstname'].' '.$row['lastname'];
			$log_msg ="User logged In Success : $user, Url: $base_url";
			wh_log($log_msg);
			logging($log_msg);

			// logging the error
error_log($log_msg);
//file_put_contents($_SERVER['DOCUMENT_ROOT']."/log/php.log", error_log($log_msg) . "\r\n", FILE_APPEND);
			return true;
			//echo'password valid';
		} else {
			$log_msg ="User logged In fail, Password not match : $user, Url: $base_url";
			wh_log($log_msg);;
			logging($log_msg);
			// logging the error
error_log($log_msg);
//file_put_contents($_SERVER['DOCUMENT_ROOT']."/log/php.log", error_log($log_msg) . "\r\n", FILE_APPEND);
		   return false;
			//echo'password invalid';		
		}
		
	}
	$log_msg ="User logged In fail, Email not match : $user, Url: $base_url";
	wh_log($log_msg);	
			// logging the error
error_log($log_msg);
			logging($log_msg);
	return false;
}


/** function To get wp user id **/
function getWPUserId($email){
	global $conn;
	$sqlWp = "SELECT ID FROM wp_users WHERE user_email='$email' LIMIT 1";
	$resWp = mysqli_query($conn,$sqlWp);
	if(mysqli_num_rows($resWp)>0){
		$rowWp = mysqli_fetch_array($resWp);
		return $rowWp['ID']; 
	}
	else return 0;
}

/** function for user registration **/
function userRegistration($post){
	global $conn,$base_url;
	
	$firstname = trim($post['firstname']);
	$lastname = trim($post['lastname']);
	$email = trim($post['email']);
	$streetAddress = trim($post['streetAddress']);
	$city = trim($post['city']);
	$state = trim($post['state']);
	$zip = trim($post['zip']);
	$memberNumber = trim($post['memberNumber']);
	$pasword = trim($post['password']);
	$cost=10; // Default cost
	$password = password_hash($pasword, PASSWORD_BCRYPT, ['cost' => $cost]);
	$is_employee = trim($post['is_employee']);
	$firstname = trim($post['firstname']);
	$date=date('Y-m-d H:i:s');
	$wlw = $_SESSION['white_label_website_id'];
	$token = generateToken();


			$getlastid =mysqli_query($conn,"SELECT id FROM whitelabel_users ORDER BY id LIMIT 1");
	$sql="INSERT INTO `whitelabel_users` (`firstname`, `lastname`, `white_label_website_id`, `email`, `status`, `password`, `avatar`, `confirmed`, `confirmation_code`, `remember_token`, `created_at`, `updated_at`, `streetAddress`, `city`, `state`, `zip`, `memberNumber`, `is_employee`, `is_super_admin`) VALUES ('$firstname', '$lastname', '$wlw', '$email', 'inactive', '$password', '', '0', '$token', NULL, '$date', '$date', '$streetAddress', '$city', '$state', '$zip', NULL, '$is_employee', '0')";

	
	if(mysqli_query($conn,$sql)){
		
		/** check if user with this email id is exists or not **/	
		$chkFlag = getWPUserId($email); 
		if($chkFlag==0){

			$getuserid =mysqli_query($conn,"SELECT ID FROM wp_users ORDER BY ID LIMIT 1");
			
			$sqluser ="INSERT INTO wp_users (user_login,user_pass,user_email,user_url,user_registered,user_activation_key,user_status,display_name) VALUES('$firstname $lastname','$password','$email','','$date','$token','0','$firstname $lastname')";
			
			if(!mysqli_query($conn,$sqluser)){
				$msg = 'Registration fail in wp_user: User Name:'.$firstname.' '.$lastname. ', Email Id: '.$email.', Sql Error: '. mysqli_error($conn);
				wh_log($msg);
			// logging the error
error_log($msg);
				logging($msg);
			}
		
			$last_id = mysqli_insert_id($conn);
			$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
			meta_key='wlw_id',
			meta_value='$wlw'
			";
			
			mysqli_query($conn,$sqlmeta1);
			
			$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
			meta_key='nickname',
			meta_value='$firstname $lastname'
			";
			mysqli_query($conn,$sqlmeta1);
			
			$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
			meta_key='first_name',
			meta_value='$firstname'
			";
			mysqli_query($conn,$sqlmeta1);
			
			
			$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
			meta_key='last_name',
			meta_value='$lastname'
			";
			mysqli_query($conn,$sqlmeta1);
			
			$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
			meta_key='description',
			meta_value=''
			";
			mysqli_query($conn,$sqlmeta1);
			
			$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
			meta_key='rich_editing',
			meta_value='true'
			";
			mysqli_query($conn,$sqlmeta1);
			
			$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
			meta_key='comment_shortcuts',
			meta_value='false'
			";
			mysqli_query($conn,$sqlmeta1);
			
			$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
			meta_key='admin_color',
			meta_value='fresh'
			";
			mysqli_query($conn,$sqlmeta1);
			
			$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
			meta_key='use_ssl',
			meta_value=0
			";
			mysqli_query($conn,$sqlmeta1);
			
			$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
			meta_key='show_admin_bar_front',
			meta_value='true'
			";
			mysqli_query($conn,$sqlmeta1);
			
			$wp_capabilities=serialize(array('cu_partner' => 1));
			$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
			meta_key='wp_capabilities',	meta_value='$wp_capabilities'";
			
			mysqli_query($conn,$sqlmeta1);
			
			$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
			meta_key='wp_user_level',
			meta_value='0'
			";
			mysqli_query($conn,$sqlmeta1);
			
			$sqlmeta1 = "INSERT INTO wp_usermeta SET user_id='$last_id',
			meta_key='dismissed_wp_pointers',
			meta_value=''
			";
			mysqli_query($conn,$sqlmeta1);
		}
		$link = $base_url.'index.php?action=verify&token='.$token;
		//$_SESSION['reg_link']=$link;
		if(isset($_SESSION['register_message_text']) && trim($_SESSION['register_message_text'])!=''){
			$body = str_replace(array('%username%','%site_title%','%link%','%user_email%','%site_url%'),array($firstname,$_SESSION['title'],$link,$email,$base_url),$_SESSION['register_message_text']);
		}
		else {
			$body="<p>Thank you $firstname for creating your new profile with BALANCE, in partnership with ".$_SESSION['title'].". Please click the link below to verify your ownership of $email.</p>

			<p>CLICK THIS LINK TO VERIFY: <BR><a href='$link'>$link</a> <BR>
			<BR><BR>
			Best,<BR>

			BALANCE in partnership with ".$_SESSION['title']."</p>";
		}
		$sendname = $firstname.' '.$lastname;
		
		if(isset($_SESSION['register_subject']) && trim($_SESSION['register_subject'])!=''){
			$register_subject = str_replace(array('%username%','%site_title%'),array($sendname,$_SESSION['title']),$_SESSION['register_subject']);
		}
		else {
			$register_subject =	'BALANCE Account Email Verification!';
		}
		
		$log = array('Registration Successful',"User Id $last_id","User Name: $sendname","Email Id: $email");	
		sendMail($email,$register_subject,$body,$sendname,$log);
			// logging the error
error_log($log);
			$msg = "Registration Successful, User Id $last_id, User Name: $sendname, Email Id: $email";
			logging($msg);
			
		return true;
			
	}
	else {
		$msg = 'Registration fail: User Name:'.$firstname.' '.$lastname. ', Email Id: '.$email.', MYSql Error: '. mysqli_error($conn);
		wh_log($msg);
		logging($msg);
			// logging the error
error_log($msg);
		return false;
	}		

}

/** function for check user is already exists or not **/
function check_user_exist($user){
	global $conn;
	$wlw = $_SESSION['white_label_website_id'];
	$sql="SELECT id FROM whitelabel_users where email='$user' AND white_label_website_id='$wlw'";	
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		$msg = "Exiting User";
			// logging the error
error_log($msg);
			logging($msg);
		return true;
	}
	else{
		return false;
		$msg = "New User";
			// logging the error
error_log($msg);
			logging($msg);
	}	
	
}

/** function for use logout **/
function logout(){
	global $base_url;
	$msg = "User ".$_SESSION['user_name'].' Logout, Site: '.$base_url;
	wh_log($msg);
			// logging the error
error_log($msg);
			logging($msg);
	session_destroy();
	session_unset();
	header("location:$base_url");exit;
}


/** function for sending mail **/
function sendMail($to,$subject,$msg,$name,$log,$attached_file=''){
	global $smtp,$mailsendfrom;
	$mail = new PHPMailer(); 
	$mail->SMTPDebug  = false;
	$mail->IsSMTP(); 
	$mail->SMTPAuth = true; 
	$mail->SMTPSecure = 'tls'; 
	$mail->Host = $smtp['Host'];
	$mail->Port = 587; 
	$mail->IsHTML(true);
	$mail->CharSet = 'UTF-8';
	$mail->Username = $smtp['Username'];
	$mail->Password = $smtp['Password'];
	$mail->SetFrom($mailsendfrom,'Balance');
	$mail->Subject = $subject;
	$mail->Body =$msg;
	$mail->AddAddress($to,$name);
	if($attached_file!='')
	$mail->addAttachment($attached_file);
	$mail->SMTPOptions=array('ssl'=>array(
		'verify_peer'=>false,
		'verify_peer_name'=>false,
		'allow_self_signed'=>false
	));
	$logmsg = implode(' | ',$log);
	$domain = $_SESSION['domain'];
	if($mail->Send()){
		$msg = $logmsg. " | Site: $domain | To: $to | Subject: $subject".' | Mail sent successfully ' . $mail->ErrorInfo;		
	}
	else {
		$msg = $logmsg. " | Site: $domain | To: $to | Subject: $subject".' | Mail not sent successfully '  . $mail->ErrorInfo;		
	}
	// logging the error
error_log($msg);
			logging($msg);
	wh_log($msg);
	
}

/** function to create log files **/
function wh_log($log_msg) {
    $log_dir = $_SERVER['DOCUMENT_ROOT']."/log";
    if (!file_exists($log_dir))
    {
        // create directory/folder uploads.
        mkdir($log_dir, 0777, true);
    }
	$logfile='data.log';
	if (!file_exists($logfile))
    {
        fopen($logfile, "w");
    }
    //$log_file_data = $log_filename.'/log_' . date('d-M-Y') . '.log';
    $log_file_data = $log_dir.'/' . $logfile;
	$log_msg = date('Y-m-d H:i:s').' :: '.$log_msg;
    file_put_contents($log_file_data, $log_msg . "\r\n", FILE_APPEND);
    
}

function contact_log($log_msg) {
    $log_dir = $_SERVER['DOCUMENT_ROOT']."/log";
    if (!file_exists($log_dir))
    {
        // create directory/folder uploads.
        mkdir($log_dir, 0777, true);
    }
	$logfile='contact.log';
	if (!file_exists($logfile))
    {
        fopen($logfile, "w");
    }
    //$log_file_data = $log_filename.'/log_' . date('d-M-Y') . '.log';
    $log_file_data = $log_dir.'/' . $logfile;
	$log_msg = date('Y-m-d H:i:s').' :: '.$log_msg;
    file_put_contents($log_file_data, $log_msg . "\r\n", FILE_APPEND);
    
}

function contactru_log($log_msg) {
    $log_dir = $_SERVER['DOCUMENT_ROOT']."/log";
    if (!file_exists($log_dir))
    {
        // create directory/folder uploads.
        mkdir($log_dir, 0777, true);
    }
	$logfile='contactru.log';
	if (!file_exists($logfile))
    {
        fopen($logfile, "w");
    }
    //$log_file_data = $log_filename.'/log_' . date('d-M-Y') . '.log';
    $log_file_data = $log_dir.'/' . $logfile;
	$log_msg = date('Y-m-d H:i:s').' :: '.$log_msg;
    file_put_contents($log_file_data, $log_msg . "\r\n", FILE_APPEND);
    
}

/** function to generate error log **/
function logging($log_msg) {    
	
    $log_dir = $_SERVER['DOCUMENT_ROOT']."/log";
    if (!file_exists($log_dir))
    {
        // create directory/folder uploads.
        mkdir($log_dir, 0777, true);
    }
	$logfile='php.log';
	if (!file_exists($logfile))
    {
        fopen($logfile, "w");
    }


    //$log_file_data = $log_filename.'/log_' . date('d-M-Y') . '.log';
    $log_file_data = $log_dir.'/' . $logfile;
	//$log_msg = date('Y-m-d H:i:s').' :: '.$log_msg;
	$log_msg = '['.date('d-M-Y H:i:s e').']'.$log_msg;
    file_put_contents($log_file_data, $log_msg . "\r\n", FILE_APPEND);    
}

/** function for generating tokens **/
function generateToken(){
	$token = openssl_random_pseudo_bytes(32);
	$token = bin2hex($token);
	return $token;
}


/** function to very token for user confirmation and password reset **/
function verifyToken(& $token){
	global $conn,$base_url;	
	$wlw = $_SESSION['white_label_website_id'];
	$sql="SELECT * FROM whitelabel_users where confirmation_code='$token'";	
	$res = mysqli_query($conn,$sql);
	
	if(mysqli_num_rows($res)>0){
		$row=mysqli_fetch_array($res);
		$status = $row['status'];
		$user = $row['email'];
		if($status=='active') return false;
		else {			
			$sqlu="select * from whitelabel_users where email='$user' and white_label_website_id='$wlw'";	
			$resu = mysqli_query($conn,$sqlu);
			if(mysqli_num_rows($resu)>0){
				$rowu = mysqli_fetch_array($resu);
				$wid = $rowu['id'];	
				$_SESSION['wp_user_id'] = getWPUserId($user);	
				$_SESSION['loggedIn']=$rowu['id'];
				$_SESSION['user_name']=$rowu['firstname'].' '.$rowu['lastname'];
				$_SESSION['user_contact']=$rowu['email'];
				$_SESSION['display_name']=$rowu['firstname'].' '.$rowu['lastname'];
			}			
			
			//$sql="UPDATE wp_users set user_status='1' where user_activation_key	='$token'";	
			//mysqli_query($conn,$sql);
			
			$sql="UPDATE whitelabel_users set status='active' where id='$wid'";	
			if(mysqli_query($conn,$sql)){
				if(isset($_SESSION['welcome_mail']) && $_SESSION['welcome_mail'] !=''){
					$welcome_mail = $_SESSION['welcome_mail'];
					$body = str_replace(array('%site_url%','%site_title%'),array($base_url,$_SESSION['title']),$welcome_mail);					
				}
				else{				
					$body = '<div class="alert " role="alert">                   
						<h1 class="text-info text-center">Welcome to BALANCE, in partnership with '.$_SESSION['title'].'</h1>

						<p>Welcome and thank you for completing your registration with BALANCE, in partnership with '.$_SESSION['title'].'.</p>

						<p>You have taken a step to financial fitness by being an active participant in your own financial wellness.</p>

						<p>Your registration gives you access to more online education programs available on <a href="'.$base_url.'">'.$base_url.'</a> .</p>

						<p>Use your email address and BALANCE password to log in to a program when prompted.</p>
						<p>We hope you enjoy your experience with BALANCE, in partnership with '.$_SESSION['title'].'. We are here to help you achieve your financial success.</p>

						<p>Start by visiting us at <a href="'.$base_url.'">'.$base_url.' </a> .</p>
						<p>Best,</p>
						<p>BALANCE, in partnership with '.$_SESSION['title'].'</p>
					</div>';
				}
				if(isset($_SESSION['message_subject']) && $_SESSION['message_subject'] !=''){
					$message_subject = $_SESSION['message_subject'];
					$message_subject = str_replace(array('%username%','%site_title%'),array($_SESSION['display_name'],$_SESSION['title']),$message_subject);					
				}
				else{	
					$message_subject = 'Welcome to BALANCE, in Partnership with '.$_SESSION['title'];	
				}	
				
				$log = array('Verify Token');
				sendMail($user,$message_subject,$body,$_SESSION['display_name'],$log);
				return true;
			}
			else {
				$msg = 'Verifying User failed for website after registration for website '.$base_url.', Mysql Error:'.mysqli_error($conn);

			// logging the error
error_log($msg);
				logging($msg);
				wh_log($msg);
				return false;	
			}
		}
	}
	else{
		$msg = 'Verifying User failed for website after registration for website '.$base_url.', Mysql Error:'.mysqli_error($conn);
			// logging the error
error_log($msg);
		logging($msg);
		wh_log($msg);
		return false;
	}
}


/** function for sneding mail for forgot password **/
function sendmailForgotPassword($email){
	global $base_url,$conn;
	$wlw = $_SESSION['white_label_website_id'];	
	
	$sqlChk = "SELECT email FROM whitelabel_users WHERE email='$email' and white_label_website_id='$wlw'";
	$reschk = mysqli_query($conn,$sqlChk);
	if(mysqli_num_rows($reschk)>0) {	
		$token = generateToken();
		$tmppwd = bin2hex(openssl_random_pseudo_bytes(3));
		
		$link = $base_url.'forgot_password/'.$token.'/'.$email;
		//$_SESSION['reg_link']=$link;
		$body="<p>Hello!<BR>
		You are receiving this email because we received a password reset request for your account.<BR>
		<BR><BR>
		Your temporary password is $tmppwd
		<BR><BR>
		
		<a href='$link'>Reset Password</a>
		<BR><BR>
		If you did not request a password reset, no further action is required.<BR>

		<BR><BR>
		<hr>

		If you're having trouble clicking the Reset Password button, copy and paste the URL below into your web browser: $link";
		
		
		$log = array('Forgot Password');
		sendMail($email,$_SESSION['title'].' Password Reset',$body,'',$log);
		$sql="UPDATE wp_users set user_url='$token' where user_email='$email'";	
		mysqli_query($conn,$sql);
		$sql = "UPDATE whitelabel_users SET remember_token='$tmppwd' WHERE email='$email' and white_label_website_id='$wlw'";
		mysqli_query($conn,$sql);
	} else {
		$msg = "User trying to reset password failed. This user is not exists for website $base_url and email id $email";
		wh_log($msg);
			// logging the error
error_log($msg);
		logging($msg);
		$_SESSION['loginmessage']="This user is not exists.";
		header("location:$base_url".'login');die;
	}
}


/** function for checking password **/
function checkPasswordExists($data){
	global $conn;
	$pass=md5($data['pass']);
	$id=$data['id'];
	$sql="SELECT id FROM whitelabel_users where password='$pass' AND id='$id'";	
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0) return 1; 
	else return 0;
		
}	

/** function to change password **/
function change_password(& $post){
	global $conn,$base_url;
	$token = trim($post['token']);
	$email = trim($post['email']);
	$pasword = trim($post['password_confirmation']);
	$cost=10; // Default cost
	$password = password_hash($pasword, PASSWORD_BCRYPT, ['cost' => $cost]);
	$tmppassword = trim($post['password']);
	$wlw = $_SESSION['white_label_website_id'];	
	
	$sql= "select * from whitelabel_users where email='$email' and remember_token='$tmppassword' and white_label_website_id='$wlw'";	
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){		
		$row = mysqli_fetch_array($res);
		
		$_SESSION['loggedIn']=$row['id'];
		$_SESSION['user_name']=$row['firstname'].' '.$row['lastname'];
		$_SESSION['user_contact']=$row['email'];
		$_SESSION['display_name']=$row['firstname'].' '.$row['lastname'];
		
		$name = $row['firstname'].' '.$row['lastname'];
		
		
		$sql="UPDATE whitelabel_users set remember_token='', password='$password' where email='$email' and white_label_website_id='$wlw'";	
		if(mysqli_query($conn,$sql)){
			$link = $base_url.'login';
			$body="<p> Dear $name,<BR>
			Your password has been changed successfully. <BR><BR>			
			<BR>		
			</p>";
			$log = array('Change Password');
			sendMail($email,'BALANCE Password!',$body,$name,$log);
			return true;
		} else{
			$msg = "Password change fail for website $base_url and user $email, Mysql Error: ".mysqli_error($conn);
			wh_log($msg);
			// logging the error
error_log($msg);
			logging($msg);
			return false;		
		}
	}
	
	else{
		$msg = "Password change fail for website $base_url and user $email";
		wh_log($msg);
		// logging the error
error_log($msg);
			logging($msg);
		return false;
	}
	
}

/** function to get user's iinformation **/
function getUserData(){	
	global $conn;	
	$id = $_SESSION['loggedIn'];	
	$sql="SELECT * FROM whitelabel_users where id='$id'";
	$res = mysqli_query($conn,$sql);
	return mysqli_fetch_array($res);	
}



/** function to update user information **/
function updateUser($post){		
	global $conn,$base_url;
	$firstname = trim($post['firstname']);
	$lastname = trim($post['lastname']);
	$id = $post['id'];	
	$emailid = $post['emailid'];	
	$streetAddress = trim($post['streetAddress']);
	$city = trim($post['city']);
	$state = trim($post['state']);
	$zip = trim($post['zip']);
	$memberNumber = trim($post['memberNumber']);	
	$is_employee = trim($post['is_employee']);	
	if($memberNumber=='') $memberNumber=0;
	
	$sql ="UPDATE whitelabel_users SET
	firstname='$firstname',
	lastname='$lastname',
	city='$city',
	state='$state',
	zip='$zip',
	streetAddress='$streetAddress',
	memberNumber='$memberNumber',
	is_employee='$is_employee'
	WHERE email='$emailid'";//exit;
	if(mysqli_query($conn,$sql)){
		$_SESSION['user_name']="$firstname $lastname";
		$_SESSION['user_contact']=$emailid;
		$_SESSION['display_name']="$firstname $lastname";
		$msg = "User updated account information: Name: $firstname $lastname, email id: $emailid and website $base_url ";
		wh_log($msg);
		// logging the error
error_log($msg);
			logging($msg);
	} else{
		$msg = "User update account information fail: Name: $firstname $lastname, email id: $emailid and website $base_url, MySql Error: ".mysqli_error($conn);
		wh_log($msg);
		// logging the error
error_log($msg);
			logging($msg);
	}
	
}

/** function for update email id **/
function updateEmail($post){		
	global $conn,$base_url;
	$id = $post['id'];	
	$wlw = $_SESSION['white_label_website_id'];
	$email = trim($post['email']);
	$oldemail = $_SESSION['user_contact'];
	//$token = generateToken();
	$sqlc="select id from whitelabel_users where email='$email' and white_label_website_id='$wlw'";
	$resc = mysqli_query($conn,$sqlc);
	if(mysqli_num_rows($resc)>0){
		$_SESSION['email_msg']='This email id is already exists. Please try another.';
		header("location:$base_url".'account');die;
	}
	else{		
		$sqlEmailWu ="UPDATE whitelabel_users SET eoldemailmail='$email' WHERE id='$id'";
		if(!mysqli_query($conn,$sqlEmailWu)){
			$msg = "User update user email in wp_user table fail: email id: $oldemail and website $base_url, MySql Error: ".mysqli_error($conn);
			wh_log($msg);
			// logging the error
error_log($msg);
			logging($msg);
		}
		$wp_user_id = $_SESSION['wp_user_id'];
		$sqlEmail ="UPDATE wp_users SET user_email='$email' WHERE ID='$wp_user_id'";
		if(mysqli_query($conn,$sqlEmail)){
			//echo $sql;exit;
			session_destroy();
			session_unset();
			$msg = "User updated his email id. New email id is $email for website $base_url";
			wh_log($msg);
			// logging the error
error_log($msg);
			logging($msg);
		} else {
			$msg = "User update user email in wp_user table fail: old email id: $oldemail and website $base_url, MySql Error: ".mysqli_error($conn);
			wh_log($msg);
			// logging the error
error_log($msg);
			logging($msg);
		}	
	}
}

/** function to update password **/
function updatePassword($post){		
	global $conn,$base_url;
	//print_r();
	$pasword = trim($post['password']);
	$id = $post['id'];
	
	$cost=10; // Default cost
	$password = password_hash($pasword, PASSWORD_BCRYPT, ['cost' => $cost]);
	
	$sqlSelect="SELECT password,email FROM whitelabel_users where id='$id'";	
	$res = mysqli_query($conn,$sqlSelect);
	$row = mysqli_fetch_array($res);
	$oldpass = $row['password'];
	$email = $row['email'];
	
	if($oldpass==$password){
		return 1;
	}
	else{	
		$sqlUpdate="UPDATE whitelabel_users SET password='$password' WHERE id='$id'";
		if(mysqli_query($conn,$sqlUpdate)){
			$msg = "User updated password for user $email on website $base_url";
			wh_log($msg);
			// logging the error
error_log($msg);
			logging($msg);
			return 2;
		}
		else {
			$msg = "User not updated password for user $email on website $base_url, MySql Error:".mysqli_error($conn);
			wh_log($msg);
			// logging the error
error_log($msg);
			logging($msg);
			return 1;
		}
	}
}

/** function to get home page data **/
function getHomePage($domain){
	global $conn,$base_url,$success_rate;
	//$url='penair.devxekera.com';//$_SERVER['HTTP_HOST'];
	$url=$_SERVER['HTTP_HOST'];
	$html = '<article aria-label="article for Quiz" class="text-block default-content-style article default-content-style quiz-header"><div class="container"><div class="row">No Data available</div></div></article>';
	$sql = "SELECT title,domain,homepage_html,resources_page_html,webinars_page_html,contact_us_page_html,white_label_website_id,logo,phone,wp_post_id FROM wp_white_label_websites WHERE domain = '$url' and status='publish' LIMIT 1";
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		$row = mysqli_fetch_array($res);
		$_SESSION['domainhtml'] = str_replace('#b3481b','',stripslashes($row['homepage_html']));
		$webinarpage =$row['webinars_page_html'];
		$_SESSION['webinars_page_html'] = $webinarpage;
		$_SESSION['contact_us_page_html'] = str_replace(array('{{contact_us_form}}','#b3481b'),array('',''),$row['contact_us_page_html']);
		$_SESSION['resources_page_html'] = $row['resources_page_html'];
		$_SESSION['phone'] = $row['phone'];
		$_SESSION['title'] = $row['title'];
		$_SESSION['domain'] = $row['domain'];
		$_SESSION['logo'] = $row['logo'];
		$_SESSION['white_label_website_id'] = $row['white_label_website_id'];
		$_SESSION['wlw_wp_post_id'] = $row['wp_post_id'];
		
		
		
		if($webinarpage!=''){
			$_SESSION['accesstowebinars'] = '1';
			$_SESSION['webinars_menu'] = 'Webinars';	
		}		
		$html = $_SESSION['domainhtml'];
	}
	$wid = $_SESSION['wlw_wp_post_id'];
	$sqlp = "SELECT meta_value FROM wp_postmeta WHERE post_id = '$wid' and meta_key='_page_edit_data' LIMIT 1";
	$resp = mysqli_query($conn,$sqlp);
	if(mysqli_num_rows($resp)>0){
		$rowp = mysqli_fetch_array($resp);
		$meta_value = unserialize($rowp['meta_value']);
		//echo '<pre>',print_r($rowp);
		if(isset($meta_value['wlw_general_info_module'][0]['promotion_logo']['image']['fullpath'])){
			//echo'promo_logo';
			$_SESSION['promo_logo'] = $meta_value['wlw_general_info_module'][0]['promotion_logo']['image']['fullpath'];			
			$_SESSION['promo_logo_name'] = $meta_value['wlw_general_info_module'][0]['promotion_logo']['image']['filename'];						
			$_SESSION['promotion_logo_url'] = $meta_value['wlw_general_info_module'][0]['promotion_logo_url'];		
		}
		$_SESSION['logo_url'] = trim($meta_value['wlw_general_info_module'][0]['logo_url']);
		$_SESSION['accesschat'] = $meta_value['wlw_general_info_module'][0]['disable_chat'];
//echo "%%".$_SESSION['accesschat'];die;
		if(isset($meta_value['wlw_resources_module'][0]['accesstoresources']) && $meta_value['wlw_resources_module'][0]['accesstoresources']!='') {
			$_SESSION['accesstoresources'] = '1';
		}
		if(isset($meta_value['wlw_programs_module'][0]['programs']) && $meta_value['wlw_programs_module'][0]['programs']!='') {
			$_SESSION['accesstoprograms'] = '1';
		}
		
		if(isset($meta_value['wlw_general_info_module'][0]['include_account_numbers']) && $meta_value['wlw_general_info_module'][0]['include_account_numbers']=='on') {
			$_SESSION['include_account_numbers'] = '1';
		}
		else $_SESSION['include_account_numbers'] = '0';
		
		/// New code for quiz interval and admin mail settings
		
		if(isset($meta_value['test_times']) && $meta_value['test_times']!=''){
			$_SESSION['noofattempts'] = $meta_value['test_times'];	
		}
		else $_SESSION['noofattempts'] = 0;
		if(isset($meta_value['test_duration']) && $meta_value['test_duration']!=''){
			$_SESSION['test_duration'] = $meta_value['test_duration'];	
		}
		else $_SESSION['test_duration'] ='NO';
		
		if(isset($meta_value['siteadmin_superadmin_mail']) && $meta_value['siteadmin_superadmin_mail']=='on'){
			$_SESSION['siteadmin_superadmin_mail'] = 'Yes';//$meta_value['siteadmin_superadmin_mail'];
		}
		else{
			$_SESSION['siteadmin_superadmin_mail'] ='No';
		}

		if(isset($meta_value['contact_page']) && $meta_value['contact_page']!=''){
			if($meta_value['contact_page']=='Yes' || $meta_value['contact_page']=='on') {
				$_SESSION['accesstocontact']='1'; 
				$_SESSION['contact_menu']='1'; 
				$_SESSION['show_contact_form']='1'; 
			}	
			else if(trim($meta_value['m34_module'][1]['copy'])!=''){
				$_SESSION['accesstocontact']='1'; 
				$_SESSION['contact_menu']='0';  
				$_SESSION['show_contact_form']='0'; 
			}
			else { 
				$_SESSION['accesstocontact']='0'; 
				$_SESSION['contact_menu']='0';  	
				$_SESSION['show_contact_form']='0'; 		
			}
		}
			else if($meta_value['contact_page']=='No') { 
				$_SESSION['accesstocontact']='0'; 
				$_SESSION['contact_menu']='0';  	
				$_SESSION['show_contact_form']='0'; 		
			}
		else{
			$_SESSION['accesstocontact'] = '0'; 
				$_SESSION['contact_menu']='0';  
			$_SESSION['show_contact_form']='0';
		}


		if(isset($meta_value['contact_number']) && $meta_value['contact_number']!=''){
			if($meta_value['contact_number']=='Yes' || $meta_value['contact_number']=='on') {
				$_SESSION['contact_number']='1'; 
			}	
			else if(trim($meta_value['m34_module'][1]['copy'])!=''){
				$_SESSION['contact_number']='0';  
			}
			else { 
				$_SESSION['contact_number']='0'; 		
			}
		}
		else{
			$_SESSION['contact_number'] = '0';
		}

		if(isset($meta_value['contact_form']) && $meta_value['contact_form']!=''){
			if($meta_value['contact_form']=='Yes' || $meta_value['contact_form']=='on') {
				$_SESSION['contact_form']='1'; 
		//		$_SESSION['show_contact_form']='1';  
			}	
			else if(trim($meta_value['m34_module'][1]['copy'])!=''){
				$_SESSION['contact_form']='0';  
		//		$_SESSION['show_contact_form']='0';  
			}
			else if($meta_value['contact_form']=='No') { 
				$_SESSION['contact_form']='0'; 	
		//		$_SESSION['show_contact_form']='0';  	
			}
			else { 
				$_SESSION['contact_form']='0'; 	
		//		$_SESSION['show_contact_form']='0';  	
			}
		}
		else{
			$_SESSION['contact_form'] = '0';
		//		$_SESSION['show_contact_form']='1';  
		}

		if(isset($meta_value['contact_address']) && $meta_value['contact_address']!=''){
			if($meta_value['contact_address']=='Yes' || $meta_value['contact_address']=='on') {
				$_SESSION['contact_address']='1'; 
			}	
			else if(trim($meta_value['m34_module'][1]['copy'])!=''){
				$_SESSION['contact_address']='0';  
			}
			else { 
				$_SESSION['contact_address']='0'; 		
			}
		}
		else{
			$_SESSION['contact_address'] = '0';
		}
		
		if(isset($meta_value['passing_percent']) && $meta_value['passing_percent']!=''){
			$_SESSION['passing_percent']=$meta_value['passing_percent'];
		}
		else{
			$_SESSION['passing_percent']=$success_rate;
		}
		
		if(isset($meta_value['message_status']) && $meta_value['message_status']=='on'){
			if(trim($meta_value['message_text'])!='')
			$_SESSION['welcome_mail']=trim($meta_value['message_text']);
			if(trim($meta_value['message_subject'])!='')
			$_SESSION['message_subject']=trim($meta_value['message_subject']);
		
		}
		
		if(isset($meta_value['thankYou_status']) && $meta_value['thankYou_status']=='on'){
			$thankYou_text=trim($meta_value['thankYou_text']);
			if($thankYou_text!='')
			$_SESSION['thankYou_text']=trim($meta_value['thankYou_text']);
		}
		
		if(isset($meta_value['certificate_status']) && $meta_value['certificate_status']=='on'){
			if(trim($meta_value['certificate_text'])!='')
			$_SESSION['certificate_mail']=trim($meta_value['certificate_text']);
			if(trim($meta_value['certificate_subject'])!='')
			$_SESSION['certificate_subject']=trim($meta_value['certificate_subject']);
		}
		
		if(isset($meta_value['certificate_admin_status']) && $meta_value['certificate_admin_status']=='on'){
			if(trim($meta_value['certificate_admin_text'])!='')
			$_SESSION['certificate_admin_mail']=trim($meta_value['certificate_admin_text']);
			if(trim($meta_value['certificate_admin_subject'])!='')
			$_SESSION['certificate_admin_subject']=trim($meta_value['certificate_admin_subject']);
		}
		
		if(isset($meta_value['register_message_status']) && $meta_value['register_message_status']=='on'){
			if(trim($meta_value['register_message_text'])!='')
			$_SESSION['register_message_text']=trim($meta_value['register_message_text']);
			if(trim($meta_value['register_subject'])!='')
			$_SESSION['register_subject']=trim($meta_value['register_subject']);
		}
		
		
		$_SESSION['chat_message']=trim($meta_value['chat_message']);
		$_SESSION['tooltip_text']=trim($meta_value['tooltip_text']);
		$_SESSION['timesettings']=$meta_value['timesettings'];
//echo "%%<pre>";print_r($meta_value['m52_module'][0]['slider_image1']['image']['fullpath']);
		$_SESSION['bannershow']=$meta_value['m52_module'][0]['login']['loginshow'];
		$_SESSION['showslider']=$meta_value['m52_module'][0]['showslider']['loginshow'];
		
if(isset($meta_value['slider_duration']) && $meta_value['slider_duration']!=''){
        $_SESSION['slider_duration']=$meta_value['slider_duration'];
    }
    if(isset($meta_value['m52_module'][0]['slider_image1']['image']['fullpath']) && $meta_value['m52_module'][0]['slider_image1']['image']['fullpath']!=''){
        $_SESSION['slider_image1']=$meta_value['m52_module'][0]['slider_image1']['image']['fullpath'];
    }else {
	$_SESSION['slider_image1']='';
}
    if(isset($meta_value['m52_module'][0]['slider_image1_url']) && $meta_value['m52_module'][0]['slider_image1_url']!=''){
        $_SESSION['slider_image1_url']=$meta_value['m52_module'][0]['slider_image1_url'];
}else {
        $_SESSION['slider_image1_url']='';
}


    if(isset($meta_value['m52_module'][0]['slider_image2']['image']['fullpath']) && $meta_value['m52_module'][0]['slider_image2']['image']['fullpath']!=''){
        $_SESSION['slider_image2']=$meta_value['m52_module'][0]['slider_image2']['image']['fullpath'];
    }else {
        $_SESSION['slider_image2']='';
}

    if(isset($meta_value['m52_module'][0]['slider_image2_url']) && $meta_value['m52_module'][0]['slider_image2_url']!=''){
        $_SESSION['slider_image2_url']=$meta_value['m52_module'][0]['slider_image2_url'];
    }else {
        $_SESSION['slider_image2_url']='';
}

    if(isset($meta_value['m52_module'][0]['slider_image3']['image']['fullpath']) && $meta_value['m52_module'][0]['slider_image3']['image']['fullpath']!=''){
        $_SESSION['slider_image3']=$meta_value['m52_module'][0]['slider_image3']['image']['fullpath'];
    }else {
        $_SESSION['slider_image3']='';
}

    if(isset($meta_value['m52_module'][0]['slider_image3_url']) && $meta_value['m52_module'][0]['slider_image3_url']!=''){
        $_SESSION['slider_image3_url']=$meta_value['m52_module'][0]['slider_image3_url'];
    }else {
        $_SESSION['slider_image3_url']='';
}

    if(isset($meta_value['m52_module'][0]['slider_image4']['image']['fullpath']) && $meta_value['m52_module'][0]['slider_image4']['image']['fullpath']!=''){
        $_SESSION['slider_image4']=$meta_value['m52_module'][0]['slider_image4']['image']['fullpath'];
    }else {
        $_SESSION['slider_image4']='';
}

    if(isset($meta_value['m52_module'][0]['slider_image4_url']) && $meta_value['m52_module'][0]['slider_image4_url']!=''){
        $_SESSION['slider_image4_url']=$meta_value['m52_module'][0]['slider_image4_url'];
    }else {
        $_SESSION['slider_image4_url']='';
}

			
		
		
		//echo 'test_duration: ',$_SESSION['test_duration'],', no of attents:',$_SESSION['noofattempts'],',superadminmail:'.$superadminmail;
		
//		print_r($_SESSION);
		//	echo 'accesstoresources:',$meta_value['wlw_resources_module'][0]['accesstoresources'];
	}

	return $html;
}


/** Function for menu name and access **/

function getMenuAccess(){
	global $conn;
	$sql = "";
}



/** function to check if site is published or not **/
function checkSitePublish($domain){
	global $conn,$base_url;
	$url=$_SERVER['HTTP_HOST'];
	$sql = "SELECT white_label_website_id FROM wp_white_label_websites WHERE domain = '$url' and status='publish' LIMIT 1";
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		return true;
	}
	else{
		return false;
	}	
}

/** function to submit quiz **/
function submitQuiz(){
	global $conn,$mailsendfrom,$adminmail,$superadminmail,$base_url;
	$post = $_POST;
	$quizid = $post['quizid'];
	$wlw = $post['wlw'];
	
	$newArr = getQuizQuestions($quizid);
	$max_tries = $newArr['max_tries'];
	$cooldown = $newArr['cooldown'];
	$success_rate_db = $newArr['success_rate'];
	$certificate_title = $newArr['certificate_title'];
	$email_subject = $newArr['email_subject'];
	$email_body = $newArr['email_body'];
	$message_success = $newArr['message_success'];
	$has_certificate = $newArr['has_certificate'];
	$post_title = $newArr['post_title'];
	
	$wp_user_id = $_SESSION['wp_user_id'];
	$white_label_website_id = $_SESSION['white_label_website_id'];
	
	unset($post['wlw']);
	unset($post['action']);
	unset($post['quizid']);
	unset($post['max_tries']);
	unset($post['cooldown']);
	
	
	
	$newpost = array_values($post);
	
	$cntans = count($newpost);
	$cntsess = $newArr['total_questions'];
	$sess = $newArr['correct'];
	$correct_val=0;
	for($s=0;$s<$cntsess;$s++){
		if(isset($post['answer-'.$s])){
			if($sess[$s]==$post['answer-'.$s]) {
				$correct_val=$correct_val+1;
			}	
		}		
	}
	
	$perscore = ($correct_val*100)/$cntsess;
	
	
	$chk = chkIntervalForQuiz($quizid);
	
	$success_rate = $_SESSION['passing_percent'];
//	echo "%%".$chk;
	if($chk=='insert'){
		$answer = addslashes(json_encode($post));
		$quiz_date = date('Y-m-d H:i:s');	
		$sql = "INSERT INTO `wp_quiz_results` (score,num_correct,answers_data,wp_user_id,wp_post_id,mssql_id,timestamp,transferred,wlwid) values('$perscore','$correct_val','$answer','$wp_user_id','$quizid',NULL,'$quiz_date',0,'$white_label_website_id')";		
		
		if(mysqli_query($conn,$sql)){
			$last_id = mysqli_insert_id($conn);	
			$display_name = ucwords($_SESSION['display_name']);

			if($perscore>=$success_rate){
				
				$post_titlex = explode(':',$post_title);
				if(isset($post_titlex[1]) && trim($post_titlex[1])!='')
				$sectitle = $post_titlex[1];
				else $sectitle ='&nbsp;';
				$html=file_get_contents('balance.html');		
				$date = date('F d, Y',strtotime(date('Y-m-d')));
				$html = str_replace(array('%date%','%user%','%post_title1%','%post_title2%'),array($date,$display_name,$post_titlex[0],$sectitle),$html);
				
				$filenme = 'cert_'.$wp_user_id.'-'.$quizid.time().'.pdf';
				$attached_file = 'uploads/'.$filenme;
				
				createPDF($html,$attached_file);
				$sqlc="INSERT INTO wp_quiz_certificates(quiz_result_id,crtf_name) VALUES('$last_id','$filenme')";
				if(!mysqli_query($conn,$sqlc)){
					$msg = "Data not inserted in wp_quiz_certificates for quiz $post_title, User $wp_user_id on website $base_url, Mysql Error:".mysqli_error($conn);
					wh_log($msg);
					// logging the error
					error_log($msg);
					logging($msg);
				}
				
				$to = $_SESSION['user_contact'];
				$roundPerscore = round($perscore,2);
				
				if(isset($_SESSION['certificate_mail']) && $_SESSION['certificate_mail']!=''){
					$data = $_SESSION['certificate_mail'];
					$data = str_replace(array('%site_title%','%score%','%correct_val%','%total_questions%'),array($_SESSION['title'],$roundPerscore,$correct_val,$cntsess),$data);
				} 
				//$_SESSION['certificate_admin_mail']
				else {
					$data = $newArr['email_body'];				
					$data .= " Your score was ".$roundPerscore."% ($correct_val/$cntsess)";
				}
				
				if(isset($_SESSION['certificate_subject']) && $_SESSION['certificate_subject']!=''){					
					$certificate_subject = str_replace(array('%quiztitle%'),array($post_title),$_SESSION['certificate_subject']);
				} 
				else  $certificate_subject = $post_title;
				
				if(isset($_SESSION['certificate_admin_subject']) && $_SESSION['certificate_admin_subject']!=''){					
					$certificate_admin_subject = str_replace(array('%quiztitle%'),array($post_title),$_SESSION['certificate_admin_subject']);
				} 
				else  $certificate_admin_subject = $post_title;
					
				//$email_subject=$post_title;
				
				if(isset($_SESSION['certificate_admin_mail']) && $_SESSION['certificate_admin_mail']!=''){
					$adminbody = $_SESSION['certificate_admin_mail'];
					$adminbody = str_replace(array('%site_title%','%user%','%score%','%correct_val%','%total_questions%'),array($_SESSION['title'],$display_name,$roundPerscore,$correct_val,$cntsess),$adminbody);
				}
				else {
					$adminbody = "<p>$display_name has completed the quiz successfully.<BR>";
					$adminbody .= "The score was ".$roundPerscore."% ($correct_val/$cntsess)";
					$adminbody .= "<BR><BR>Website : ".$_SESSION['title'].'<BR>';
					$adminbody .= "<BR><BR>Thanks<BR>";
					$adminbody .= "Balancepro.org<BR>";
				}
				$log = array('Quiz',$post_title, $display_name,'Score '.$roundPerscore.'%');
				sendMail($to,$certificate_subject,$data,'',$log,$attached_file);
				//start of if condition swati code for tinkerfcu admin mail SmithAs@tinkerfcu.org
					
				//end of if condition swati code for tinkerfcu admin mail
				if($_SESSION['siteadmin_superadmin_mail']=='Yes'){
					if(isset($_SESSION['siteadmin'])){
						for($m=0;$m<count($_SESSION['siteadmin']); $m++){
							$smailid = $_SESSION['siteadmin'][$m];
							sendMail($smailid,$certificate_admin_subject,$adminbody,'',$log,$attached_file);
							$msg = 'Email has sent to Site admin '.$smailid;
							error_log($msg);
							logging($msg);
						}
					}
					sendMail($_SESSION['superadmin'],$certificate_admin_subject,$adminbody,'',$log,$attached_file);
					$msg = 'Email has sent to Super admin '.$_SESSION['superadmin'];
					error_log($msg);
					logging($msg);
					
				}
			} else {
				$roundPerscore = round($perscore,2);
				$log_msg = ' Quiz: '.$post_title.', User: '.$display_name.', Score '.$roundPerscore.'%';
				wh_log($log_msg);
				// logging the error
error_log($log_msg);
				logging($log_msg);
			}
		} else{
			$msg = "Data not inserted in wp_quiz_results for quiz $post_title, User $wp_user_id on website $base_url, Mysql Error:".mysqli_error($conn);
			wh_log($msg);
			// logging the error
error_log($msg);
			logging($msg);
		}
	}
	else{
		$_SESSION['message'] = 'You have recently taken this quiz. You must wait at least 24 hours to access it again.1';
		$referrer = $_SERVER['HTTP_REFERER'];
		header("location:$referrer");die;
	}
	
	
	return $last_id;
}

/** function to check interval of 24 hours for vacu **/
function chkIntervalForQuiz($quizid){
	global $conn,$quiz_sites_24;
	$chk='insert';
	$wp_user_id = $_SESSION['wp_user_id'];
	$white_label_website_id = $_SESSION['white_label_website_id'];
	
	$siteurl = $_SERVER['HTTP_HOST'];
	$noofattempts = $_SESSION['noofattempts'];
	$test_duration = $_SESSION['test_duration'];
	
	
	if($noofattempts==0) {
		return $chk;	
	}	
	//if(in_array($siteurl,$quiz_sites_24)){	
	else {	
//		if($test_duration=='NO') {
//			$sql = "SELECT ID FROM wp_quiz_results WHERE wp_user_id='$wp_user_id' AND wp_post_id='$quizid' AND wlwid='$white_label_website_id' ORDER BY ID DESC";
//		}
//		else {	
			$sql = "SELECT ID FROM wp_quiz_results WHERE wp_user_id='$wp_user_id' AND wp_post_id='$quizid' AND wlwid='$white_label_website_id' AND timestamp > NOW() - INTERVAL $test_duration HOUR ORDER BY ID DESC";
//		}
		$res = mysqli_query($conn,$sql);
		$numrows = mysqli_num_rows($res);
//echo "%%".$numrows."%%".$noofattempts;
		if($numrows>=$noofattempts){
			$chk='noinsert';
			$msg = "User trying to take quiz within 24 hours for website $siteurl. User ID is $wp_user_id, Quiz Id:$quizid";
			wh_log($msg);
			// logging the error
error_log($msg);
			logging($msg);
		}		
	}
	return $chk;	
}


/** function to get result of quiz ***/
function getLatestQuizResult(){
	global $conn,$base_url;
	$cond='';
	$userid = $_SESSION['wp_user_id'];
	$wlwid = $_SESSION['white_label_website_id'];
	if(isset($_GET['rid']) && $_GET['rid']!=''){
		$rid = $_GET['rid'];
		$cond = " AND ID='$rid'";
	}
	$sql = "SELECT * FROM wp_quiz_results WHERE wp_user_id='$userid' AND wlwid='$wlwid' $cond ORDER BY ID DESC LIMIT 1";
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		return mysqli_fetch_object($res);		
	}
	return '';	
}

/** function to get all past quizes  ***/
function getPastQuizes(){
	global $conn,$base_url,$pastquiz_days;
	$userid = $_SESSION['wp_user_id'];
	$wid = $_SESSION['white_label_website_id'];
	
	$sql = "SELECT r.*,crtf_name FROM wp_quiz_results r, wp_quiz_certificates c WHERE quiz_result_id=r.ID AND wp_user_id='$userid' AND wlwid='$wid' AND timestamp > NOW() - INTERVAL $pastquiz_days DAY ORDER BY ID DESC";
	$res = mysqli_query($conn,$sql);
	
	if(mysqli_num_rows($res)>0){
		return mysqli_fetch_all($res,MYSQLI_ASSOC);		
	}
	return '';	
}

/** function to send contact mail to user, site admin and wordpress admin ***/
function sendmailContact(){
	global $base_url,$mailsendfrom,$superadminmail,$adminmail;
	$from = $_POST['email']; // this is the sender's Email address
	$request_type = $_POST['request_type'];
	$primary_concern = $_POST['primary_concern'];
	$secondary_concern = $_POST['secondary_concern'];
	$name = $_POST['name'];
	$email = $_POST['email'];
	$messagep= $_POST['message'];

	if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
	//check ip from share internet
			$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
	//to check ip is pass from proxy
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
			$ip = $_SERVER['REMOTE_ADDR'];
	}

	
	$name = ($name!='') ? $name : 'Name not captured';
	
	$primary_concern = ($primary_concern!='') ? ucfirst(str_replace('-',' ',$primary_concern)) : 'None stated';
	
	$secondary_concern = ($secondary_concern!='') ? ucfirst(str_replace('-',' ',$secondary_concern)) : 'None stated';
	
	$email = ($email!='') ? $email : 'Email not captured';
	$messagep = ($messagep!='') ? $messagep : 'Email not captured';

	$logDetails = 'client IP: '.$ip.' | Request to speak to a counselor: '.$request_type.' | Primary Concern: '.$primary_concern.' | Secondary Concern: '.$secondary_concern.' | First Name & Last Name: '.$name.' | Email: '.$email;
	
	$message = "<style>th, td {padding: 0px;font-family: Arial, Helvetica, sans-serif;height:25px;}</style>";
	$message .= '<table>';
	$message .= "<tr><th style='font-size:25px'>".$_SESSION['title']."</th></tr>";
	$message .= "<tr><td>&nbsp;</td></tr>";
	if(isset($_POST['request_type']) && $_POST['request_type']=='counselor')
	$message .="<tr><td><b>Request For Counselor</b></td></tr>";
	else 
	$message .="<tr><td><b>General Inquiry</b></td></tr>";
	$message .= "<tr><td>&nbsp;</td></tr>";
	
	$message .= "<tr><td><b>Message from</b></td></tr>";
	
	$message .= "<tr><td>$name</td></tr>";
	$message .= "<tr><td>&nbsp;</td></tr>";
	$message .= "<tr><td><b>Sender email</b></td></tr>";
	$message .= "<tr><td>$email</td></tr>";
	$message .= "<tr><td>&nbsp;</td></tr>";
	$message .= "<tr><td><b>Primary Concern</b></td></tr>";
	$message .= "<tr><td>$primary_concern</td></tr>";
	$message .= "<tr><td>&nbsp;</td></tr>";
	$message .= "<tr><td><b>Secondary Concern</b></td></tr>";
	$message .= "<tr><td>$secondary_concern</td></tr>";
	$message .= "<tr><td>&nbsp;</td></tr>";
	$message .= "<tr><td><b>Message</b></td></tr>";
	$message .= "<tr><td>$messagep</td></tr>";	
	$message .= "</table>";
	
	$sendermessage="Dear $name,<BR>Your contact details has submitted, our team will get back to you as soon as possible.<BR><BR>Thanks,<BR>Team BalancePro<BR>";
	$log = array('Contact',$name);
	if (strpos($email, '.ru') !== false) { 
		contactru_log($logDetails);
	} else {
		if($_SESSION['siteadmin_superadmin_mail']=='Yes'){
			if(isset($_SESSION['siteadmin'])){
				for($m=0;$m<count($_SESSION['siteadmin']); $m++){
					$smailid = $_SESSION['siteadmin'][$m];
					sendMail($smailid,'Thanks for contacting us!', $message,'Balance','');
					//sendMail($smailid,'Thanks for contacting us!', $message,'',$log);
					$msg = 'Contact message has sent to Site admin'.$smailid;
					error_log($msg);
					logging($msg);
				}
			}	
			
			sendMail($_SESSION['superadmin'],'Thanks for contacting us!', $message,'Balance','');
			//sendMail($_SESSION['superadmin'],'Thanks for contacting us!', $message,'',$log);
			$msg = 'Contact message has sent to super admin'.$_SESSION['superadmin'];
			error_log($msg);
			logging($msg);
		}

	sendMail($email,'Contact BalancePro', $sendermessage,'Balance','');
	//sendMail($email,'BalancePro Contact', $sendermessage,'',$log);

	contact_log($logDetails);
	}

	$_SESSION['message']="Your form is submitted successfully!";
	header("location:$base_url".'contact');
	die();	
}

/** function to get quiz questions **/
function getQuizQuestions($postid){
	global $conn,$base_url;	
	
	$getQuestionDataQuery = mysqli_query($conn,"SELECT * FROM `wp_postmeta` WHERE `post_id`='$postid' AND `meta_key`='_page_edit_data'");

	$questionData = mysqli_fetch_array($getQuestionDataQuery);
	$data = unserialize($questionData[3]);
	
	
	$newArr=array();
	for($q=0;$q<count($data['quiz_module'][0]['quiz']); $q++){
		$newArr['question'][$q] = $data['quiz_module'][0]['quiz'][$q]['question'];
		
		for($w=0; $w<count($data['quiz_module'][0]['quiz'][$q]['answers']);$w++){			
			$newArr['answer'][$q][$w] = $data['quiz_module'][0]['quiz'][$q]['answers'][$w]['answer'];			
		}	
		
		for($a=0; $a<count($data['quiz_module'][0]['quiz'][$q]['answers']);$a++){			
			if(isset($data['quiz_module'][0]['quiz'][$q]['answers'][$a]['is_correct']) && $data['quiz_module'][0]['quiz'][$q]['answers'][$a]['is_correct']==1){
				$newArr['correct'][$q]=$a;
				break;
			}
		}	
	}
	
	$newArr['copy']=$data['m34_module'][0]['copy'];
	$newArr['max_tries'] = $data['quiz_module'][0]['max_tries'];
	$newArr['cooldown'] = $data['quiz_module'][0]['cooldown'];
	$newArr['success_rate'] = $data['quiz_module'][0]['success_rate'];
	$newArr['certificate_title'] = $data['quiz_module'][0]['certificate_title'];
	$newArr['email_subject'] = $data['quiz_module'][0]['email_subject'];
	$newArr['email_body'] = $data['quiz_module'][0]['email_body'];
	$newArr['message_success'] = $data['quiz_module'][0]['message_success'];
	$newArr['post_title'] = $data['post_title'];
	$newArr['message_fail'] = $data['quiz_module'][0]['message_fail'];
	$newArr['has_certificate'] = $data['quiz_module'][0]['has_certificate'];
	$newArr['total_questions'] = count($data['quiz_module'][0]['quiz']);
	//echo'<pre>',print_r($data),'</pre>';
	return $newArr;	
}

/** function to create PDF file for quiz ***/
function createPDF($html,$filename){
	require('pdf/vendor/autoload.php');
	//$mpdf=new \Mpdf\Mpdf();
	$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => [210, 170],'margin_top' => 10,'margin_left' => -1,'margin_right' => -10]);
	$mpdf->WriteHTML($html);
	$mpdf->output($filename,'F');	
}



/** function to get super admin email id and site admin email id ***/
function getAdminEmailIds(){
	global $conn;
	$wlw = $_SESSION['white_label_website_id'];
	$sql="SELECT email FROM whitelabel_admins WHERE white_label_website_id='$wlw'";
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		$i=0;
		while($row = mysqli_fetch_array($res)){
			$_SESSION['siteadmin'][$i] = $row['email'];
			$i++;
		}
	}
	$sql = "SELECT option_value FROM `wp_options` WHERE option_name='admin_email'";
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		$row = mysqli_fetch_array($res);		
		$_SESSION['superadmin'] = $row['option_value'];
	}	
}

/** function to fetch programs ***/
function getPrograms(){
	global $conn;
	$postid = $_SESSION['wlw_wp_post_id'];
	$getQuestionDataQuery = mysqli_query($conn,"SELECT * FROM `wp_postmeta` WHERE `post_id`='$postid' AND `meta_key`='_page_edit_data'");
	$questionData = mysqli_fetch_array($getQuestionDataQuery);
	$str = 	(substr($questionData[3],5));
	$res = unserialize($questionData[3]);
	$ids = $res['wlw_programs_module'][0]['programs'];
	$sql="select * from wp_posts where post_status='publish' and post_type='program' and ID in ($ids) order by menu_order asc";
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		$row = mysqli_fetch_all($res,MYSQLI_ASSOC);		
	}
	return $row;
}

/** function to get program image ***/
function getProgramFeaturedImage($programId){
	global $conn; 
	$sql="select TN.* from wp_postmeta as PM left join wp_postmeta as TN on TN.post_id=PM.meta_value where PM.meta_key='_thumbnail_id' and PM.post_id='$programId'";
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		return $row = mysqli_fetch_all($res,MYSQLI_ASSOC);
	}
}


/** functions for program page **/
function getProgram($id) {
	global $conn; 
	 
	$sql="select * from wp_resources as P join wp_postmeta as PM on PM.post_id=P.wp_post_id where status='publish' and PM.meta_key='_page_edit_data' and P.type='program' and P.slug='$id'";//exit;
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		$row = mysqli_fetch_all($res,MYSQLI_ASSOC);
		$program = setProgramModuleData($row[0]);
		///echo'<pre>',print_r($program['level_of_access']);exit;
		$checkAuth = checkAuthProgram($program);
        if ( $checkAuth ) {
            return $checkAuth;
        }
		
		$template = determineProgramTemplate($program['m56bModule']['program_type'],$program['m56bModule']['link_to_old_site']);
		//exit;
		list($hero, $html, $programResources) = generateProgramTemplateData($template, $program, 1);
	}
	return $hero.'<br>'.$html;
	
}

/** functions for program templates **/
function generateProgramTemplateData($template, $program, $pageNum)  {
	//print_r($program);
	$programResources = '';
	$heroButton = createProgramHeroButton($program['m1Module']);	
	$heroBackgroundImage = $program['m1Module']['bgimage']['image']['fullpath'];
	$heroTitle = $program['m1Module']['title'];
	$heroCopy = $program['m1Module']['copy'];
	$hero = '<div style="background-image: url('.$heroBackgroundImage.');" class="banner banner-hero text-center">
    <div class="container">
        <div class="row">
            <div class="banner-block short">
                <div class="banner-text">
                    <h1>'.$heroTitle.'</h1>
                    <p>'.$heroCopy.'</p>'.$heroButton .'
                </div>
            </div>
        </div>
    </div>
	</div>';
	
	if ($template === 'T26') {
		$html = $program['html'];
	} else {
		$programResources = getProgramPageInfo($program['m56bModule'][$program['m56bModule']['program_type']], $program['slug'], $pageNum);
		$html = getProgramResource($programResources['resource_id'], $programResources['page']);
	}
	return [$hero, $html, $programResources];
	
}

/** functions for program module data **/
function setProgramModuleData($program) {
	$meta = unserialize($program['meta_value']);
	$program['meta']= $meta;
	$program['m56bModule'] = $meta['m56b_module'][0];
	$program['m1Module'] = $meta['m1_module'][0];
	unset($program['meta_key'], $program['meta_value']);
	return $program;
}

/** functions to check program template page **/
function determineProgramTemplate($m56bModule, $redirectLocation) {
	global $base_url;
	switch ($m56bModule) {
		case 'link_to_old_site':			
			header("Location: $redirectLocation");exit;
			break;
		case 'program_resources':
			return 'T26';
			break;
		case 'resource_as_program':
		default:
			return 'T27';
			break;
	}
}

/** functions for program page information **/
function getProgramPageInfo($id, $programSlug, $currentPage=1)   {
	list($postId, $programResources) = getProgramsResourcesData($id);
	$pageInfo = generateProgramPageInfo($currentPage, $programSlug, $programResources, $postId);
	return $pageInfo;
}

/** functions for program resource data **/
function getProgramsResourcesData($id) {
	global $conn;
	if (is_array($id)) {
		$id = $id[0]['list'];
	}
	
	$sql = "SELECT title, post_title, wp_post_id, status, count('wp_post_id') as pages FROM wp_resources WHERE wp_post_id IN ($id) AND status = 'publish' GROUP BY wp_post_id ORDER BY field(wp_post_id, $id)";
	
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		$row = mysqli_fetch_all($res,MYSQLI_ASSOC);
	}
	return ['wp_post_id', $row];
}

/** functions for program page information **/
function generateProgramPageInfo($currentPage, $programSlug, $programResources, $postId) {
	$pageInfo = ['page' => 0, 'total_pages' => 0, 'resource_id' => 0, 'chapters' => []];

	foreach ($programResources as $idx => $resource) {
		$prev = $pageInfo['total_pages'];
		$pageInfo['total_pages'] += $resource->pages;
		$chapter = ['title' => $resource->post_title, 'slug' => createProgramUrl($programSlug, (int)$prev + 1), 'selected' => false];
		if ($pageInfo['total_pages'] >= $currentPage && empty($pageInfo['page'])) {
			$pageInfo['page'] = $currentPage - $prev;
			$pageInfo['resource_id'] = $resource->$postId;
			$chapter['selected'] = 'selected';
		}
		$pageInfo['chapters'][] = $chapter;
	}	

	return $pageInfo;
}

/** functions for program button **/
function createProgramHeroButton($m1Module) {
	$return = '';
	if (!empty($m1Module['buttontext'])) {
		$return .= !empty($m1Module['linkfield']) ? sprintf($this->build_link($m1Module['linkfield']), 'btn btn-default', $m1Module['buttontext']) : '';
	}
	return $return;
}

/** functions for program page url **/
function createProgramUrl($programSlug, $pageNum)  {
    return '/programs/' . $programSlug . '/page/'.$pageNum;
}

/** functions for program page resource **/
function getProgramResource($resourceId, $resourcePage)  {
	global $conn;
	
	$sql = "SELECT * FROM wp_resources WHERE wp_post_id = '$resourceId' AND status = 'publish' AND page_order = '$resourcePage' LIMIT 1";
	
	$res = mysqli_query($conn,$sql);
	if(mysqli_num_rows($res)>0){
		$row = mysqli_fetch_all($res,MYSQLI_ASSOC);
		return $row[0]['html'];
	}	
}

/** functions to generate resource url **/
function resourcesUrl($redirectLocation){
	global $base_url;
	$new = str_replace('/resources/','',$redirectLocation);
	$newx = explode('/',$new);
	$id = str_replace('/','',$newx[1]);	
	$type=$newx[0];
	$newsite=$base_url.'index.php?action=resources1&type='.$type.'&id='.$id;
	return $newsite;
}
/** functions to check if string contains any domain or not **/
/*function checkIfContainsDomainName($string)
{
	$pattern = '/(http[s]?\:\/\/)?(?!\-)(?:[a-zA-Z\d\-]{0,62}[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}/';
	return preg_match($pattern, $string);
}*/
?>
