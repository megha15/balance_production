<?php
include 'includes/config.php';
include 'includes/functions.php';

$sqlPastQuizes = "SELECT r.* FROM wp_quiz_results r where r.ID not in (select quiz_result_id from wp_quiz_certificates) and score>=80 AND timestamp > NOW() - INTERVAL 90 DAY ORDER BY ID DESC";


$res = mysqli_query($conn,$sqlPastQuizes);
if(mysqli_num_rows($res)>0){
	while($row = mysqli_fetch_array($res)){
			$id = $row['ID'];
			$score = $row['score'];
			$wp_post_id = $row['wp_post_id'];
			$timestamp = $row['timestamp'];
			$wp_user_id = $row['wp_user_id'];
			
			$sqlWp = "SELECT display_name FROM wp_users WHERE ID='$wp_user_id' LIMIT 1";
			$resWp = mysqli_query($conn,$sqlWp);		
			$rowWp = mysqli_fetch_array($resWp);
			$display_name = $rowWp['display_name'];
			
			
			$filenme = 'cert_'.$wp_user_id.'-'.$id.time().'.pdf';
			$sqlc="INSERT INTO wp_quiz_certificates(quiz_result_id,crtf_name) VALUES('$id','$filenme')";
			mysqli_query($conn,$sqlc);
			$attached_file = 'uploads/'.$filenme;
			$html=file_get_contents('balance.html');		
			$date = date('F d, Y',strtotime($row['timestamp']));
			
			$sqlmeta_value = "SELECT meta_value FROM `wp_postmeta` WHERE `post_id`='$wp_post_id' AND `meta_key`='_page_edit_data'";
			$res_meta = mysqli_query($conn,$sqlmeta_value);
			$rowm = mysqli_fetch_array($res_meta);
			$data = unserialize($rowm['meta_value']);
			
			$post_title = $data['post_title'];
			$post_titlex = explode(':',$post_title);
			if(isset($post_titlex[1]) && trim($post_titlex[1])!='')
			$sectitle = $post_titlex[1];
			else $sectitle ='&nbsp;';
			
			$html = str_replace(array('%date%','%user%','%post_title1%','%post_title2%'),array($date,$display_name,$post_titlex[0],$sectitle),$html);
			
			createPDF($html,$attached_file);
	}
}	
?>