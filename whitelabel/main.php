<!DOCTYPE html>
<html>

<head>
    <!-- set the encoding of your site -->
    <meta charset="utf-8">
    <!-- set the viewport width and initial-scale on mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$_SESSION['title']?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" href="/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png" />
    <!-- include the site stylesheet -->
	<link rel="stylesheet" href="/assets/css/main.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src='https://www.google.com/recaptcha/api.js' async defer ></script>
	<script>
  /*Google Analytics*/
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-62486504-4', 'auto');
  ga('send', 'pageview');
  /*Siteimprove*/
  /*<![CDATA[*/
  (function() {
    var sz = document.createElement('script'); sz.type = 'text/javascript'; sz.async = true;
    sz.src = '//siteimproveanalytics.com/js/siteanalyze_6138989.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sz, s);
  })();
  /*]]>*/
</script>
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod? n.callMethod.apply(n,arguments):n.queue.push(arguments)}; if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0'; n.queue=[];t=b.createElement(e);t.async=!0; t.src=v;s=b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s)}(window,document,'script', 'https://connect.facebook.net/en_US/fbevents.js');

fbq('init',  '1048906129296962');
 
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" width="1" src="https://www.facebook.com/tr?id=1048906129296962&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

<!-- Global site tag (gtag.js) - Google Ads: 10825277892 --> 
<script
async src="https://www.googletagmanager.com/gtag/js?id=AW-
10825277892"></script> 
<script> window.dataLayer = window.dataLayer

|| []; function gtag(){dataLayer.push(arguments);} gtag('js', new
Date()); gtag('config', 'AW-10825277892'); </script>
</head>
<style>

@font-face {
    font-family: dinot;
    src: url(/assets/fonts/DINOT-CondBold.eot);
    src: url(/assets/fonts/DINOT-CondBold.eot?#iefix) format("embedded-opentype"), url(/assets/fonts/DINOT-CondBold.svg) format("svg"), url(/assets/fonts/DINOT-CondBold.woff) format("woff"), url(/assets/fonts/DINOT-CondBold.ttf) format("truetype");
    font-weight: 700;
    font-style: normal
}

@font-face {
    font-family: dinot;
    src: url(/assets/fonts/DINOT-CondMedium.eot);
    src: url(/assets/fonts/DINOT-CondMedium.eot?#iefix) format("embedded-opentype"), url(/assets/fonts/DINOT-CondMedium.svg) format("svg"), url(/assets/fonts/DINOT-CondMedium.woff) format("woff"), url(/assets/fonts/DINOT-CondMedium.ttf) format("truetype");
    font-weight: 600;
    font-style: normal
}

@font-face {
    font-family: dinot;
    src: url(/assets/fonts/DINOT-CondRegular.eot);
    src: url(/assets/fonts/DINOT-CondRegular.eot?#iefix) format("embedded-opentype"), url(/assets/fonts/DINOT-CondRegular.svg) format("svg"), url(/assets/fonts/DINOT-CondRegular.woff) format("woff"), url(/assets/fonts/DINOT-CondRegular.ttf) format("truetype");
    font-weight: 400;
    font-style: normal
}

@font-face {
    font-family: Gotham;
    src: url(/assets/fonts/Gotham-Bold.eot);
    src: url(/assets/fonts/Gotham-Bold.eot?#iefix) format("embedded-opentype"), url(/assets/fonts/Gotham-Bold.svg) format("svg"), url(/assets/fonts/Gotham-Bold.woff) format("woff"), url(/assets/fonts/Gotham-Bold.ttf) format("truetype");
    font-weight: 700;
    font-style: normal
}

@font-face {
    font-family: Gotham;
    src: url(/assets/fonts/Gotham-Book.eot);
    src: url(/assets/fonts/Gotham-Book.eot?#iefix) format("embedded-opentype"), url(/assets/fonts/Gotham-Book.svg) format("svg"), url(/assets/fonts/Gotham-Book.woff) format("woff"), url(/assets/fonts/Gotham-Book.ttf) format("truetype");
    font-weight: 400;
    font-style: normal
}

@font-face {
    font-family: Gotham;
    src: url(/assets/fonts/Gotham-Medium.eot);
    src: url(/assets/fonts/Gotham-Medium.eot?#iefix) format("embedded-opentype"), url(/assets/fonts/Gotham-Medium.svg) format("svg"), url(/assets/fonts/Gotham-Medium.woff) format("woff"), url(/assets/fonts/Gotham-Medium.ttf) format("truetype");
    font-weight: 600;
    font-style: normal
}

.wcolor{color:white;}

.alert {
  padding: 20px; 
  opacity: 1;
  transition: opacity 0.6s;
  margin-bottom: 15px;
}

.alert.success {background-color: #04AA6D;}
.alert.info {background-color: #2196F3;}
.alert.warning {background-color: #ff9800;}

.closebtn {
  margin-left: 15px;
  color: white;
  font-weight: bold;
  float: right;
  font-size: 22px;
  line-height: 20px;
  cursor: pointer;
  transition: 0.3s;
}

.closebtn:hover {
  color: black;
}
</style>

<body>
    <!-- header of the page -->
   <?php include 'templates/header.php'?>
    <!-- contain main informative part of the site -->
        <!-- footer of the page -->
		
    
    <?php include $file?>
    <?php include 'templates/footer.php'?>
	<?php if($action=='login'){ ?>
    <?php include 'templates/onlyregistration.php'?>
    <?php } ?>
	
    
	
    <script src="/assets/js/vendor.js"></script>
    <script src="/assets/js/plugins.js"></script>
    <script src="/assets/js/main.js"></script>
    <script src="/assets/js/ajax.js"></script>
	<script src="/assets/js/validation.js"></script>
	<script src="/assets/js/custom.js?ver=10.5"></script>
<script>
var close = document.getElementsByClassName("closebtn");
var i;
for (i = 0; i < close.length; i++) {
  close[i].onclick = function(){
    var div = this.parentElement;
    div.style.opacity = "0";
    setTimeout(function(){ div.style.display = "none"; }, 600);
  }
}
</script>
<?php if(isset($_SESSION['reg_link'])){?>
				<a href=<?=$_SESSION['reg_link']?>>link</a><BR>
				<?php } ?>
</body>

</html>
