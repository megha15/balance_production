$( document ).ready(function() {
	$("#frm_register").on('submit', function(e) {		
		e.preventDefault();		
		if(validRegister()){
		$('#frmsubmitbtn').hide();	
		$('#load_msg').html('Loading...');	
		var email = $('#emailweb').val();
		
		let success_msg =`<main id="main" role="main" style="margin-top:0">
    <div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="alert alert-success" style="float:left">
    Your Email verification has been sent to: <a href=" " class="__cf_email__" data-cfemail="cfa2aaa8a7aea3aeadaeabaafefd8fa8a2aea6a3e1aca0a2e1">[${email}]</a>
</div>
            
        </div>
    </div>
</div>
</main>`;	

		var formData = new FormData(this);
		$.ajax({
			url: 'index.php?action=do_register',
			data: formData,
			type: 'POST',
			success: function(data) {
				$('#frmsubmitbtn').show();
				$('#load_msg').html('');	
//				alert(data);
				data = data.trim();
				//alert(data)
				if(data=='1'){
					 $("#myModal").removeClass("fade").modal("hide");
					 $("#Modalsuccess").modal("show").addClass("fade");
					 $("#log_reg_msg_id").html(success_msg);
					 $("#frm_register").trigger("reset");
					 $('.pmsg').html('');
				}
				else if(data=='2'){					
					$("#emailmsg").html('This user already exists').addClass('pmsg');					
					$('#email').focus();
					$('#frmsubmitbtn').show();
					$('#load_msg').html('');	
				}
				else if(data=='3'){						
					$('#frmsubmitbtn').show();
					$('#grecaptchamsg').css('color','red').css('text-align','center').html('Something went wrong in captcha.');	
				}
				else if(data=='4'){						
					$('#frmsubmitbtn').show();
					$('#grecaptchamsg').css('color','red').css('text-align','center').html('Please select captcha.');	
				}	
				else{
					$("#Modalerror").modal("show").addClass("fade");
				}
			},
			cache: false,
			contentType: false,
			processData: false
		});
		}
	})	
});

$( document ).ready(function() {
	$("#frm_register_mob").on('submit', function(e) {		
		e.preventDefault();		
		if(validRegisterMob()){
		$('#mob_frmsubmitbtn').hide();	
		$('#load_msgmob').html('Loading...');	
		var email = $('#emailmob').val();
		let success_msg =`<main id="main" role="main" style="margin-top:0">
    <div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="alert alert-success" style="float:left">
    Your Email verification has been sent to: <a href=" " class="__cf_email__" data-cfemail="cfa2aaa8a7aea3aeadaeabaafefd8fa8a2aea6a3e1aca0a2e1">[${email}]</a>
</div>
            
        </div>
    </div>
</div>
</main>`;	
		
		var formData = new FormData(this);
		$.ajax({
			url: 'index.php?action=do_register',
			data: formData,
			type: 'POST',
			success: function(data) {
				$('#mob_frmsubmitbtn').show();
				$('#load_msgmob').html('');	
				data = data.trim();
				console.log(data);
//alert(data);
				if(data=='1'){
					 $("#myModal").removeClass("fade").modal("hide");
					 $("#Modalsuccess").modal("show").addClass("fade");
					 $("#log_reg_msg_id").html(success_msg);
					 $("#frm_register_mob").trigger("reset");
					 $('.pmsg').html('');
				}
				else if(data=='2'){					
					$("#emailmsgmob").html('This user already exists').addClass('pmsg');					
					$('#emailmob').focus();
					$('#mob_frmsubmitbtn').show();
					$('#load_msgmob').html('');	
				}
				else if(data=='3'){						
					$('#frmsubmitbtn').show();
					$('#grecaptchamsgmob').css('color','red').css('text-align','center').html('Something went wrong in captcha.');	
				}
				else if(data=='4'){						
					$('#frmsubmitbtn').show();
					$('#grecaptchamsgmob').css('color','red').css('text-align','center').html('Please select captcha.');
					
				}	
				else{
					$("#Modalerror").modal("show").addClass("fade");
				}
			},
			cache: false,
			contentType: false,
			processData: false
		});
		}
	})	
});
/*
$( document ).ready(function() {
	$("#frm_login").on('submit', function(e) {
		$('#frmsubmitbtnlogin').hide();
		$('#load_msg_login').html('Loading...');
		e.preventDefault();		
		if(validlogin()){			
		var formData = new FormData(this);
		$.ajax({
			url: 'index.php?action=do_login',
			data: formData,
			type: 'POST',
			success: function(data) {
				$('#frmsubmitbtnlogin').show();
				$('#load_msg_login').html();
				if(data=='0'){
					 $("#Modalerror").modal("show").addClass("fade");
				}
				else{
					$("#Modal1").removeClass("fade").modal("hide");
					 $("#logout_hdr").show();
					 $("#logged_name_hdr").show();
					 $("#li_account").show();
					 $("#nologin_hdr").hide();
					 $("#nologin_hdrmob").hide();
					 $("#logged_name_hdr").html('<span class="icon-user"></span>'+data);
					 $("#log_reg_msg_id").html("Logged in Successfully!");
					 $("#Modal1").removeClass("fade").modal("hide");
					 $("#Modalsuccess").modal("show").addClass("fade");
					 $("#frm_login").trigger("reset");
					
				}
			},
			cache: false,
			contentType: false,
			processData: false
		});
		}
	})	
});

*/
$( document ).ready(function() {
	$("#frm_forgot_password").on('submit', function(e) {	
		e.preventDefault();		
		//if(validlogin()){			
		var formData = new FormData(this);
		$.ajax({
			url: 'index.php?action=sendmail_forgot',
			data: formData,
			type: 'POST',
			success: function(data) {
				//$('#Modal2').hide();
				$("#Modal2").removeClass("fade").modal("hide");
			},
			cache: false,
			contentType: false,
			processData: false
		});
		//}
	})	
});

$( document ).ready(function() {
	$("#frm_forgot_password").on('submit', function(e) {	
		e.preventDefault();		
		//if(validlogin()){			
		var formData = new FormData(this);
		$.ajax({
			url: 'index.php?action=sendmail_forgot',
			data: formData,
			type: 'POST',
			success: function(data) {
				//$('#Modal2').hide();
				$("#Modal2").removeClass("fade").modal("hide");
			},
			cache: false,
			contentType: false,
			processData: false
		});
		//}
	})	
});

$( document ).ready(function() {
	$("#mob_frm_forgot_password").on('submit', function(e) {	
		e.preventDefault();		
		//if(validlogin()){			
		var formData = new FormData(this);
		$.ajax({
			url: 'index.php?action=sendmail_forgot',
			data: formData,
			type: 'POST',
			success: function(data) {
				//$('#Modal2').hide();
				$("#Modal2").removeClass("fade").modal("hide");
			},
			cache: false,
			contentType: false,
			processData: false
		});
		//}
	})	
});

function chkUserExits(s,dtype){
	var email=s;
	var flag=0;
	//alert(email);
//	if(email!=''){
//	email = email.replace('+','%2B');

//	if(validEmail(emailid,emailmsg)==true){	

	if(s!=''){
		if(dtype=='web') valid = validEmail('emailweb','emailmsg');
		else valid = validEmail('emailmob','emailmsgmob');
	email = email.replace('+','%2B');
	if(valid==true){
	$.ajax({
			url: 'index.php?action=chkuserexits&email='+email,
			success: function(data) {
				if(data==1){
					$('#emailmsg').html('This user already exists').addClass('pmsg');
					$('#emailmsgmob').html('This user already exists').addClass('pmsg');
					$('#mailexistsid').val('1')
					flag=1;
					
				} else {
					$('#emailmsg').html('').removeClass('pmsg');
					$('#emailmsgmob').html('').removeClass('pmsg');
					$('#mailexistsid').val('0')
				}
			},
			cache: false,
			contentType: false,
			processData: false
		});		
	}

	}
}

function checkMailId(emailid,msgid){
	var email=$('#'+emailid).val();
	var flag=0;
	//alert(email);
	if(email!=''){
	email = email.replace('+','%2B');
	if(validEmail(emailid,msgid)==true){
	
	$.ajax({
			url: 'index.php?action=chkuserexits&email='+email,
			success: function(data) {
				//alert(data)
				if(data==2){
					$('#'+msgid).html('This user is not exists! Please enter correct email id.').addClass('pmsg');
					$('#chkmailforfot').val('1')					
					
				} else {
					$('#'+msgid).html('').removeClass('pmsg');
					$('#chkmailforfot').val('0')
				}
			},
			cache: false,
			contentType: false,
			processData: false
		});		
	}

	}
	return false;
}
