
function validlogin(){
	var flag=0;
	var email = $('#email').val();
	var password = $('#password').val();
	if(email==''){	
		//alert('Please enter marks');
		$('#emailmsgmain').html('Please enter the email id').addClass('pmsg');
		$('#email').focus();		
		flag=1;
	} 
	else if(IsEmail(email)==false){
        $('#emailmsgmain').html('Please enter the correct email id').addClass('pmsg');
		$('#email').focus();		
		flag=1;
        }
	else{
		$('#emailmsgmain').html('').removeClass('pmsg');
	}	
	if(password.trim()==''){
		$('#passmsg').html('Please enter the password').addClass('pmsg');
		$('#password').focus();
		flag=1;
	}
	 else{
		$('#passmsg').html('').removeClass('pmsg')
	}
	
	var googleResponse = $('#g-recaptcha-response').val();
	//alert(googleResponse)
	if (!googleResponse) {
		$('#grecaptchamsglogin').html('Please select captcha.').addClass('pmsg');	
		
		flag=1;
	} else {            
		$('#grecaptchamsglogin').html('').removeClass('pmsg');			
	}
	
if(flag==1)	
	return false;
	else return true;

}


function validEmailForgot(){
	var flag=0;
	var email = $('#emailf').val().trim();
	checkMailId('emailf','emailmsgf');
	var chkmailforfot = $('#chkmailforfot').val().trim();
	
	if(email==''){
		$('#emailmsgf').html('Please enter email address').addClass('pmsg');
		flag=1;
	}
	else if(chkmailforfot=='1'){
		//alert(email)
		$('#emailmsgf').html('This user is not exists! Please enter correct email id.').addClass('pmsg');
		flag=1;
	}
	if(flag==1)	return false;
	else return true;

}	
function validRegister(){	
	var flag=0;
	var firstname = $('#firstname').val().trim();
	var lastname = $('#lastname').val().trim();
	var email = $('#emailweb').val().trim();
	var passwordweb = $('#passwordweb').val().trim();
	//alert(passwordweb)
	var zip = $('#zip').val().trim();
	chkUserExits(email,'web');
	var mailexistsid = $('#mailexistsid').val().trim();
	
	//alert(email)
	//alert(chkUserExits(email))
	
	if(firstname==''){
		$('#firstnamemsg').html('Please enter First Name').addClass('pmsg');
		flag=1;
	}
	else{
		$('#firstnamemsg').html('').removeClass('pmsg');
	}
	
	if(lastname==''){
		$('#lastnamemsg').html('Please enter Last Name').addClass('pmsg');
		flag=1;
	}
	else{
		$('#lastnamemsg').html('').removeClass('pmsg');
	}
	
	if(email==''){
		$('#emailmsg').html('Please enter Email address').addClass('pmsg');
		flag=1;
	}
	else if(mailexistsid=='1'){
		//alert(email)
		$('#emailmsg').html('This user already exists').addClass('pmsg');
		flag=1;
	}
	else{
		$('#emailmsg').html('').removeClass('pmsg');
	}
	
	if (zip!='' && !isUSAZipCode(zip)) 
	{
		$('#zipmsg').html('Please enter correct zip code').addClass('pmsg');
		flag=1;
	} 
	else{
		$('#zipmsg').html('').removeClass('pmsg');
	}
	//alert(passwordweb)
	if(passwordweb==''){
		$('#passwordmsg').html('Please enter Password').addClass('pmsg');
		flag=1;		
	}
	else if(validPass(passwordweb)==false){
		flag=1;
	}
	else{
		$('#passwordmsg').html('').removeClass('pmsg');
	}
	
	var password_confirmation = $('#password_confirmation').val().trim();	
	if(password_confirmation==''){	
		$('#password_confirmationmsg').html('Please enter the Confirm Password').addClass('pmsg');
		flag=1;
	}
	else{
		$('#password_confirmationmsg').html('').removeClass('pmsg');
	}
	
	
	if(passwordweb!=password_confirmation){
		$('#password_confirmationmsg').html('Please enter the correct Confirm Password').addClass('pmsg');
		flag=1;
	}
	else{
		$('#password_confirmationmsg').html('').removeClass('pmsg');
		
	}
	var ckb_status = $("#counseling_and_privacy_agreement").prop('checked'); 
	
	if(!ckb_status){
		$('#counseling_and_privacy_agreementmsg').html('Please click on checkbox to agree with terms and conditions').addClass('pmsg');		
		flag=1;
	}	
	else{
		
		$('#counseling_and_privacy_agreementmsg').removeClass('pmsg').html('');
	}
	/*var googleResponse = $('#g-recaptcha-response').val();
	if (!googleResponse) {
		$('#grecaptchamsg').html('Please select google captcha');
		$('#grecaptchamsg').addClass('pmsg')		
		flag=1;
	} else {            
		$('#grecaptchamsg').html('');
		$('#grecaptchamsg').removeClass('pmsg')
	}*/
	

if(flag==1)	return false;
	else return true;
}


function validEmail(id,msgid){
	
	var email = $('#'+id).val()
	var flag=0;
	if(email==''){	
		$('#'+msgid).html('Please enter the email id').addClass('pmsg');
		flag=1;
	} 	
	
	else if(IsEmail(email)==false){
        $('#'+msgid).html('Please enter the correct email id').addClass('pmsg');		
		flag=1;
    }
	else{
		$('#'+msgid).html('').removeClass('pmsg');
		
	}
	
if(flag==1) return false;
else return true;	

}


function validRegisterMob(){	
	var flag=0;
	var firstname = $('#firstnamemob').val().trim();
	var lastname = $('#lastnamemob').val().trim();
	var email = $('#emailmob').val().trim();
	var passwordmob = $('#passwordmob').val().trim();
	//alert(passwordweb)
	var zip = $('#zip').val().trim();
	chkUserExits(email,'mob');
	var mailexistsid = $('#mailexistsid').val().trim();
	
	//alert(email)
	//alert(chkUserExits(email))
	
	if(firstname==''){
		$('#firstnamemsgmob').html('Please enter First Name').addClass('pmsg');
		flag=1;
	}
	else{
		$('#firstnamemsgmob').html('').removeClass('pmsg');
	}
	
	if(lastname==''){
		$('#lastnamemsgmob').html('Please enter Last Name').addClass('pmsg');
		flag=1;
	}
	else{
		$('#lastnamemsgmob').html('').removeClass('pmsg');
	}
	
	if(email==''){
		$('#emailmsgmob').html('Please enter email address').addClass('pmsg');
		flag=1;
	}
	else if(mailexistsid=='1'){
		//alert(email)
		$('#emailmsgmob').html('This user already exists').addClass('pmsg');
		flag=1;
	}
	else{
		$('#emailmsgmob').html('').removeClass('pmsg');
	}
	
	if (zip!='' && !isUSAZipCode(zip)) 
	{
		$('#zipmsgmob').html('Please enter correct zip code').addClass('pmsg');
		flag=1;
	} 
	else{
		$('#zipmsgmob').html('').removeClass('pmsg');
	}
	//alert(passwordweb)
	if(passwordmob==''){
		$('#passwordmsgmob').html('Please enter the password').addClass('pmsg');
		flag=1;		
	}
	else if(validPass(passwordmob)==false){
		flag=1;
	}
	else{
		$('#passwordmsgmob').html('').removeClass('pmsg');
	}
	
	var password_confirmation = $('#password_confirmationmob').val().trim();	
	if(password_confirmation==''){	
		$('#password_confirmationmsgmob').html('Please enter the confirm password').addClass('pmsg');
		flag=1;
	}
	else{
		$('#password_confirmationmsgmob').html('').removeClass('pmsg');
	}
	
	
	if(passwordmob!=password_confirmation){
		$('#password_confirmationmsgmob').html('Please enter the correct confirm password').addClass('pmsg');
		flag=1;
	}
	else{
		$('#password_confirmationmsgmob').html('').removeClass('pmsg');
		
	}
	var ckb_status = $("#counseling_and_privacy_agreementmob").prop('checked'); 
	
	if(!ckb_status){
		$('#counseling_and_privacy_agreementmsgmob').html('Please click on checkbox to agree with terms and conditions').addClass('pmsg');		
		flag=1;
	}	
	else{
		
		$('#counseling_and_privacy_agreementmsgmob').removeClass('pmsg').html('');
	}
	/*var googleResponse = $('#g-recaptcha-response').val();
	if (!googleResponse) {
		$('#grecaptchamsg').html('Please select google captcha');
		$('#grecaptchamsg').addClass('pmsg')		
		flag=1;
	} else {            
		$('#grecaptchamsg').html('');
		$('#grecaptchamsg').removeClass('pmsg')
	}*/
	

if(flag==1)	return false;
	else return true;
}


/*function validEmail(id,msgid){
	
	var email = $('#'+id).val()
	
	var flag=0;
	if(email==''){	
		$('#'+msgid).html('Please enter the email id').addClass('pmsg');
		flag=1;
	} 
	
	
	else if(IsEmail(email)==false){
        $('#'+msgid).html('Please enter the correct email id').addClass('pmsg');		
		flag=1;
    }
	else{
		$('#emailmsg').html('').removeClass('pmsg');
		
	}
if(flag==1) return false;
else return true;	

}
*/
function validChnageUser(){
	var flag=0;
	var firstname = $('#firstname').val().trim();
	var lastname = $('#lastname').val().trim();
	if(firstname==''){
		$('#ufirstmsg').html('Please enter first name').addClass('pmsg');
		$('#firstname').focus()
		flag=1;
	} else { 
	$('#ufirstmsg').html('').removeClass('pmsg');
	}
	
	if(lastname==''){
		$('#ulastmsg').html('Please enter last name').addClass('pmsg');
		$('#lastname').focus()
		flag=1;
	} else { 
	$('#ulastmsg').html('').removeClass('pmsg');
	}
	
	var zip = $('#zip').val().trim();
	//alert(zip)		
	if (zip!='' && !isUSAZipCode(zip)) 
	{
		$('#uzipmsg').html('Please enter correct zip code').addClass('pmsg');
		$('#zip').focus()		
		flag=1;
	} 
	else{
		$('#zipmsg').html('');
		$('#zipmsg').removeClass('pmsg')
	}
	
if(flag==1) return false;
else return true;	
	
	
	
	
}

function validPass(s){
	if(s.length<8){
		$('#passwordmsg').html('Please enter 8 character password').addClass('pmsg');
		return false;	
	}
	else if(CheckPassword(s)==false){
		
		$('#passwordmsg').html('Password should contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character,').addClass('pmsg');
		return false;	
	}
	else {		
		$('#passwordmsg').html('');
		return true;
	}
}



function cvalidPass(s,id){
	var ckhpassword = $('#'+id).val().trim();
	if(s.value.length<8){
		$('#password_confirmationmsg').html('Please enter 8 character password').addClass('pmsg');	
		//$('#password_confirmation').focus();	
	} 
	else if(ckhpassword!=s.value){
		$('#password_confirmationmsg').html('Please enter correct password').addClass('pmsg');	
		//$('#password_confirmation').focus();	
	}	
	else {
		$('#password_confirmationmsg').html('').removeClass('pmsg');
	}
}

function validPassReset(s,id){	
	if(s.length<8){
		$('#'+id).html('Please enter 8 character password').addClass('pmsg');
		return false;		
	} 
	else if(CheckPassword(s)==false){		
		$('#'+id).html(' <br>Password should contain at least one lowercase letter, <BR>one uppercase letter, <BR>one numeric digit, <BR>and one special character.').addClass('pmsg');
		return false;	
	}
	else {
		$('#'+id).html('');
	}
}

function CvalidPassReset(s){
	var ckhpassword = $('#password').val().trim();
	if(s.value.length<8){
		$('#msg_cpass').html('Please enter 8 character password').addClass('pmsg');		
	} 
	else if(ckhpassword!=s.value){
		$('#msg_cpass').html('Please enter correct password').addClass('pmsg');		
	}	
	else {
		$('#msg_cpass').html('').removeClass('pmsg');
	}
}

function validPassMob(s){
	//alert(s)
	if(s.length<8){
		$('#passwordmsgmob').html('Please enter 8 character password').addClass('pmsg');		
	} 
	else if(CheckPassword(s)==false){
		
		$('#passwordmsgmob').html('Password should contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character,').addClass('pmsg');
		return false;	
	}
	else {
		$('#passwordmsgmob').html('');
	}
}


function cvalidPassMob(s){
	var ckhpassword = $('#passwordmob').val().trim();	
	if(s.value.length<8){
		$('#password_confirmationmsgmob').html('Please enter 8 character password').addClass('pmsg');		
	} 
	else if(ckhpassword!=s.value){
		$('#password_confirmationmsgmob').html('Please enter correct password').addClass('pmsg');		
	}	
	else {
		$('#password_confirmationmsgmob').html('').removeClass('pmsg');
	}
}
 
 
 
function validReset(){	
	var flag=0;
	//var email=$('#email').val().trim();
	var chkmail = validEmail('email','emailmsg')
	if(chkmail==false){
		flag=1;
	}
	
	var ckhpassword=$('#password_confirmation_reset').val().trim();
	if(ckhpassword.length<8){
		$('#cmsg_pass').html('Please enter 8 character password').addClass('pmsg');		
		flag=1;
	}
	else if(validPassReset(ckhpassword,'cmsg_pass')==false){
		flag=1;
	}
	else{
		$('#cmsg_pass').html('').removeClass('pmsg');
	}
	
	var googleResponse = $('#g-recaptcha-response').val();
	
	if (!googleResponse) {
		$('#grecaptchamsgreset').html('Please select captcha.').addClass('pmsg');		
		flag=1;
	} else {            
		$('#grecaptchamsgreset').html('').removeClass('pmsg');			
	}
	
	if(flag==1)	
	return false;
	else return true;

}
/* 

function validReset(){	
	var flag=0;
	//var email=$('#email').val().trim();
	var chkmail = validEmail('email','emailmsg')
	if(chkmail==false){
		flag=1;
	}
	
	var ckhpassword=$('#password').val().trim();
	var password_confirmation=$('#password_confirmation_reset').val().trim();
	if(ckhpassword.length<8){
		$('#msg_pass').html('Please enter 8 character password').addClass('pmsg');		
		flag=1;
	}
	else if(validPassReset(ckhpassword,'msg_pass')==false){
		flag=1;
	}
	else{
		$('#msg_pass').html('').removeClass('pmsg');
	}
	if(ckhpassword!=password_confirmation){
		$('#msg_cpass').html('Please enter the correct confirm password').addClass('pmsg');
		flag=1;
	}
	else{
		$('#msg_cpass').html('').removeClass('pmsg');
	}
	
	if(flag==1)	
	return false;
	else return true;

}
*/

function checkPassExists(id,pass){
	//alert(pass)
	var ckhpassword=$('#password').val().trim();
	if(validPassReset(ckhpassword,'passwordmsg')==false){
	//if(pass.length<8){
		///$('#passwordmsg').html('Please enter 8 character password').addClass('pmsg');
		//$('#password').focus();
		
	} else {		
	$.ajax({
			url: 'index.php?action=check_password_exists&id='+id+'&pass='+pass,
			
			type: 'POST',
			success: function(data) {
				//alert(data)
				if(data==1){
					$('#passwordmsg').html('Do not enter same password, please try another.').addClass('pmsg')
				//	$('#password').focus();
					//flag=1;
				}
				else { 
				$('#passwordmsg').html('').removeClass('pmsg')
				
				}
				
			},
			cache: false,
			contentType: false,
			processData: false
		});
	}

}

function validPassChange(s){
	var flag=0;
	if(s.value.length<8){
		$('#passwordmsg').html('Please enter 8v character password').addClass('pmsg');
		flag=1;	
		$('#password').focus();
	} else {		
		$('#passwordmsg').html('');
	}
	if(flag==1)	
	return false;
	else return true;
}


function changePassword(){
	var flag=0;
	var ckhpassword=$('#password').val().trim();
	
	//
	var id=$('#isloggedin').val();
	//alert(id);return false;;
	var password_confirmation=$('#password_confirmation').val().trim();
	if(ckhpassword.length<8){
		$('#passwordmsg').html('Please enter 8 character password');
		$('#passwordmsg').addClass('pmsg');
		$('#password').focus();		
		flag=1;
	}
	else{
		
		$('#passwordmsg').html('');
		$('#passwordmsg').removeClass('pmsg')
	}
	if(ckhpassword!=password_confirmation){
		$('#password_confirmationmsg').html('Please enter the correct confirm password');
		$('#password_confirmationmsg').addClass('pmsg')
		$('#password_confirmation').focus();		
		flag=1;
	}
	else{
		$('#password_confirmationmsg').html('');
		$('#password_confirmationmsg').removeClass('pmsg')
	}
	
	
	//alert(flag);
	
	if(flag==1)	
	return false;
	else return true;
	
}
function validforgot(){
	var flag=0;
	var email = $('#user').val();
	if((document.getElementById('user').value).trim()==''){
	
		//alert('Please enter marks');
		$('#usermsg').html('Please enter the email id');
		$('#usermsg').addClass('alert alert-warning')
		document.getElementById('user').focus();
		flag=1;
	}
	else if(IsEmail(email)==false){
        $('#usermsg').html('Please enter the correct email id');
		$('#usermsg').addClass('alert alert-warning')
		document.getElementById('user').focus();		
		flag=1;
        }
	else{
		$('#usermsg').html('');
		$('#usermsg').removeClass('alert alert-warning')
	}	
	
if(flag==1)	
	return false;
	else return true;
}

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if(!regex.test(email)) {
    return false;
  }else{
    return true;
  }
}



/*function CheckPassword(inputtxt) { 
	
      var passw= /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/; 
      if (inputtxt.value.match(passw)) {  
        return true;  
      }  
      else {         
         return false;  
      }  
   }  
*/
function CheckPassword(str)
{
    var re = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    return re.test(str);
}

function isUSAZipCode(str) 
{
  return /^\d{5}(-\d{4})?$/.test(str);
}

function captchValid(){	
	var googleResponse = $('#g-recaptcha-response').val();
	if (!googleResponse) {
		$('#grecaptchamsg').html('Please select captcha.');
		$('#grecaptchamsg').addClass('pmsg')		
		return false;
	} else {            
		$('#grecaptchamsg').html('');
		$('#grecaptchamsg').removeClass('pmsg')
		return true;
	}	
}

