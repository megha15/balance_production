#Balance Client Whitelabel Sites
App utilizes core PHP as it's core framework with the ability to
register, login (username+password & Social: Facebook and Twitter), reset your password, and send emails via the Mandrill API.

Also it gives functionality for particular quiz where flow starts with quiz attempt then quiz result and then receives quiz certification and also can view all the past certificates.

## Setup
run `chmod -R 777 /path/to/public_html/uploads` to allow for the cache and logs to be written.
Make the `public_html` directory web accessible.
Create includes/config.php file in the root of the install directory and fill in your values, you can use and replace it as needed

## .config.php
```
$host = DBHOST;
$user = DBUSER";
$pass = DBPASSWORD;
$database = DBDATABASE;

$secret = SECRETKEY;
$site_key=SITEKEY;


$base_url='http://'.$_SERVER['HTTP_HOST'].'/';

$smtp['Host'] = SMTPHOST;
$smtp['Username'] = SMTPUSER;
$smtp['Password'] = SMTPPASSWORD;
$smtp['Port'] = SMTPPORT;
$mailsendfrom = SMTPSENDMAIL;

$parent_url=PARENTURL;
```

### LIVE SERVER DEPLOYMENT

In `/var/www/whitelabel` directory you as your user do the following:
- `git pull` to get the latest version from github
- `git checkout TAG` to set the latest TAG
Then from anywhere you can do the following to deploy the latest updates  
- `deploy` to start gulp build with user `app` in `/var/www/whitelabel`  
