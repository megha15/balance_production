
<!-- contain main informative part of the site -->
   <main id="main" role="main">
        
		<?php if(isset($_SESSION['contact_us_page_html']) && $_SESSION['contact_us_page_html']!=''){?>
		<article class="text-block article default-content-style" aria-label="article module "> <div class="container">   <div class="row">
		<?php echo $_SESSION['contact_us_page_html'];?>
		
		
    </div>  </div></article>
		<?php } ?>	
		
		
        <section class="block background-white">
            <div class="container">
			<?php
			if(isset($_SESSION['error_msg']) && $_SESSION['error_msg']!=''){
			?>
			<div class="alert alert-warning">
			  <span class="closebtn">&times;</span>  
			  <?php echo $_SESSION['error_msg']?>
			</div>

			<?php
			} 
			unset($_SESSION['error_msg']);
			?>
                <div class="row">
                    <h1 class="text-info text-touch text-center">Contact</h1>
                      <?php if(isset($_SESSION['message'])){?>						  
						 <div class="alert alert-success">
						  <span class="closebtn">&times;</span>  
						  <?php echo $_SESSION['message']?>
						</div>
						<?php 
						unset($_SESSION['message']);
						}				  
						  
						  ?>              
                    <p><strong>Mailing Address</strong></p>
                    <address class="address modules">1655 Grant Street, Suite 1300<br>Concord, CA 94520</address>
                    <span class="tel-holder hidden-xs"><span class="tel-wrap">Fax:<a href="tel:8663636301" style="color: #4b6b68;"> 866-363-6301</a></span></span>
    
                    <br>
    
                    <form action="" class="submit-form"  method="POST" onsubmit='return captchValid()'>
                        <input type="hidden" name="action" value="sendmailContact">
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <select name="request_type" class="select1 form-control inner-select jcf-hidden">
                                            <option value="counselor">Request to speak to a counselor</option>
                                            <option value="general">General contact</option>
                                        </select>
                                    </div>
                                    <div class="input-group">
                                        <select name="primary_concern" class="select2 form-control inner-select jcf-hidden">
                                            <option value="">Primary Concern:</option>
                                            <option value="debt-and-budget-counseling">Debt/Budget Counseling</option>
                                            <option value="bankruptcy-services">Bankruptcy Services</option>
                                            <option value="foreclosure-prevention-counseling">Foreclosure Prevention Counseling</option>
                                            <option value="student-loan-counseling">Student Loan Counseling</option>
                                            <option value="credit-report-review">Credit Report Review</option>
                                            <option>Other</option>
                                        </select>
                                    </div>
                                    <div class="input-group">
                                        <select name="secondary_concern" class="select3 form-control inner-select jcf-hidden">
                                            <option value="">Secondary Concern:</option>
                                            <option value="debt-and-budget-counseling">Debt/Budget Counseling</option>
                                            <option value="bankruptcy-services">Bankruptcy Services</option>
                                            <option value="foreclosure-prevention-counseling">Foreclosure Prevention Counseling</option>
                                            <option value="student-loan-counseling">Student Loan Counseling</option>
                                            <option value="credit-report-review">Credit Report Review</option>
                                            <option>Other</option>
                                        </select>
                                    </div>
                                    <div class="input-group">
                                        <input type="text" placeholder="First Name &amp; Last Name" data-required="true" class="form-control" name="name" value="">
                                    </div>
                                    <div class="input-group">
                                        <input type="email" placeholder="Email" data-required="true" class="form-control" name="email" value="">
                                    </div>
										
										
										
                                </div>
                               
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <textarea cols="30" rows="10" placeholder="Message" data-required="true" class="form-control" name="message" style="height:334px"></textarea>
                                    </div>
                               
								<div class="input-group">
									<div class="g-recaptcha" data-sitekey="<?=$site_key?>"></div> <p id='grecaptchamsg'></p>
								</div>
								 </div>
                            </div>
							
								
							
								
                            <div class="submit-holder">
                                <input type="submit" value="Submit" name="submitcontact" class="but btn btn-primary form-control">
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </section>
    </main>
    <!-- footer of the page -->