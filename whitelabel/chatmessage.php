<?php
session_start();

if(isset($_SESSION['chat_message']) && trim($_SESSION['chat_message'])!='') $chatmessage = $_SESSION['chat_message'];
else $chatmessage ='<p>'.$_SESSION['title'].' chat is currently unavailable. Our normal business hours are in (UTC-08:00) Pacific Time (US):<br> %timesettings%.</p><p> You may call us at 1-888-456-2227 during normal business hours.</p>';	
	$timesettings = $_SESSION['timesettings'];
 if(isset($timesettings) && !empty($timesettings)){
$chat_time_msg ='<BR><table border="0">
	<tr>
		<td>Monday</td>';
		if(isset($timesettings['mon_status']) && $timesettings['mon_status']=='on'){ 		
		$chat_time_msg.='<td>'.$timesettings['mon_start'].$timesettings['mon_start_ap'].' to '.$timesettings['mon_end'].$timesettings['mon_end_ap'].' </td>';
		}
		else $chat_time_msg.='<td>Closed</td>';
	   $chat_time_msg.='</tr>
	<tr>
		<td>Tuesday</td>';
		if(isset($timesettings['tue_status']) && $timesettings['tue_status']=='on'){ 		
		$chat_time_msg.='<td>'.$timesettings['tue_start'].$timesettings['tue_start_ap'].' to '.$timesettings['tue_end'].$timesettings['tue_end_ap'].' </td>';
		}
	else $chat_time_msg.='<td>Closed</td>';
	   $chat_time_msg.='</tr>
	<tr>
		<td>Wednesday</td>';
		if(isset($timesettings['wed_status']) && $timesettings['wed_status']=='on'){ 		
		$chat_time_msg.='<td>'.$timesettings['wed_start'].$timesettings['wed_start_ap'].' to '.$timesettings['wed_end'].$timesettings['wed_end_ap'].'</td>';
		}
		else $chat_time_msg.='<td>Closed</td>';
	   $chat_time_msg.='</tr>
	<tr>
		<td>Thursday</td>';
		if(isset($timesettings['thu_status']) && $timesettings['thu_status']=='on'){ 		
		$chat_time_msg.='<td>'.$timesettings['thu_start'].$timesettings['thu_start_ap'].' to '.$timesettings['thu_end'].$timesettings['thu_end_ap'].' </td>';
		} else $chat_time_msg.='<td>Closed</td>';
	   $chat_time_msg.='</tr>
	<tr>
		<td>Friday</td>';
		if(isset($timesettings['fri_status']) && $timesettings['fri_status']=='on'){ 		
		$chat_time_msg.='<td>'.$timesettings['fri_start'].$timesettings['fri_start_ap'].' to '.$timesettings['fri_end'].$timesettings['fri_end_ap'].' </td>';
		} else $chat_time_msg.='<td>Closed</td>';
	   $chat_time_msg.='</tr>
	<tr>
		<td>Saturday</td>';
		if(isset($timesettings['sat_status']) && $timesettings['sat_status']=='on'){ 		
		$chat_time_msg.='<td>'.$timesettings['sat_start'].$timesettings['sat_start_ap'].' to '.$timesettings['sat_end'].$timesettings['sat_end_ap'].'<br>';
		} else $chat_time_msg.='<td>Closed</td>';
	   $chat_time_msg.='</tr>
	<tr>
		<td>Sunday</td>';
		if(isset($timesettings['sun_status']) && $timesettings['sun_status']=='on'){ 		
		$chat_time_msg.='<td>'.$timesettings['sun_start'].$timesettings['sun_start_ap'].' To '.$timesettings['sun_end'].$timesettings['sun_end_ap'].'</td>';		
		} else $chat_time_msg.='<td>Closed</td>';
	   $chat_time_msg.='</tr>
</table>';
	$chat_title = str_replace(array('%sitetitle%','%timesettings%'),array($_SESSION['title'],$chat_time_msg),$chatmessage);
 } else {
	$chat_title = '<p>'.$_SESSION['title'].' chat is currently unavailable. Our normal business hours are in (UTC-08:00) Pacific Time (US):<BR> Monday to Thursday from 7:30am—6:00pm, Friday 7:30am—5:00pm, and Saturday 9:00am—2:00pm (PST). You may call us at 1-888-456-2227 during normal business hours.</p>';
 }
?>
<div style="font-family: 'Open Sans', sans-serif;"><?php echo $chat_title?></div>
